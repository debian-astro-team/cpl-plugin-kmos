/* $Id: kmo_priv_shift.h,v 1.2 2012-02-27 09:48:01 aagudo Exp $
 *
 * This file is part of the KMOS Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: aagudo $
 * $Date: 2012-02-27 09:48:01 $
 * $Revision: 1.2 $
 * $Name: not supported by cvs2svn $
 */

#ifndef KMOS_PRIV_SHIFT_H
#define KMOS_PRIV_SHIFT_H

/*-----------------------------------------------------------------------------
 *                              Includes
 *----------------------------------------------------------------------------*/

#include <cpl.h>

#include "kmclipm_functions.h"

/*------------------------------------------------------------------------------
 *                        Prototypes
 *----------------------------------------------------------------------------*/

cpl_error_code kmo_priv_shift(cpl_imagelist **data,
                              cpl_imagelist **noise,
                              cpl_propertylist **header_data,
                              cpl_propertylist **header_noise,
                              double xshift,
                              double yshift,
                              int flux,
                              const char *method,
                              const enum extrapolationType extrapolate);

#endif
