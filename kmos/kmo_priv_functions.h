/*
 * This file is part of the KMOS Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef KMOS_PRIV_FUNCTIONS_H
#define KMOS_PRIV_FUNCTIONS_H

/*-----------------------------------------------------------------------------
 *                              Includes
 *----------------------------------------------------------------------------*/

#include <cpl.h>

#include <kmclipm_vector.h>

/*------------------------------------------------------------------------------
 *                        Prototypes
 *----------------------------------------------------------------------------*/
extern int print_warning_once_tweak_std;
extern int print_warning_once_tweak_std_noise;
extern int print_warning_once_tweak_closest;

/***********************/
/* Parameters Handling */
int kmos_combine_pars_create(
        cpl_parameterlist   *   pl,
        const char          *   recipe_name,
        const char          *   default_method,
        int                     no_cmethod) ;
void kmos_band_pars_create(
        cpl_parameterlist   *   pl,
        const char          *   recipe_name) ;
cpl_error_code kmos_combine_pars_load(
        cpl_parameterlist   *   pl,
        const char          *   recipe_name,
        const char          **  cmethod,
        double              *   cpos_rej,
        double              *   cneg_rej,
        int                 *   citer,
        int                 *   cmin,
        int                 *   cmax,
        int                     no_cmethod) ;
cpl_error_code kmos_band_pars_load(
        cpl_parameterlist   *   pl,
        const char          *   recipe_name) ;
/***********************/

cpl_vector*         kmo_create_lambda_vec(
                                int size,
                                int crpix,
                                double crval,
                                double cdelt);

int                 kmo_is_in_range(
                                const cpl_vector *ranges,
                                const cpl_vector *ifu_lambda_in,
                                int index);

cpl_vector*         kmo_identify_slices(
                                const cpl_vector *ranges,
                                double ifu_crpix,
                                double ifu_crval,
                                double ifu_cdelt,
                                int size_ifu);

cpl_vector*         kmo_identify_ranges(const char *txt);

cpl_vector*         kmo_identify_values(const char *txt);

double              kmo_image_get_stdev_median(
                                const cpl_image *data);

cpl_error_code      kmo_check_frameset_setup(
                                cpl_frameset *frameset,
                                const char *frame_type,
                                int check_filter,
                                int check_grating,
                                int check_rotation);

cpl_error_code kmo_check_oh_spec_setup(
        cpl_frameset    *   frameset,
        const char      *   frame_type) ;

cpl_error_code      kmo_check_frame_setup(
                                cpl_frameset *frameset,
                                const char *frame_type1,
                                const char *frame_type2,
                                int check_filter,
                                int check_grating,
                                int check_rotation);

cpl_error_code      kmo_priv_compare_frame_setup(
                                const cpl_frame *frame1,
                                const cpl_frame *frame2,
                                const char *frame_type1,
                                const char *frame_type2,
                                int check_filter,
                                int check_grating,
                                int check_rotation);

cpl_error_code      kmo_check_frame_setup_md5(
                                cpl_frameset *frameset);

cpl_error_code      kmo_check_frame_setup_md5_xycal(
                                cpl_frameset *frameset);

const char*         kmo_get_pro_keyword_val(
                                const cpl_propertylist *header,
                                const char *par_name);

cpl_error_code      kmo_check_frame_setup_sampling(
                                cpl_frameset *frameset);

#endif
