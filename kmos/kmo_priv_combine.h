/* 
 * This file is part of the KMOS Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef KMOS_PRIV_COMBINE_H
#define KMOS_PRIV_COMBINE_H

/*-----------------------------------------------------------------------------
 *                              Includes
 *----------------------------------------------------------------------------*/

#include <cpl.h>

#include "kmclipm_functions.h"


/*------------------------------------------------------------------------------
 *                        Prototypes
 *----------------------------------------------------------------------------*/

cpl_error_code kmos_combine_frames(
        const cpl_imagelist *   data,
        const char          *   cmethod,
        double                  cpos_rej,
        double                  cneg_rej,
        int                     citer,
        int                     cmax,
        int                     cmin,
        cpl_image           **  avg_data,
        cpl_image           **  avg_noise,
        double                  default_val) ;


char*          kmo_shorten_ifu_string(const char *in);

cpl_error_code kmo_align_subpix(double *x, double *y,
                                cpl_imagelist **data,
                                cpl_imagelist **noise,
                                cpl_propertylist **header_data,
                                cpl_propertylist **header_noise,
                                int flux,
                                const char *method,
                                const enum extrapolationType extrapolate,
                                double tol,
                                FILE *fid,
                                int *xmin,
                                int *xmax,
                                int *ymin,
                                int *ymax,
                                const char *name);

cpl_error_code kmo_priv_combine(cpl_imagelist **cube_data,
                                cpl_imagelist **cube_noise,
                                cpl_propertylist **header_data,
                                cpl_propertylist **header_noise,
                                cpl_size data_counter,
                                int noise_counter,
                                const char *name,
                                const char *ifus_txt,
                                const char *method,
                                const char *smethod,
                                const char *fmethod,
                                const char *filename,
                                const char *cmethod,
                                double cpos_rej,
                                double cneg_rej,
                                int citer,
                                int cmin,
                                int cmax,
                                const enum extrapolationType extrapolate,
                                int flux,
                                cpl_imagelist **cube_combined_data,
                                cpl_imagelist **cube_combined_noise,
                                cpl_image **exp_mask);

cpl_error_code kmo_edge_nan(cpl_imagelist *data,
                            int ifu_nr);

#endif
