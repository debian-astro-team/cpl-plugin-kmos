/* $Id: kmo_priv_reconstruct.h,v 1.8 2013-05-17 15:59:45 aagudo Exp $
 *
 * This file is part of the KMOS Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: aagudo $
 * $Date: 2013-05-17 15:59:45 $
 * $Revision: 1.8 $
 * $Name: not supported by cvs2svn $
 */

#ifndef KMOS_PRIV_RECONSTRUCT_H
#define KMOS_PRIV_RECONSTRUCT_H

/*-----------------------------------------------------------------------------
 *                              Includes
 *----------------------------------------------------------------------------*/

#include <cpl.h>

#include "kmclipm_functions.h"

/*------------------------------------------------------------------------------
 *                        Prototypes
 *----------------------------------------------------------------------------*/
extern int print_warning_once_reconstruct;

cpl_image* kmo_create_mask_from_xcal(const cpl_image *xcal,
                                 int ifu_id);

double          kmo_calc_flux_in(const cpl_image *data,
                                const cpl_image *xcal_mask);

double          kmo_calc_mode_in(const cpl_image *data,
                                const cpl_image *xcal,
                                int ifu_id,
                                double *noise);

//cpl_error_code  kmo_calc_wcs(const cpl_propertylist *main_header,
//                                cpl_propertylist *sub_header,
//                                int ifu_nr,
//                                float crval3,
//                                float cdelt3);

cpl_error_code  kmo_calc_wcs_gd(const cpl_propertylist *main_header,
                                cpl_propertylist *sub_header,
                                int ifu_nr,
                                gridDefinition gd);

cpl_error_code  kmo_save_det_img_ext(cpl_image * det_img,
                                gridDefinition gd,
                                int dev,
                                const char *filename,
                                const char *obs_suffix,
                                cpl_propertylist *header,
                                int dev_flip,
                                int is_noise);

#endif
