/* $Id: kmo_priv_fits_check.h,v 1.1.1.1 2012-01-18 09:31:59 yjung Exp $
 *
 * This file is part of the KMOS Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: yjung $
 * $Date: 2012-01-18 09:31:59 $
 * $Revision: 1.1.1.1 $
 * $Name: not supported by cvs2svn $
 */

#ifndef KMOS_PRIV_FITS_CHECK_H
#define KMOS_PRIV_FITS_CHECK_H

#include <cpl.h>

#include "kmo_utils.h"

cpl_error_code kmo_fits_check_print_header(const cpl_propertylist *pl);

cpl_error_code kmo_fits_check_print_vector(const kmclipm_vector *vec);

cpl_error_code kmo_fits_check_print_image(const cpl_image *img);

cpl_error_code kmo_fits_check_print_imagelist(const cpl_imagelist *imglist);

cpl_error_code kmo_fits_check_print_table(const cpl_table *table);

cpl_error_code kmo_fits_check_print_info(int valid,
                                         const main_fits_desc *desc,
                                         const cpl_frame *frame);
#endif
