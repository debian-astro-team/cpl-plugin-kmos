/* 
 * This file is part of the KMOS Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef KMOS_UTILS_H
#define KMOS_UTILS_H

/*------------------------------------------------------------------------------
 *                        Includes
 *----------------------------------------------------------------------------*/

#include <cpl.h>

/*------------------------------------------------------------------------------
 *                        Defines
 *----------------------------------------------------------------------------*/

#define RAW                      "RAW"
#define F1D                      "F1D"
#define F2D                      "F2D"
#define F1I                      "F1I"
#define F2I                      "F2I"
#define F3I                      "F3I"
#define B2D                      "B2D"
#define F1L                      "F1L"
#define F1S                      "F1S"
#define F2L                      "F2L"

/*------------------------------------------------------------------------------
                                Enums
 *----------------------------------------------------------------------------*/

/**
  @brief Available KMOS frame categories.
*/
enum kmo_frame_type {
    /* Default initialisation frame type */
    illegal_frame,
    detector_frame,
    list_frame,
    spectrum_frame,
    ifu_frame
};

/**
  @brief    Available KMOS frame types.
*/
enum kmo_fits_type {
    /* Default initialisation frame type */
    illegal_fits,   
    /* Raw frame */
    raw_fits,
    /* Obsolete */
    f1d_fits,
    /* processed float 2D detector frame */
    f2d_fits,
    /* processed float 2D detector frame, badpixel */
    b2d_fits,
    /* Table, LUT */
    f2l_fits,
    /* processed 1D IFU vector */
    f1i_fits,
    /* Table with two columns (e.g. wavelength, strength) */
    f1l_fits,
    /* Spectrum, one dimensional */
    f1s_fits,
    /* processed 2D IFU image */
    f2i_fits,
    /* processed 3D IFU cube */
    f3i_fits
};

/**
    @brief IDL-like comparative operators
*/
enum idl_rel_ops {eq, ne, ge, gt, le, lt};

/*------------------------------------------------------------------------------
 *                        structs
 *----------------------------------------------------------------------------*/

typedef struct {
    int     ext_nr;
    int     valid_data;
    int     is_noise;
    int     is_badpix;
    int     device_nr;
} sub_fits_desc;

typedef struct {
    enum kmo_fits_type  fits_type;
    enum kmo_frame_type frame_type;
    int                 naxis;
    int                 naxis1;
    int                 naxis2;
    int                 naxis3;
    int                 ex_noise;
    int                 ex_badpix;
    int                 nr_ext;
    sub_fits_desc   *   sub_desc; 
} main_fits_desc;

/*------------------------------------------------------------------------------
 *                        Prototypes
 *----------------------------------------------------------------------------*/

const char * kmos_get_license(void) ;

const char * kmos_get_base_name(const char *) ;

cpl_error_code kmos_check_and_set_groups(
  cpl_frameset *frameset);

int kmos_count_raw_in_frameset(const cpl_frameset *) ;

cpl_frameset * kmos_extract_frameset(
        const cpl_frameset  *   in,
        const char          *   tag) ;

cpl_image * kmos_illum_load(
        const char  *   filename,
        cpl_type        im_type,
        int             ifu_nr,
        double          angle,
        double  *       angle_found) ;

char * kmos_get_reflex_suffix(
        int             mapping_id,
        const char  *   ifus_txt,
        const char  *   name,
        const char  *   obj_name) ;
int kmos_collapse_cubes(const char *, cpl_frameset *, cpl_parameterlist *,
		double, const char *, const char *, double, double, int, int, int) ;
cpl_frame * kmos_get_angle_frame(cpl_frameset *, int, const char *) ;
cpl_frameset * kmos_get_angle_frameset(cpl_frameset *, int, const char *) ;
cpl_frameset * kmos_purge_wrong_angles_frameset(cpl_frameset *, int, 
        const char *) ;

int * kmos_get_angles(cpl_frameset *, int *, const char *) ;

int kmos_3dim_clean_plist(cpl_propertylist *) ;
int kmos_all_clean_plist(cpl_propertylist *) ;


cpl_error_code      kmo_cut_endings(cpl_vector** vec,
                                      int *begin,
                                      int *end,
                                      int cut);
cpl_error_code      kmo_easy_gaussfit(const cpl_vector *x,
                                      const cpl_vector *y,
                                      double *x0,
                                      double *sigma,
                                      double *area,
                                      double *offset);
cpl_vector*         kmo_polyfit_1d(   cpl_vector *x,
                                      const cpl_vector *y,
                                      const int degree);
double              kmo_to_deg(double val);

/* ---------------------- enum-conversion functions ------------------------- */
enum kmo_frame_type kmo_string_to_frame_type(const char* frame_type_str);

/* ---------------------- fits-descriptor functions ------------------------- */
main_fits_desc      kmo_identify_fits_header(const char *filename);
sub_fits_desc       kmo_identify_fits_sub_header(int ext_nr,
                                                 int valid_data,
                                                 int is_noise,
                                                 int is_badpix,
                                                 int id);
int                 kmo_check_indices(int *id, int nr_id, int ex_noise);
void                kmo_free_fits_desc(main_fits_desc *desc);
void                kmo_init_fits_desc(main_fits_desc *desc);
void                kmo_init_fits_sub_desc(sub_fits_desc *desc);

/* ---------------------- idl-functions ------------------------------------- */
cpl_vector*         kmo_idl_where(const cpl_vector *data,
                                      double val,
                                      int op);
cpl_vector*         kmo_idl_values_at_indices(const cpl_vector *data,
                                      const cpl_vector *indices);

/* ---------------------- other --------------------------------------------- */
cpl_array**         kmo_get_unused_ifus(cpl_frameset *frameset,
                                        int omit_telluric,
                                        int omit_illum);
cpl_error_code      kmo_set_unused_ifus(cpl_array **unused,
                                        cpl_propertylist *header,
                                        const char *recipe_name);
cpl_array**         kmo_duplicate_unused_ifus(cpl_array **unused);
void                kmo_print_unused_ifus(cpl_array **unused, int after);
void                kmo_free_unused_ifus(cpl_array **unused);
char**              kmo_strsplit(const char *str, const char *delimiter, int *size);
void                kmo_strfreev(char **strarr);
char*               kmo_strlower(char *);
char*               kmo_strupper(char *);

#endif
