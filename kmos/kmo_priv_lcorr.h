/* $Id: kmo_priv_lcorr.h,v 1.8 2013-09-16 13:40:02 aagudo Exp $
 *
 * This file is part of the KMOS Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: aagudo $
 * $Date: 2013-09-16 13:40:02 $
 * $Revision: 1.8 $
 * $Name: not supported by cvs2svn $
 */

#ifndef KMOS_PRIV_LCORR_H
#define KMOS_PRIV_LCORR_H

/*-----------------------------------------------------------------------------
 *                              Includes
 *----------------------------------------------------------------------------*/

#include <cpl.h>

#include "kmclipm_functions.h"

/*-----------------------------------------------------------------------------
 *              Types
 *-----------------------------------------------------------------------------*/
extern int print_info_once_oh_ref;
/*------------------------------------------------------------------------------
 *                        Prototypes
 *----------------------------------------------------------------------------*/

cpl_polynomial *kmo_lcorr_get(const cpl_imagelist *cube,
                              const cpl_propertylist *header,
                              const cpl_frame *ref_spectrum_frame,
                              const gridDefinition gd,
                              const char *filter_id,
                              int ifu_id);

cpl_bivector *kmo_lcorr_extract_spectrum (const cpl_imagelist *cube,
        const cpl_propertylist *header,
        const double min_frac,
        const cpl_vector *range);

cpl_image *kmo_lcorr_create_object_mask (const cpl_imagelist *cube,
        float min_frac,
        const cpl_vector *lambda,
        const cpl_vector *range);

cpl_vector *kmo_lcorr_create_lambda_vector (const cpl_propertylist *header);

cpl_bivector *kmo_lcorr_read_reference_spectrum (const char *filename, const cpl_vector *lambda);

cpl_array *kmo_lcorr_get_peak_positions(const cpl_bivector *spectrum,
        double min_frac,
        cpl_vector *range);

cpl_vector *kmo_lcorr_get_peak_lambdas (const cpl_bivector *spectrum,
        double min_frac,
        cpl_vector *range);

cpl_polynomial *kmo_lcorr_crosscorrelate_spectra(cpl_bivector *object,
        cpl_bivector *reference,
        cpl_vector *peaks,
        const char *band_id);

void kmo_lcorr_open_debug_file(char* filename);
void kmo_lcorr_close_debug_file(void);

#endif
