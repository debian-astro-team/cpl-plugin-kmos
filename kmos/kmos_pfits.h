/*
 * This file is part of the KMOS Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef KMOS_PFITS_H
#define KMOS_PFITS_H

/*-----------------------------------------------------------------------------
   								Includes
 -----------------------------------------------------------------------------*/

#include <cpl.h>

/*-----------------------------------------------------------------------------
   								Functions prototypes
 -----------------------------------------------------------------------------*/

int kmos_pfits_get_naxis1(const cpl_propertylist *) ;
int kmos_pfits_get_naxis2(const cpl_propertylist *) ;
int kmos_pfits_get_naxis3(const cpl_propertylist *) ;
int kmos_pfits_get_ndit(const cpl_propertylist *) ;
int kmos_pfits_get_obs_id(const cpl_propertylist *) ;
int kmos_pfits_get_qc_combined_cubes_nb(const cpl_propertylist *) ;
int kmos_pfits_get_datancom(const cpl_propertylist *) ;

double kmos_pfits_get_dit(const cpl_propertylist *) ;
double kmos_pfits_get_rotangle(const cpl_propertylist *) ;
double kmos_pfits_get_exptime(const cpl_propertylist *) ;
double kmos_pfits_get_crval1(const cpl_propertylist *) ;
double kmos_pfits_get_cdelt1(const cpl_propertylist *) ;
double kmos_pfits_get_crval3(const cpl_propertylist *) ;
double kmos_pfits_get_crpix3(const cpl_propertylist *) ;
double kmos_pfits_get_cd3_3(const cpl_propertylist *) ;
double kmos_pfits_get_qc_ar_vscale(const cpl_propertylist *) ;
double kmos_pfits_get_qc_ne_vscale(const cpl_propertylist *) ;
double kmos_pfits_get_qc_ar_fwhm_mean(const cpl_propertylist *) ;
double kmos_pfits_get_qc_ne_fwhm_mean(const cpl_propertylist *) ;
double kmos_pfits_get_qc_ar_pos_stdev(const cpl_propertylist *) ;
double kmos_pfits_get_qc_ne_pos_stdev(const cpl_propertylist *) ;
double kmos_pfits_get_qc_expmask_avg(const cpl_propertylist *) ;
double kmos_pfits_get_mjd_obs(const cpl_propertylist *) ;
double kmos_pfits_get_pro_mjd_obs(const cpl_propertylist *) ;
double kmos_pfits_get_ia_fwhmlin(const cpl_propertylist *) ;
double kmos_pfits_get_airmass_start(const cpl_propertylist *) ;
double kmos_pfits_get_airmass_end(const cpl_propertylist *) ;
 
const char * kmos_pfits_get_obs_targ_name(const cpl_propertylist *) ;
const char * kmos_pfits_get_reflex_suffix(const cpl_propertylist *) ;
const char * kmos_pfits_get_pro_date_obs(const cpl_propertylist *) ;
const char * kmos_pfits_get_date_obs(const cpl_propertylist *) ;
const char * kmos_pfits_get_extname(const cpl_propertylist *) ;
const char * kmos_pfits_get_tplid(const cpl_propertylist *) ;
const char * kmos_pfits_get_progid(const cpl_propertylist *) ;
const char * kmos_pfits_get_arcfile(const cpl_propertylist *) ;
const char * kmos_pfits_get_readmode(const cpl_propertylist *) ;
const char * kmos_pfits_get_qc_cube_unit(const cpl_propertylist *) ;
const char * kmos_pfits_get_qc_collapse_name(const cpl_propertylist *) ;
const char * kmos_pfits_get_qc_expmask_name(const cpl_propertylist *) ;
const char * kmos_pfits_get_object(const cpl_propertylist *) ;

#endif
