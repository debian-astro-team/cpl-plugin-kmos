/* $Id: kmo_priv_stats.h,v 1.3 2012-06-29 10:26:44 aagudo Exp $
 *
 * This file is part of the KMOS Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: aagudo $
 * $Date: 2012-06-29 10:26:44 $
 * $Revision: 1.3 $
 * $Name: not supported by cvs2svn $
 */

#ifndef KMOS_PRIV_STATS_H
#define KMOS_PRIV_STATS_H

/*-----------------------------------------------------------------------------
 *                              Includes
 *----------------------------------------------------------------------------*/

#include <cpl.h>

#include "kmclipm_vector.h"

/*------------------------------------------------------------------------------
 *                        Prototypes
 *----------------------------------------------------------------------------*/

kmclipm_vector*     kmo_calc_stats_cube(const cpl_imagelist *data,
                                        const cpl_image *mask,
                                        double cpos_rej,
                                        double cneg_rej,
                                        int citer);

kmclipm_vector*     kmo_calc_stats_img(const cpl_image *data,
                                       const cpl_image *mask,
                                       double cpos_rej,
                                       double cneg_rej,
                                       int citer);

kmclipm_vector*     kmo_calc_stats_vec(kmclipm_vector *data,
                                       kmclipm_vector *mask,
                                       double cpos_rej,
                                       double cneg_rej,
                                       int citer);

int                 kmo_count_masked_pixels(const cpl_image *mask);

cpl_vector*         kmo_vector_identify_infinite(const cpl_vector *vec);

kmclipm_vector*     kmo_imagelist_to_vector(
                                   const cpl_imagelist *imglist,
                                   const cpl_image *mask,
                                   int *nr_mask_pix);

kmclipm_vector*     kmo_image_to_vector(
                                   const cpl_image *img,
                                   const cpl_image *mask,
                                   int *nr_mask_pix);

cpl_error_code      kmo_calc_mode(
                                   const kmclipm_vector *data,
                                   double *mode,
                                   double *noise,
                                   double cpos_rej,
                                   double cneg_rej,
                                   int citer);

cpl_vector*         kmo_reject_sigma(const cpl_vector *d,
                                   double center,
                                   double high,
                                   double low,
                                   double dev,
                                   cpl_vector **ret_index);

cpl_error_code      kmo_calc_remaining(
                                   kmclipm_vector *data_in_vec,
                                   kmclipm_vector *data_out,
                                   double cpos_rej,
                                   double cneg_rej,
                                   int citer, int nr_finite);

#endif /* KMOS_PRIV_STATS_H */
