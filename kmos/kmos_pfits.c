/*
 * This file is part of the KMOS Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

/*-----------------------------------------------------------------------------
   								Includes
 -----------------------------------------------------------------------------*/

#include <cpl.h>
#include <string.h>

#include "kmos_pfits.h"
#include "kmo_dfs.h"

/*----------------------------------------------------------------------------*/
/**
 * @defgroup kmos_pfits     FITS header protected access
 *
 */
/*----------------------------------------------------------------------------*/

/**@{*/

/*-----------------------------------------------------------------------------
  							Function codes
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the EXPTIME
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double kmos_pfits_get_exptime(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, "EXPTIME") ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the seeing
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double kmos_pfits_get_ia_fwhmlin(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, "ESO TEL IA FWHM") ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the airmass start
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double kmos_pfits_get_airmass_start(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, "ESO TEL AIRM START") ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the airmass end
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double kmos_pfits_get_airmass_end(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, "ESO TEL AIRM END") ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the position angle
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double kmos_pfits_get_rotangle(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, "ESO OCS ROT NAANGLE") ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the OBS TARG NAME
  @param    plist       property list to read from
  @return   pointer to statically allocated character string
 */
/*----------------------------------------------------------------------------*/
const char * kmos_pfits_get_obs_targ_name(const cpl_propertylist * plist)
{
    return (const char *)cpl_propertylist_get_string(plist,"ESO OBS TARG NAME");
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the PRO REFLEX SUFFIX
  @param    plist       property list to read from
  @return   pointer to statically allocated character string
 */
/*----------------------------------------------------------------------------*/
const char * kmos_pfits_get_reflex_suffix(const cpl_propertylist * plist)
{
    return (const char *)cpl_propertylist_get_string(plist,
            "ESO PRO REFLEX SUFFIX");
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the extname
  @param    plist       property list to read from
  @return   pointer to statically allocated character string
 */
/*----------------------------------------------------------------------------*/
const char * kmos_pfits_get_extname(const cpl_propertylist * plist)
{
    return (const char *) cpl_propertylist_get_string(plist, "EXTNAME");
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the arcfile   
  @param    plist       property list to read from
  @return   pointer to statically allocated character string
 */
/*----------------------------------------------------------------------------*/
const char * kmos_pfits_get_arcfile(const cpl_propertylist * plist)
{
    return (const char *) cpl_propertylist_get_string(plist, "ARCFILE");
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the OBJECT
  @param    plist       property list to read from
  @return   pointer to statically allocated character string
 */
/*----------------------------------------------------------------------------*/
const char * kmos_pfits_get_object(const cpl_propertylist * plist)
{
    return (const char *) cpl_propertylist_get_string(plist, "OBJECT");
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the expmask name
  @param    plist       property list to read from
  @return   pointer to statically allocated character string
 */
/*----------------------------------------------------------------------------*/
const char * kmos_pfits_get_qc_expmask_name(const cpl_propertylist * plist)
{
    return (const char *) cpl_propertylist_get_string(plist, 
            "ESO QC EXPMASK NAME");
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the collapse image name
  @param    plist       property list to read from
  @return   pointer to statically allocated character string
 */
/*----------------------------------------------------------------------------*/
const char * kmos_pfits_get_qc_collapse_name(const cpl_propertylist * plist)
{
    return (const char *) cpl_propertylist_get_string(plist, 
            "ESO QC COLLAPSE NAME");
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the QC COMBINED_CUBES NB
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
int kmos_pfits_get_qc_combined_cubes_nb(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_int(plist, "ESO QC COMBINED_CUBES NB");
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the QC EXPMASK AVG
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double kmos_pfits_get_qc_expmask_avg(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, "ESO QC EXPMASK AVG");
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the QC ARC AR VSCALE
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double kmos_pfits_get_qc_ar_vscale(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, "ESO QC ARC AR VSCALE");
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the QC ARC NE VSCALE
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double kmos_pfits_get_qc_ne_vscale(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, "ESO QC ARC NE VSCALE");
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the QC ARC AR FWHM MEAN
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double kmos_pfits_get_qc_ar_fwhm_mean(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, "ESO QC ARC AR FWHM MEAN");
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the QC ARC NE FWHM MEAN
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double kmos_pfits_get_qc_ne_fwhm_mean(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, "ESO QC ARC NE FWHM MEAN");
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the QC ARC NE POS STDEV
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double kmos_pfits_get_qc_ne_pos_stdev(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, "ESO QC ARC NE POS STDEV");
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the QC ARC AR POS STDEV
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double kmos_pfits_get_qc_ar_pos_stdev(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, "ESO QC ARC AR POS STDEV");
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the QC CUBE_UNIT
  @param    plist       property list to read from
  @return   pointer to statically allocated character string
 */
/*----------------------------------------------------------------------------*/
const char * kmos_pfits_get_qc_cube_unit(const cpl_propertylist * plist)
{
    return (const char *) cpl_propertylist_get_string(plist,"ESO QC CUBE_UNIT");
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the PROG ID
  @param    plist       property list to read from
  @return   pointer to statically allocated character string
 */
/*----------------------------------------------------------------------------*/
const char * kmos_pfits_get_progid(const cpl_propertylist * plist)
{
    return (const char *) cpl_propertylist_get_string(plist, 
            "ESO OBS PROG ID");
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the TPL ID
  @param    plist       property list to read from
  @return   pointer to statically allocated character string
 */
/*----------------------------------------------------------------------------*/
const char * kmos_pfits_get_tplid(const cpl_propertylist * plist)
{
    return (const char *) cpl_propertylist_get_string(plist, 
            "ESO TPL ID");
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the READMODE
  @param    plist       property list to read from
  @return   pointer to statically allocated character string
 */
/*----------------------------------------------------------------------------*/
const char * kmos_pfits_get_readmode(const cpl_propertylist * plist)
{
    return (const char *) cpl_propertylist_get_string(plist, 
            "ESO DET READ CURNAME");
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the NCOMBINE value
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
int kmos_pfits_get_datancom(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_int(plist, "ESO PRO DATANCOM")  ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the NAXIS1 value 
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
int kmos_pfits_get_naxis1(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_int(plist, "NAXIS1")  ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the NAXIS2 value 
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
int kmos_pfits_get_naxis2(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_int(plist, "NAXIS2")  ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the NAXIS3 value 
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
int kmos_pfits_get_naxis3(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_int(plist, "NAXIS3")  ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the DATE-OBS value 
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
const char * kmos_pfits_get_date_obs(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_string(plist, KEY_DATEOBS)  ;
}


/*----------------------------------------------------------------------------*/
/**
  @brief    find out the MJD-OBS value 
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double kmos_pfits_get_mjd_obs(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, KEY_MJDOBS)  ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the PRO DATE-OBS value 
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
const char * kmos_pfits_get_pro_date_obs(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_string(plist, PRO_DATE_OBS)  ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the PRO MJD-OBS value 
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double kmos_pfits_get_pro_mjd_obs(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, PRO_MJD_OBS)  ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the DIT value 
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double kmos_pfits_get_dit(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, "ESO DET SEQ1 DIT")  ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the NDIT value 
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
int kmos_pfits_get_ndit(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_int(plist, "ESO DET NDIT")  ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the OBS ID value 
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
int kmos_pfits_get_obs_id(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_int(plist, "ESO OBS ID")  ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the CRVAL1 value 
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double kmos_pfits_get_crval1(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, "CRVAL1")  ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the CDELT1 value 
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double kmos_pfits_get_cdelt1(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, "CDELT1")  ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the CRVAL3 value 
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double kmos_pfits_get_crval3(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, "CRVAL3")  ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the CRPIX3 value 
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double kmos_pfits_get_crpix3(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, "CRPIX3")  ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the CD3_3 value 
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double kmos_pfits_get_cd3_3(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, "CD3_3")  ;
}

/**@}*/
