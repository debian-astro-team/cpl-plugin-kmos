/* $Id: kmo_constants.h,v 1.5 2013-09-12 14:37:53 aagudo Exp $
 *
 * This file is part of the KMOS Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: aagudo $
 * $Date: 2013-09-12 14:37:53 $
 * $Revision: 1.5 $
 * $Name: not supported by cvs2svn $
 */

#ifndef KMOS_CONSTANTS_H
#define KMOS_CONSTANTS_H

/**
    @defgroup kmo_constants  KMOS specific defines

    @brief Definitions of constants specific to data reduction

    @par Synopsis:
    @code
      #include <kmo_constants.h>
    @endcode
    @{
*/

/*-----------------------------------------------------------------------------
                    KMOS device specific defines
 -----------------------------------------------------------------------------*/
#include "kmclipm_constants.h"
#include "kmclipm_functions.h"

/*-----------------------------------------------------------------------------
                    KMOS pipeline specific defines
 -----------------------------------------------------------------------------*/

// saturation
/** @brief The saturation threshold */
#define KMO_FLAT_SATURATED  50000

/** @brief The number of images in which a pixel must be saturated at least. */
#define KMO_FLAT_SAT_MIN    2

// rejection default values
/** @brief The default number of minimum pixels to reject */
#define DEF_NR_MIN_REJ      1
/** @brief The default number of maximum pixels to reject */
#define DEF_NR_MAX_REJ      1
/** @brief The default number of iterations for rejection */
#define DEF_ITERATIONS      3
/** @brief The positive rejection sigma */
#define DEF_POS_REJ_THRES   3.0
/** @brief The negative rejection sigma */
#define DEF_NEG_REJ_THRES   3.0
/** @brief The rejection method */
#define DEF_REJ_METHOD      "ksigma"

// reference lines for QC parameters in kmo_wave_cal
/** @brief The reference line in H band for Argon */
#define REF_LINE_AR_H       1.67446
/** @brief The reference line in H band for Neon */
#define REF_LINE_NE_H       1.71666
/** @brief The reference line in HK band for Argon */
#define REF_LINE_AR_HK      1.79196
/** @brief The reference line in HK band for Neon */
#define REF_LINE_NE_HK      1.80882
/** @brief The reference line in IZ band for Argon */
#define REF_LINE_AR_IZ      0.922703
/** @brief The reference line in IZ band for Neon */
#define REF_LINE_NE_IZ      0.865676
/** @brief The reference line in K band for Argon */
#define REF_LINE_AR_K       2.15401
/** @brief The reference line in K band for Neon */
#define REF_LINE_NE_K       2.25365
/** @brief The reference line in YJ band for Argon */
#define REF_LINE_AR_YJ      1.21430
/** @brief The reference line in YJ band for Neon */
#define REF_LINE_NE_YJ      1.17700

// spectral resolutions for all bands (microns/pixel)
/** @brief The spectral resolution for H band [um/pix] */
#define SPEC_RES_H          0.0002042
/** @brief The spectral resolution for HK band [um/pix] */
#define SPEC_RES_HK         0.0004427
/** @brief The spectral resolution for IZ band [um/pix] */
#define SPEC_RES_IZ         0.00014207
/** @brief The spectral resolution for K band [um/pix] */
#define SPEC_RES_K          0.000269
/** @brief The spectral resolution for YJ band [um/pix] */
#define SPEC_RES_YJ         0.00016558

// constants for zeropoint and throughput calculations
/** @brief Lambda min for throughput calculation in H Band */
#define LAMBDA_MIN_H     1.5365
/** @brief Lambda min for throughput calculation in K Band */
#define LAMBDA_MIN_K     2.028
/** @brief Lambda min for throughput calculation in Z Band */
#define LAMBDA_MIN_Z     0.985
/** @brief Lambda min for throughput calculation in J Band */
#define LAMBDA_MIN_J     1.154

/** @brief Lambda max for throughput calculation in H Band */
#define LAMBDA_MAX_H     1.7875
/** @brief Lambda max for throughput calculation in K Band */
#define LAMBDA_MAX_K     2.290
/** @brief Lambda max for throughput calculation in Z Band */
#define LAMBDA_MAX_Z     1.000
/** @brief Lambda max for throughput calculation in J Band */
#define LAMBDA_MAX_J     1.316

// Nph lambda [ photons/s/m2/um ]
/** @brief The throughput correction factor for H band */
#define NPH_LAMBDA_H        9.47e+9
/** @brief The throughput correction factor for K band */
#define NPH_LAMBDA_K        4.65e+9
/** @brief The throughput correction factor for Z band */
#define NPH_LAMBDA_Z        3.81e+10
/** @brief The throughput correction factor for J band */
#define NPH_LAMBDA_J        1.944e+10

/** @brief The effective VLT mirror area:
           outer primary mirror radius: 8.2m
           outer secondary mirror radius: 1.12m
           (8.2/2)^2*pi - (1.12/2)^2*pi
*/
#define MIRROR_AREA         51.8249690506

// Strings defining the labels and lines in the arcline list
/** @brief String defining the 1st column in the linelist */
#define WAVE_COL            "wavelength"
/** @brief String defining the 2nd column in the linelist */
#define STRE_COL            "strength"
/** @brief String defining the 3rd column in the linelist */
#define GAS_COL             "gas"
/** @brief String defining Argon for 3rd column in the linelist */
#define AR_LINE             "Ar"
/** @brief String defining Neon for 3rd column in the linelist */
#define NE_LINE             "Ne"

// Strings for guess polynom coefficients for wave length calibration
#define NR_COEFFS_COLS 4
#define BAND_COL "band"
#define IFU_COL "IFU"
#define A0_COL "a0"
#define A1_COL "a1"
#define A2_COL "a2"
#define A3_COL "a3"
#define COEFF_COLS A0_COL,A1_COL,A2_COL,A3_COL
#define NR_BANDS 5
#define BANDS "H","K","HK","IZ","YJ"

#define IFUS_USER_DEFINED "user_defined_IFUs"

/** @} */

#endif  /* KMO_CONSTANTS_H */
