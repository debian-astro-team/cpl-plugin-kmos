/*
 * This file is part of the KMOS Library
 * Copyright (C) 2007-2008 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "kmo_priv_make_image.h"
#include "kmo_utils.h"
#include "kmo_debug.h"

/**
    @defgroup kmo_priv_make_image_test   kmo_priv_make_image unit tests

    @{
*/

void kmo_test_verbose_off();
void kmo_test_verbose_on();
void kmo_test_fill_vector(cpl_vector *vec, float seed, float offset);

/**
    @brief   test for kmo_identify_slices_with_oh()
*/
void test_kmo_identify_slices_with_oh(void)
{
    cpl_vector  *data   = NULL,
                *lambda = NULL,
                *slices = NULL,
                *ranges = NULL;

    /* --- invalid tests --- */
    kmo_test_verbose_off();
    cpl_test_null(slices = kmo_identify_slices_with_oh(NULL, NULL, NULL,
                                               -1, 1, 0.8, 0.001, 10));
    cpl_test_error(CPL_ERROR_NULL_INPUT);
    data = cpl_vector_new(1000);
    kmo_test_fill_vector(data, 0.0, 1.0);
    cpl_test_null(slices = kmo_identify_slices_with_oh(data, NULL, NULL,
                                               -1, 1, 0.8, 0.001, 10));
    cpl_test_error(CPL_ERROR_NULL_INPUT);
    kmo_test_verbose_on();

    /* --- valid tests --- */
    lambda = cpl_vector_new(1000);
    kmo_test_fill_vector(lambda, 0.8, 0.0001);
    cpl_test_nonnull(slices = kmo_identify_slices_with_oh(data, lambda, NULL,
                                                  -1, 1, 0.8, 0.001, 10));
    cpl_test_eq(cpl_vector_get_size(slices), 10);
    cpl_test_eq(cpl_vector_get_mean(slices), 0.9);
    cpl_vector_delete(slices);
    cpl_test_error(CPL_ERROR_NONE);

    ranges = cpl_vector_new(2);
    cpl_vector_set(ranges, 0, 0.805);
    cpl_vector_set(ranges, 1, 0.809);

    cpl_test_nonnull(slices = kmo_identify_slices_with_oh(data, lambda, ranges,
                                                  -1, 1, 0.8, 0.001, 10));
    cpl_test_eq(cpl_vector_get_size(slices), 10);
    cpl_test_eq(cpl_vector_get_mean(slices), 0.5);
    cpl_vector_delete(slices);
    cpl_test_error(CPL_ERROR_NONE);

    cpl_test_nonnull(slices = kmo_identify_slices_with_oh(data, lambda, ranges,
                                                  60, 1, 0.8, 0.001, 10));
    cpl_test_eq(cpl_vector_get_size(slices), 10);
    cpl_test_eq(cpl_vector_get_mean(slices), 0.2);

    cpl_vector_delete(slices);
    cpl_test_error(CPL_ERROR_NONE);

    cpl_vector_delete(data);
    cpl_vector_delete(lambda);
    cpl_vector_delete(ranges);
}

/**
    @brief   Test of helper functions for kmo_make_image
*/
int main(void)
{
    cpl_test_init("<usd-help@eso.org>", CPL_MSG_WARNING);

    test_kmo_identify_slices_with_oh();

    return cpl_test_end(0);
}

/** @} */
