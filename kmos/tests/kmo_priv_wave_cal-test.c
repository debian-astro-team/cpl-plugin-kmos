/*
 * This file is part of the KMOS Library
 * Copyright (C) 2007-2008 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "kmo_priv_wave_cal.h"
#include "kmo_debug.h"

/**
    @defgroup kmo_priv_wave_cal_test   kmo_priv_wave_cal unit tests

    @{
*/

/**
    @brief   test kmo_calc_fitted_slitlet_edge()
*/
void test_kmo_calc_fitted_slitlet_edge(void)
{
    int left_edge   = 0,
        right_edge  = 0;

    // table of two rows (edges)
    cpl_table *tbl = cpl_table_new(2);
    cpl_table_new_column(tbl, "ID", CPL_TYPE_DOUBLE);
    cpl_table_new_column(tbl, "A0", CPL_TYPE_DOUBLE);
    cpl_table_new_column(tbl, "A1", CPL_TYPE_DOUBLE);
    cpl_table_new_column(tbl, "A2", CPL_TYPE_DOUBLE);
    cpl_table_new_column(tbl, "A3", CPL_TYPE_DOUBLE);

    // large values, edge will be outside 2048-badpix border
    cpl_table_set(tbl, "ID", 0, 1);
    cpl_table_set(tbl, "A0", 0, 0.1);
    cpl_table_set(tbl, "A1", 0, 0.2);
    cpl_table_set(tbl, "A2", 0, 0.3);
    cpl_table_set(tbl, "A3", 0, 0.4);

    cpl_table_set(tbl, "ID", 1, 1);
    cpl_table_set(tbl, "A0", 1, 0.5);
    cpl_table_set(tbl, "A1", 1, 0.6);
    cpl_table_set(tbl, "A2", 1, 0.7);
    cpl_table_set(tbl, "A3", 1, 0.8);

    left_edge  = (int)(kmo_calc_fitted_slitlet_edge(tbl, 0, 200)+0.5);
    right_edge = (int)(kmo_calc_fitted_slitlet_edge(tbl, 1, 200)+0.5);

    cpl_test_eq(left_edge, 2043);
    cpl_test_eq(right_edge, 2043);

    // large values, edge will be outside 2048-badpix border
    cpl_table_set(tbl, "ID", 0, 1);
    cpl_table_set(tbl, "A0", 0, 0.00001);
    cpl_table_set(tbl, "A1", 0, 0.00002);
    cpl_table_set(tbl, "A2", 0, 0.00003);
    cpl_table_set(tbl, "A3", 0, 0.00004);

    cpl_table_set(tbl, "ID", 1, 1);
    cpl_table_set(tbl, "A0", 1, 0.00005);
    cpl_table_set(tbl, "A1", 1, 0.00006);
    cpl_table_set(tbl, "A2", 1, 0.00007);
    cpl_table_set(tbl, "A3", 1, 0.00008);

    left_edge  = (int)(kmo_calc_fitted_slitlet_edge(tbl, 0, 200)+0.5);
    right_edge = (int)(kmo_calc_fitted_slitlet_edge(tbl, 1, 200)+0.5);

    cpl_test_eq(left_edge, 321);
    cpl_test_eq(right_edge, 643);

    cpl_table_delete(tbl);

    cpl_test_error(CPL_ERROR_NONE);
}

int main(void)
{
    cpl_test_init("<usd-help@eso.org>", CPL_MSG_WARNING);

//    test_kmo_calc_wave_calib();
//    test_kmo_extract_initial_trace();
//    test_kmo_estimate_lines();
//    test_kmo_get_lines();
//    test_kmo_extrapolate_arclines();
//    test_kmo_fit_arcline();
//    test_kmo_fit_spectrum();
//    test_kmo_fit_spectrum_1();
//    test_kmo_fit_spectrum_2();
//    test_kmo_reconstructed_arc_image();
    test_kmo_calc_fitted_slitlet_edge();
//    test_kmo_image_get_saturated();
//    test_kmo_get_filter_setup();

    return cpl_test_end(0);
}

/** @} */

