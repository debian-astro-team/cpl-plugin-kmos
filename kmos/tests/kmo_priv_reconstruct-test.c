/*
 * This file is part of the KMOS Library
 * Copyright (C) 2007-2008 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "kmclipm_constants.h"

#include "kmo_priv_reconstruct.h"
#include "kmo_utils.h"
#include "kmo_debug.h"

/**
    @defgroup kmo_priv_reconstruct_test   kmo_priv_reconstruct unit tests

    @{
*/

void kmo_test_verbose_off();
void kmo_test_verbose_on();

/**
    @brief   test for kmo_calc_flux_in()
*/
void test_kmo_calc_flux_in(void)
{
    float tol = 0.01;

    cpl_image  *data = cpl_image_new(2, 2, CPL_TYPE_FLOAT);
    cpl_image  *xcal = cpl_image_new(2, 2, CPL_TYPE_FLOAT);
    cpl_image *xcal_mask = NULL;

    cpl_image_set(data, 1, 1, .3);
    cpl_image_set(data, 1, 2, .1);
    cpl_image_set(data, 2, 1, .6);
    cpl_image_set(data, 2, 2, .2);

    cpl_image_set(xcal, 1, 1, 1.3);
    cpl_image_set(xcal, 1, 2, 30000.3);
    cpl_image_set(xcal, 2, 1, 11.6);
    cpl_image_set(xcal, 2, 2, 12.7);

    /* --- invalid tests --- */
    kmo_test_verbose_off();
    cpl_test_eq(0, kmo_calc_flux_in(NULL, NULL));
    cpl_test_error(CPL_ERROR_NULL_INPUT);

    cpl_test_eq(0, kmo_calc_flux_in(data, NULL));
    cpl_test_error(CPL_ERROR_NULL_INPUT);
    kmo_test_verbose_on();

    /* --- valid tests --- */
    xcal_mask = kmo_create_mask_from_xcal(xcal, 3);
    cpl_test_abs(kmo_calc_flux_in(data, xcal_mask), 0.4, tol);
    cpl_image_delete(xcal_mask);
    cpl_test_error(CPL_ERROR_NONE);

    xcal_mask = kmo_create_mask_from_xcal(xcal, 6);
    cpl_test_abs(kmo_calc_flux_in(data, xcal_mask), 0.6, tol);
    cpl_image_delete(xcal_mask);
    cpl_test_error(CPL_ERROR_NONE);

    xcal_mask = kmo_create_mask_from_xcal(xcal, 7);
    cpl_test_abs(kmo_calc_flux_in(data, xcal_mask), 0.2, tol);
    cpl_image_delete(xcal_mask);
    cpl_test_error(CPL_ERROR_NONE);

    cpl_image_delete(data); data = NULL;
    cpl_image_delete(xcal); xcal = NULL;
    cpl_test_error(CPL_ERROR_NONE);
}

/**
    @brief   test for kmo_calc_wcs_gd()
*/
void test_kmo_calc_wcs_gd(void)
{
    gridDefinition gd;
    gd.lamdaDistanceScale    = 1.0;
    gd.x.start               = -KMOS_PIXEL_RANGE/2;
    gd.x.delta               = KMOS_PIXEL_RESOLUTION;
    gd.x.dim                 = KMOS_SLITLET_X;
    gd.y.start               = -KMOS_PIXEL_RANGE/2;
    gd.y.delta               = KMOS_PIXEL_RESOLUTION;
    gd.y.dim                 = KMOS_SLITLET_Y;
    gd.l.dim                 = kmclipm_band_samples;
    gd.neighborHood.distance = 1.001;
    gd.neighborHood.scale    = PIXEL;
    gd.neighborHood.type     = N_CUBE;
    gd.method                = CUBIC_SPLINE;
    gd.rot_na_angle          = 0.;
    gd.rot_off_angle         = 0.;
    gd.l.start               = -1;
    gd.l.delta               = -1;


    /* Illegal inputs */

    kmo_test_verbose_off();

      kmo_calc_wcs_gd(NULL, NULL, -1, gd);
      cpl_test_error(CPL_ERROR_NULL_INPUT);

      char *my_path = cpl_sprintf("%s/ref_data/head.fits", getenv("srcdir"));
      cpl_propertylist *pl1 = kmclipm_propertylist_load(my_path, 0);
      cpl_propertylist_update_double(pl1, "ESO OCS TARG DITHA", 0);
      cpl_propertylist_update_double(pl1, "ESO OCS TARG DITHD", 0);
      kmo_calc_wcs_gd(pl1, NULL, -1, gd);
      cpl_test_error(CPL_ERROR_NULL_INPUT);

      cpl_propertylist *pl2 = kmclipm_propertylist_load(my_path, 1);
      cpl_free(my_path);
      kmo_calc_wcs_gd(pl1, pl2, -1, gd);
      cpl_test_error(CPL_ERROR_ILLEGAL_INPUT);

    kmo_test_verbose_on();


    /* Legal inputs */

    kmo_calc_wcs_gd(pl1, pl2, 2, gd);
    cpl_test_error(CPL_ERROR_NONE);

    gd.l.start = 10.0;
    gd.l.delta = 0.01;
    kmo_calc_wcs_gd(pl1, pl2, 2, gd);
    cpl_test_error(CPL_ERROR_NONE);


    /* Test values */

    cpl_test_abs(0.0444875, cpl_propertylist_get_double(pl2, CRVAL1), 0.0001);
    cpl_test_abs(-84.9851, cpl_propertylist_get_double(pl2, CRVAL2), 0.0001);
    cpl_test_abs(10, cpl_propertylist_get_double(pl2, CRVAL3), 0.0001);
    cpl_test_abs(7.5, cpl_propertylist_get_double(pl2, CRPIX1), 0.0001);
    cpl_test_abs(7.5, cpl_propertylist_get_double(pl2, CRPIX2), 0.0001);
    cpl_test_abs(1, cpl_propertylist_get_double(pl2, CRPIX3), 0.0001);
    cpl_test_abs(-5.55556e-05, cpl_propertylist_get_double(pl2, CDELT1), 0.00001);
    cpl_test_abs(5.55556e-05, cpl_propertylist_get_double(pl2, CDELT2), 0.00001);
    cpl_test_abs(0.01, cpl_propertylist_get_double(pl2, CDELT3), 0.00001);
    cpl_test_eq_string("RA---TAN", cpl_propertylist_get_string(pl2, CTYPE1));
    cpl_test_eq_string("DEC--TAN", cpl_propertylist_get_string(pl2, CTYPE2));
    cpl_test_eq_string("WAVE", cpl_propertylist_get_string(pl2, CTYPE3));
    cpl_test_abs(-5.55556e-05, cpl_propertylist_get_double(pl2, CD1_1), 0.00001);
    cpl_test_abs(0, cpl_propertylist_get_double(pl2, CD1_2), 0.00001);
    cpl_test_abs(0, cpl_propertylist_get_double(pl2, CD1_3), 0.00001);
    cpl_test_abs(0, cpl_propertylist_get_double(pl2, CD2_1), 0.00001);
    cpl_test_abs(5.55556e-05, cpl_propertylist_get_double(pl2, CD2_2), 0.00001);
    cpl_test_abs(0, cpl_propertylist_get_double(pl2, CD2_3), 0.00001);
    cpl_test_abs(0, cpl_propertylist_get_double(pl2, CD3_1), 0.00001);
    cpl_test_abs(0, cpl_propertylist_get_double(pl2, CD3_2), 0.00001);
    cpl_test_abs(0.01, cpl_propertylist_get_double(pl2, CD3_3), 0.00001);
    cpl_test_eq_string("H", cpl_propertylist_get_string(pl2, "ESO INS FILT1 ID"));

    cpl_test_error(CPL_ERROR_NONE);


    /* Clean up */
    cpl_propertylist_delete(pl1);
    cpl_propertylist_delete(pl2);
}

/**
    @brief   test for kmo_save_det_img_ext()
*/
void test_kmo_save_det_img_ext(void)
{

    cpl_test_error(CPL_ERROR_NONE);
}

/**
    @brief   Test of helper functions for kmo_reconstruct
*/
int main(void)
{
    cpl_test_init("<usd-help@eso.org>", CPL_MSG_WARNING);

    test_kmo_calc_flux_in();
    test_kmo_calc_wcs_gd();
    test_kmo_save_det_img_ext();

    return cpl_test_end(0);
}

/** @} */
