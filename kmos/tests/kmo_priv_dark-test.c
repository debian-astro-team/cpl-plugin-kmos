/*
 * This file is part of the KMOS Library
 * Copyright (C) 2007-2008 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <math.h>

#include <cpl_plot.h>

#include "kmo_priv_dark.h"
#include "kmo_cpl_extensions.h"
#include "kmo_debug.h"

/**
    @defgroup kmo_priv_dark_test   kmo_priv_dark unit tests

    @{
*/

void kmo_test_verbose_off();
void kmo_test_verbose_on();

/**
    @brief   test for kmo_create_bad_pix_dark()
*/
void test_kmo_create_bad_pix_dark(void)
{
    cpl_image  *img    = NULL,
               *badpix = NULL;
    float      tol     = 0.01;

    kmo_test_verbose_off();
    cpl_test_eq(-1, kmo_create_bad_pix_dark(NULL, -1.0, -1.0, -1.0, -1.0,
                                           NULL));
    cpl_test_error(CPL_ERROR_NULL_INPUT);
    img = cpl_image_new(10, 10, CPL_TYPE_FLOAT);
    cpl_test_eq(-1, kmo_create_bad_pix_dark(img, -1.0, -1.0, -1.0, -1.0,
                                           NULL));
    cpl_test_error(CPL_ERROR_NULL_INPUT);
    cpl_test_eq(-1, kmo_create_bad_pix_dark(img, 1.0, -1.0, -1.0, -1.0,
                                           NULL));
    cpl_test_error(CPL_ERROR_NULL_INPUT);
    cpl_test_eq(-1, kmo_create_bad_pix_dark(img, 1.0, 1.0, -1.0, -1.0,
                                           NULL));
    cpl_test_error(CPL_ERROR_NULL_INPUT);
    cpl_test_eq(-1, kmo_create_bad_pix_dark(img, 1.0, 1.0, 1.0, -1.0,
                                           NULL));
    cpl_test_error(CPL_ERROR_NULL_INPUT);
    cpl_test_eq(-1, kmo_create_bad_pix_dark(img, 1.0, 1.0, 1.0, 1.0,
                                           NULL));
    cpl_test_error(CPL_ERROR_NULL_INPUT);
    kmo_test_verbose_on();

    cpl_image_set(img, 5, 5, 0.);
    cpl_image_set(img, 5, 6, 1.);
    cpl_image_set(img, 6, 5, 100.);
    cpl_image_set(img, 6, 6, -10.);

    cpl_test_eq(1, kmo_create_bad_pix_dark(img,
                                            cpl_image_get_mean(img),
                                            cpl_image_get_stdev(img),
                                            2.0, 2.0,
                                            &badpix));
    cpl_test_error(CPL_ERROR_NONE);

    cpl_test_abs(cpl_image_get_mean(badpix), 99./100., tol);

    cpl_image_delete(img);
    cpl_image_delete(badpix);

    cpl_test_error(CPL_ERROR_NONE);
}

/**
    @brief   test for kmo_add_bad_pix_border()
*/
void test_kmo_add_bad_pix_border(void)
{
    int tmp;

    kmo_test_verbose_off();
    cpl_test_null(kmo_add_bad_pix_border(NULL, -1));
    cpl_test_error(CPL_ERROR_NULL_INPUT);

    cpl_image *in = cpl_image_new(2,2,CPL_TYPE_FLOAT),
              *out = NULL;
    cpl_image_set(in, 1, 1, 1);
    cpl_image_set(in, 1, 2, 2);
    cpl_image_set(in, 2, 1, 3);
    cpl_image_set(in, 2, 2, 4);

    cpl_test_null(kmo_add_bad_pix_border(in, -1));
    cpl_test_error(CPL_ERROR_ILLEGAL_INPUT);
    kmo_test_verbose_on();

    cpl_test_nonnull(out = kmo_add_bad_pix_border(in, FALSE));
    cpl_test_eq(10, cpl_image_get_size_x(out));
    cpl_test_eq(10, cpl_image_get_size_y(out));
    cpl_test_eq(0, cpl_image_get(out, 1, 1, &tmp));
    cpl_test_eq(0, tmp);
    cpl_test_eq(0, cpl_image_get(out, 2, 2, &tmp));
    cpl_test_eq(0, tmp);
    cpl_test_eq(0, cpl_image_get(out, 3, 3, &tmp));
    cpl_test_eq(0, tmp);
    cpl_test_eq(0, cpl_image_get(out, 4, 4, &tmp));
    cpl_test_eq(0, tmp);
    cpl_test_eq(1, cpl_image_get(out, 5, 5, &tmp));
    cpl_test_eq(2, cpl_image_get(out, 5, 6, &tmp));
    cpl_test_eq(3, cpl_image_get(out, 6, 5, &tmp));
    cpl_test_eq(4, cpl_image_get(out, 6, 6, &tmp));
    cpl_test_eq(0, cpl_image_get(out, 10, 10, &tmp));
    cpl_test_eq(0, tmp);
    cpl_image_delete(out);
    cpl_test_error(CPL_ERROR_NONE);

    cpl_test_nonnull(out = kmo_add_bad_pix_border(in, TRUE));
    cpl_test_eq(10, cpl_image_get_size_x(out));
    cpl_test_eq(10, cpl_image_get_size_y(out));
    cpl_test_eq(0, cpl_image_get(out, 1, 1, &tmp));
    cpl_test_eq(1, tmp);
    cpl_test_eq(0, cpl_image_get(out, 2, 2, &tmp));
    cpl_test_eq(1, tmp);
    cpl_test_eq(0, cpl_image_get(out, 3, 3, &tmp));
    cpl_test_eq(1, tmp);
    cpl_test_eq(0, cpl_image_get(out, 4, 4, &tmp));
    cpl_test_eq(1, tmp);
    cpl_test_eq(1, cpl_image_get(out, 5, 5, &tmp));
    cpl_test_eq(2, cpl_image_get(out, 5, 6, &tmp));
    cpl_test_eq(3, cpl_image_get(out, 6, 5, &tmp));
    cpl_test_eq(4, cpl_image_get(out, 6, 6, &tmp));
    cpl_test_eq(0, cpl_image_get(out, 10, 10, &tmp));
    cpl_test_eq(1, tmp);
    cpl_image_delete(out);
    cpl_test_error(CPL_ERROR_NONE);

    cpl_image_delete(in);
}

/**
    @brief   Test of helper functions for kmo_dark
*/
int main(void)
{
    cpl_test_init("<usd-help@eso.org>", CPL_MSG_WARNING);

    test_kmo_create_bad_pix_dark();
    test_kmo_add_bad_pix_border();

    return cpl_test_end(0);
}

/** @} */
