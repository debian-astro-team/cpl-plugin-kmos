## Process this file with automake to produce Makefile.in

##   This file is part of the UVES Pipeline Library
##   Copyright (C) 2002,2003 European Southern Observatory

##   This library is free software; you can redistribute it and/or modify
##   it under the terms of the GNU General Public License as published by
##   the Free Software Foundation; either version 2 of the License, or
##   (at your option) any later version.

##   This program is distributed in the hope that it will be useful,
##   but WITHOUT ANY WARRANTY; without even the implied warranty of
##   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##   GNU General Public License for more details.

##   You should have received a copy of the GNU General Public License
##   along with this program; if not, write to the Free Software
##   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

AUTOMAKE_OPTIONS = 1.8 foreign

DISTCLEANFILES = *~

EXTRA_DIST = ref_data

if MAINTAINER_MODE

MAINTAINERCLEANFILES = Makefile.in

endif

INCLUDES = $(all_includes) -I../../kmclipm/include/

LDADD = $(LIBCPLDFS) $(LIBCPLUI) $(LIBCPLDRS) $(LIBCPLCORE) $(LIBKMOS)

# Test programs
check_PROGRAMS = \
                kmo_cpl_extensions-test \
                kmo_debug-test \
                kmo_dfs-test \
                kmo_functions-test \
                kmo_priv_arithmetic-test \
                kmo_priv_combine-test \
                kmo_priv_copy-test \
                kmo_priv_dark-test \
                kmo_priv_extract_spec-test \
                kmo_priv_fit_profile-test \
                kmo_priv_fits_check-test \
                kmo_priv_fits_stack-test \
                kmo_priv_flat-test \
                kmo_priv_functions-test \
                kmo_priv_lcorr-test \
                kmo_priv_make_image-test \
                kmo_priv_noise_map-test \
                kmo_priv_reconstruct-test \
                kmo_priv_rotate-test \
                kmo_priv_shift-test \
                kmo_priv_sky_mask-test \
                kmos_priv_sky_tweak-test \
                kmo_priv_stats-test \
                kmo_priv_std_star-test \
                kmo_priv_wave_cal-test \
                kmo_utils-test

kmo_cpl_extensions_test_SOURCES = create_data_kmos.c kmo_cpl_extensions-test.c
kmo_cpl_extensions_test_LDFLAGS = $(CPL_LDFLAGS)
kmo_cpl_extensions_test_LDADD = $(LDADD)

kmo_debug_test_SOURCES = create_data_kmos.c kmo_debug-test.c
kmo_debug_test_LDFLAGS = $(CPL_LDFLAGS)
kmo_debug_test_LDADD = $(LDADD)

kmo_dfs_test_SOURCES = create_data_kmos.c kmo_dfs-test.c
kmo_dfs_test_LDFLAGS = $(CPL_LDFLAGS)
kmo_dfs_test_LDADD = $(LDADD)

kmo_functions_test_SOURCES = kmo_functions-test.c
kmo_functions_test_LDFLAGS = $(CPL_LDFLAGS)
kmo_functions_test_LDADD = $(LDADD)

kmo_priv_arithmetic_test_SOURCES = create_data_kmos.c kmo_priv_arithmetic-test.c
kmo_priv_arithmetic_test_LDFLAGS = $(CPL_LDFLAGS)
kmo_priv_arithmetic_test_LDADD = $(LDADD)

kmo_priv_combine_test_SOURCES = create_data_kmos.c kmo_priv_combine-test.c
kmo_priv_combine_test_LDFLAGS = $(CPL_LDFLAGS)
kmo_priv_combine_test_LDADD = $(LDADD)

kmo_priv_copy_test_SOURCES = create_data_kmos.c kmo_priv_copy-test.c
kmo_priv_copy_test_LDFLAGS = $(CPL_LDFLAGS)
kmo_priv_copy_test_LDADD = $(LDADD)

kmo_priv_dark_test_SOURCES = create_data_kmos.c kmo_priv_dark-test.c
kmo_priv_dark_test_LDFLAGS = $(CPL_LDFLAGS)
kmo_priv_dark_test_LDADD = $(LDADD)

kmo_priv_extract_spec_test_SOURCES = create_data_kmos.c kmo_priv_extract_spec-test.c
kmo_priv_extract_spec_test_LDFLAGS = $(CPL_LDFLAGS)
kmo_priv_extract_spec_test_LDADD = $(LDADD)

kmo_priv_fit_profile_test_SOURCES = kmo_priv_fit_profile-test.c
kmo_priv_fit_profile_test_LDFLAGS = $(CPL_LDFLAGS)
kmo_priv_fit_profile_test_LDADD = $(LDADD)

kmo_priv_fits_check_test_SOURCES = create_data_kmos.c kmo_priv_fits_check-test.c
kmo_priv_fits_check_test_LDFLAGS = $(CPL_LDFLAGS)
kmo_priv_fits_check_test_LDADD = $(LDADD)

kmo_priv_fits_stack_test_SOURCES = create_data_kmos.c kmo_priv_fits_stack-test.c
kmo_priv_fits_stack_test_LDFLAGS = $(CPL_LDFLAGS)
kmo_priv_fits_stack_test_LDADD = $(LDADD)

kmo_priv_flat_test_SOURCES = create_data_kmos.c kmo_priv_flat-test.c
kmo_priv_flat_test_LDFLAGS = $(CPL_LDFLAGS)
kmo_priv_flat_test_LDADD = $(LDADD)

kmo_priv_functions_test_SOURCES = create_data_kmos.c kmo_priv_functions-test.c
kmo_priv_functions_test_LDFLAGS = $(CPL_LDFLAGS)
kmo_priv_functions_test_LDADD = $(LDADD)

kmo_priv_lcorr_test_SOURCES = create_data_kmos.c kmo_priv_lcorr-test.c
kmo_priv_lcorr_test_LDFLAGS = $(CPL_LDFLAGS)
kmo_priv_lcorr_test_LDADD = $(LDADD)

kmo_priv_make_image_test_SOURCES = create_data_kmos.c kmo_priv_make_image-test.c
kmo_priv_make_image_test_LDFLAGS = $(CPL_LDFLAGS)
kmo_priv_make_image_test_LDADD = $(LDADD)

kmo_priv_noise_map_test_SOURCES = create_data_kmos.c kmo_priv_noise_map-test.c
kmo_priv_noise_map_test_LDFLAGS = $(CPL_LDFLAGS)
kmo_priv_noise_map_test_LDADD = $(LDADD)

kmo_priv_reconstruct_test_SOURCES = create_data_kmos.c kmo_priv_reconstruct-test.c
kmo_priv_reconstruct_test_LDFLAGS = $(CPL_LDFLAGS)
kmo_priv_reconstruct_test_LDADD = $(LDADD)

kmo_priv_rotate_test_SOURCES = create_data_kmos.c kmo_priv_rotate-test.c
kmo_priv_rotate_test_LDFLAGS = $(CPL_LDFLAGS)
kmo_priv_rotate_test_LDADD = $(LDADD)

kmo_priv_shift_test_SOURCES = create_data_kmos.c kmo_priv_shift-test.c
kmo_priv_shift_test_LDFLAGS = $(CPL_LDFLAGS)
kmo_priv_shift_test_LDADD = $(LDADD)

kmo_priv_sky_mask_test_SOURCES = create_data_kmos.c kmo_priv_sky_mask-test.c
kmo_priv_sky_mask_test_LDFLAGS = $(CPL_LDFLAGS)
kmo_priv_sky_mask_test_LDADD = $(LDADD)

kmos_priv_sky_tweak_test_SOURCES = create_data_kmos.c kmos_priv_sky_tweak-test.c
kmos_priv_sky_tweak_test_LDFLAGS = $(CPL_LDFLAGS)
kmos_priv_sky_tweak_test_LDADD = $(LDADD)

kmo_priv_stats_test_SOURCES = create_data_kmos.c kmo_priv_stats-test.c
kmo_priv_stats_test_LDFLAGS = $(CPL_LDFLAGS)
kmo_priv_stats_test_LDADD = $(LDADD)

kmo_priv_std_star_test_SOURCES = create_data_kmos.c kmo_priv_std_star-test.c
kmo_priv_std_star_test_LDFLAGS = $(CPL_LDFLAGS)
kmo_priv_std_star_test_LDADD = $(LDADD)

kmo_priv_wave_cal_test_SOURCES = kmo_priv_wave_cal-test.c
kmo_priv_wave_cal_test_LDFLAGS = $(CPL_LDFLAGS)
kmo_priv_wave_cal_test_LDADD = $(LDADD)

kmo_utils_test_SOURCES = create_data_kmos.c kmo_utils-test.c
kmo_utils_test_LDFLAGS = $(CPL_LDFLAGS)
kmo_utils_test_LDADD = $(LDADD)

TESTS          = $(check_PROGRAMS)

# Expected failures
XFAIL_TESTS =

# Be sure to reexport important environment variables.
TESTS_ENVIRONMENT = MAKE="$(MAKE)" CC="$(CC)" CFLAGS="$(CFLAGS)" \
        CPPFLAGS="$(CPPFLAGS)" LD="$(LD)" LDFLAGS="$(LDFLAGS)" \
        LIBS="$(LIBS)" LN_S="$(LN_S)" NM="$(NM)" RANLIB="$(RANLIB)" \
        OBJEXT="$(OBJEXT)" EXEEXT="$(EXEEXT)"

# We need to remove any files that the above tests created.
clean-local:
	$(RM) *.fits *.paf *.log *.txt $(top_srcdir)/kmos/tests/test_data
