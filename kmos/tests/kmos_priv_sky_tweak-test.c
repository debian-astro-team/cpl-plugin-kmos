
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <math.h>

#include <cpl.h>

#include "kmclipm_priv_splines.h"
#include "kmclipm_functions.h"

#include "kmos_priv_sky_tweak.h"
#include "kmo_priv_lcorr.h"

void kmo_test_verbose_off();

/**
    @brief  
 */
int main(void)
{
    cpl_imagelist  *obj_cube = NULL,
                   *sky_cube = NULL;
    cpl_propertylist *obj_header = NULL;
    int ix;

    cpl_test_init("usd-help@eso.org", CPL_MSG_WARNING);
    kmo_test_verbose_off();

    char * my_path = cpl_sprintf("%s/ref_data/sky_tweak_obj_cube.fits", getenv("srcdir"));
    obj_cube = cpl_imagelist_load(my_path, CPL_TYPE_FLOAT, 0);
    obj_header = cpl_propertylist_load(my_path,0);
    cpl_free(my_path);
    my_path = cpl_sprintf("%s/ref_data/sky_tweak_sky_cube.fits", getenv("srcdir"));
    sky_cube = cpl_imagelist_load(my_path, CPL_TYPE_FLOAT, 0);
    cpl_free(my_path);

    for (ix=0; ix<cpl_imagelist_get_size(obj_cube); ix++) {
        kmclipm_reject_nan(cpl_imagelist_get(obj_cube, ix));
    }
    for (ix=0; ix<cpl_imagelist_get_size(sky_cube); ix++) {
        kmclipm_reject_nan(cpl_imagelist_get(sky_cube, ix));
    }

    cpl_propertylist_delete(obj_header);
    cpl_imagelist_delete(obj_cube);
    cpl_imagelist_delete(sky_cube);
    return cpl_test_end(0);
}
