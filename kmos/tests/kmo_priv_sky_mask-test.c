/*
 * This file is part of the KMOS Library
 * Copyright (C) 2007-2008 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "kmo_priv_sky_mask.h"
#include "kmo_debug.h"

/**
    @defgroup kmo_priv_sky_mask_test   kmo_priv_sky_mask unit tests

    @{
 */

void kmo_test_verbose_off();
void kmo_test_verbose_on();
cpl_imagelist* kmo_test_create_cube(int x, int y, int z, int gauss, int *offset);

/**
    @brief   test for kmo_calc_sky_mask()
*/
void test_kmo_calc_sky_mask(void)
{
    float           tol     = 0.1;

    cpl_imagelist   *cube   = NULL;

    cpl_image       *mask   = NULL;

    double          crpix   = 0.0,
                    crval   = 0.0,
                    cdelt   = 0.0;

    int             offset  = 0;

    /* --- invalid tests --- */
    kmo_test_verbose_off();

    cpl_test_null(mask = kmo_calc_sky_mask(NULL,
                             NULL,
                             0.25,
                             crpix,
                             crval,
                             cdelt,
                             3.0,
                             3.0,
                             3));
    cpl_test_error(CPL_ERROR_NULL_INPUT);

    kmo_test_verbose_on();

    /* --- valid tests --- */

    /* create test data */
    cpl_test_nonnull(
        cube = kmo_test_create_cube(14, 14, 14, TRUE, &offset));

    crpix = 1.0;
    crval = 0.8;
    cdelt = 0.001;

    cpl_image *noise = cpl_image_new(14, 14, CPL_TYPE_FLOAT);
    cpl_image_fill_noise_uniform(noise, 0., 0.02);

    cpl_imagelist_multiply_scalar(cube, 10.);
    cpl_imagelist_add_image(cube, noise);

    cpl_vector *ranges = cpl_vector_new(2);
    cpl_vector_set(ranges, 0, 0.805);
    cpl_vector_set(ranges, 1, 0.83);

    /* run test with noisy gauss-cube */
    cpl_test_nonnull(mask = kmo_calc_sky_mask(cube,
                             ranges,
                             0.25,
                             crpix,
                             crval,
                             cdelt,
                             3.0,
                             3.0,
                             3));
    cpl_test_error(CPL_ERROR_NONE);
    cpl_test_abs(cpl_image_get_mean(mask), 0.408163, tol);
    cpl_imagelist_delete(cube);
    cpl_image_delete(mask);
    cpl_vector_delete(ranges);
    cpl_image_delete(noise);

    /* run test with uniform-noise-cube */
    cube = cpl_imagelist_new();
    int i;
    for (i = 0; i < 50; i++) {
        noise = cpl_image_new(14, 14, CPL_TYPE_FLOAT);
        cpl_image_fill_noise_uniform(noise, 0., 0.02);
        cpl_imagelist_set(cube, noise, i);
    }
    cpl_test_nonnull(mask = kmo_calc_sky_mask(cube,
                             NULL,
                             0.25,
                             crpix,
                             crval,
                             cdelt,
                             3.0,
                             3.0,
                             3));
    cpl_test_error(CPL_ERROR_NONE);
    cpl_test_abs(cpl_image_get_mean(mask), 0.0, tol);
    cpl_imagelist_delete(cube);
    cpl_image_delete(mask);
}

/**
    @brief   Test of helper functions for kmo_sky_mask
*/
int main(void)
{
    cpl_test_init("<usd-help@eso.org>", CPL_MSG_WARNING);

    test_kmo_calc_sky_mask();

    return cpl_test_end(0);
}

/** @} */
