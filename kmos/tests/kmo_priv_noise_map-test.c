/*
 * This file is part of the KMOS Library
 * Copyright (C) 2007-2008 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "kmo_priv_noise_map.h"
#include "kmo_utils.h"

/**
    @defgroup kmo_priv_noise_map_test   kmo_priv_noise_map unit tests

    @{
*/

void kmo_test_verbose_off();
void kmo_test_verbose_on();
void kmo_test_fill_image(cpl_image *img, float seed, float offset);

/**
    @brief   test for kmo_calc_noise_map
*/
void test_kmo_calc_noise_map(void)
{
    int       size2D        = 100,
              rej           = 0;
    float     tol           = 0.01;

    cpl_image  *img_in      = cpl_image_new(size2D, size2D, CPL_TYPE_FLOAT),
               *img_out     = NULL;

    kmo_test_fill_image(img_in, 0.0, 1.0);

    cpl_test_nonnull(img_out = kmo_calc_noise_map(img_in, 3.01, 0.2));
    cpl_test_eq(cpl_image_get_size_x(img_out), cpl_image_get_size_x(img_in));
    cpl_test_eq(cpl_image_get_size_y(img_out), cpl_image_get_size_y(img_in));
    cpl_test_abs(cpl_image_get(img_out, 3, 4, &rej), 10.0168, tol);
    cpl_image_delete(img_out);

    kmo_test_verbose_off();

    img_out = kmo_calc_noise_map(NULL, 3.01, 0.2);
    cpl_test_error(CPL_ERROR_NULL_INPUT);

    img_out = kmo_calc_noise_map(img_in, -0.1, 0.2);
    cpl_test_error(CPL_ERROR_ILLEGAL_INPUT);

    img_out = kmo_calc_noise_map(img_in, 0.1, -0.2);
    cpl_test_error(CPL_ERROR_ILLEGAL_INPUT);

    kmo_test_verbose_on();

    cpl_image_delete(img_in);

    return;
}


/**
    @brief   Test of helper functions for kmo_noise_map
*/
int main(void)
{
    cpl_test_init("<usd-help@eso.org>", CPL_MSG_WARNING);

    test_kmo_calc_noise_map();

    return cpl_test_end(0);
}

/** @} */
