/*
 * This file is part of the KMOS Library
 * Copyright (C) 2007-2008 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "kmo_debug.h"
#include "kmo_utils.h"
#include "kmo_cpl_extensions.h"

/**
    @defgroup kmo_priv_debug_test   kmo_priv_debug unit tests

    @{
*/

void kmo_test_verbose_off();
void kmo_test_verbose_on();

/**
    @brief   test kmo_debug_header()
*/
void test_kmo_debug_header(void)
{
    cpl_propertylist *pl = cpl_propertylist_new();
    kmo_test_verbose_off();
    cpl_test_eq(CPL_ERROR_NONE, kmo_debug_header(NULL));
    cpl_test_eq(CPL_ERROR_NONE, kmo_debug_header(pl));
    kmo_test_verbose_on();
    cpl_propertylist_delete(pl);
}

/**
    @brief   test kmo_debug_frameset()
*/
void test_kmo_debug_frameset(void)
{
    cpl_frameset *fs = cpl_frameset_new();
    kmo_test_verbose_off();
    cpl_test_eq(CPL_ERROR_NONE, kmo_debug_frameset(NULL));
    cpl_test_eq(CPL_ERROR_NONE, kmo_debug_frameset(fs));
    kmo_test_verbose_on();
    cpl_frameset_delete(fs);
}

/**
    @brief   test kmo_debug_frame()
*/
void test_kmo_debug_frame(void)
{
    cpl_frame *f = cpl_frame_new();
    kmo_test_verbose_off();
    cpl_test_eq(CPL_ERROR_NONE, kmo_debug_frame(NULL));
    cpl_test_eq(CPL_ERROR_NONE, kmo_debug_frame(f));
    kmo_test_verbose_on();
    cpl_frame_delete(f);
}

/**
    @brief   test kmo_debug_cube()
*/
void test_kmo_debug_cube(void)
{
    cpl_imagelist *cube = cpl_imagelist_new();
    kmo_test_verbose_off();
    cpl_test_eq(CPL_ERROR_NONE, kmo_debug_cube(NULL));
    cpl_test_eq(CPL_ERROR_NONE, kmo_debug_cube(cube));
    kmo_test_verbose_on();
    cpl_imagelist_delete(cube);
}

/**
    @brief   test kmo_debug_image()
*/
void test_kmo_debug_image(void)
{
    cpl_image *img = cpl_image_new(5, 5, CPL_TYPE_FLOAT);
    kmo_test_verbose_off();
    cpl_test_eq(CPL_ERROR_NONE, kmo_debug_image(NULL));
    cpl_test_eq(CPL_ERROR_NONE, kmo_debug_image(img));
    kmo_test_verbose_on();
    cpl_image_delete(img);
}

/**
    @brief   test kmo_debug_vector()
*/
void test_kmo_debug_vector(void)
{
    cpl_vector *vec = cpl_vector_new(5);
    kmo_test_verbose_off();
    cpl_test_eq(CPL_ERROR_NONE, kmo_debug_vector(NULL));
    cpl_vector_fill(vec, 0.0);
    cpl_test_eq(CPL_ERROR_NONE, kmo_debug_vector(vec));
    kmo_test_verbose_on();
    cpl_vector_delete(vec);
}

/**
    @brief   test kmo_debug_array()
*/
void test_kmo_debug_array(void)
{
    cpl_array *a = cpl_array_new(5, CPL_TYPE_INT);
    kmo_test_verbose_off();
    cpl_test_eq(CPL_ERROR_NONE, kmo_debug_array(NULL));
    kmo_array_fill_int(a, 9);
    cpl_test_eq(CPL_ERROR_NONE, kmo_debug_array(a));
    kmo_test_verbose_on();
    cpl_array_delete(a);
}

/**
    @brief   test kmo_debug_desc()
*/
void test_kmo_debug_desc(void)
{
    kmo_test_verbose_off();
    char *my_path = cpl_sprintf("%s/ref_data/KMOS_dummy.fits", getenv("srcdir"));
    main_fits_desc desc = kmo_identify_fits_header(my_path);
    cpl_test_eq(CPL_ERROR_NONE, kmo_debug_desc(desc));
    kmo_test_verbose_on();
    kmo_free_fits_desc(&desc);
    cpl_free(my_path);
}

/**
    @brief   test kmo_debug_table()
*/
void test_kmo_debug_table(void)
{
    cpl_table *t = cpl_table_new(3);
    kmo_test_verbose_off();
    cpl_test_eq(CPL_ERROR_NONE, kmo_debug_table(NULL));
    /* cpl_test_eq(CPL_ERROR_NONE, kmo_debug_table(t)); */
    kmo_test_verbose_on();
    cpl_table_delete(t);
}

/**
    @brief   test kmo_plot_vector()
*/
void test_kmo_plot_vector(void)
{
    kmo_test_verbose_off();
    kmo_plot_vector(NULL, NULL, NULL);
    kmo_test_verbose_on();
}

/**
    @brief   test kmo_plot_vectors_xy()
*/
void test_kmo_plot_vectors_xy(void)
{
    kmo_test_verbose_off();
    kmo_plot_vectors_xy(NULL, NULL, NULL, NULL);
    kmo_test_verbose_on();
}

/**
    @brief   test kmo_plot_vectors2()
*/
void test_kmo_plot_vectors2(void)
{
    kmo_test_verbose_off();
    kmo_plot_vectors2(NULL, NULL, NULL, NULL, NULL);
    kmo_test_verbose_on();
}

/**
    @brief   test kmo_plot_image()
*/
void test_kmo_plot_image(void)
{
    kmo_test_verbose_off();
    kmo_plot_image(NULL, NULL, NULL);
    kmo_test_verbose_on();
}

/**
    @brief   Test of debug helper functions
*/
int main(void)
{
    cpl_test_init("<usd-help@eso.org>", CPL_MSG_WARNING);

    test_kmo_debug_header();
    test_kmo_debug_frameset();
    test_kmo_debug_frame();
    test_kmo_debug_cube();
    test_kmo_debug_image();
    test_kmo_debug_vector();
    test_kmo_debug_array();
    test_kmo_debug_desc();
    test_kmo_debug_table();
    test_kmo_plot_vector();
    test_kmo_plot_vectors_xy();
    test_kmo_plot_vectors2();
    test_kmo_plot_image();

    return cpl_test_end(0);
}

/** @} */
