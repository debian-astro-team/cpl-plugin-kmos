/*
 * This file is part of the KMOS Library
 * Copyright (C) 2007-2008 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "kmclipm_vector.h"

#include "kmo_priv_fits_check.h"
#include "kmo_cpl_extensions.h"
#include "kmo_debug.h"

/**
    @defgroup kmo_priv_fits_check_test   kmo_priv_fits_check unit tests

    @{
 */

void kmo_test_verbose_off();
void kmo_test_verbose_on();

/**
    @brief   test for kmo_fits_check_print_header()
*/
void test_kmo_fits_check_print_header(void)
{
    cpl_propertylist *pl = NULL;

    /* --- invalid tests --- */
    kmo_test_verbose_off();
    kmo_fits_check_print_header(NULL);
    cpl_test_error(CPL_ERROR_NULL_INPUT);
    kmo_test_verbose_on();

    /* --- valid tests --- */
    pl = cpl_propertylist_new();
    /* kmo_fits_check_print_header(pl); */
    cpl_test_error(CPL_ERROR_NONE);
    cpl_propertylist_delete(pl);
}

/**
    @brief   test for kmo_fits_check_print_vector()
*/
void test_kmo_fits_check_print_vector(void)
{
    kmclipm_vector *vec = NULL;

    /* --- invalid tests --- */
    kmo_test_verbose_off();
    kmo_fits_check_print_vector(NULL);
    cpl_test_error(CPL_ERROR_NULL_INPUT);

    /* --- valid tests --- */
    vec = kmclipm_vector_new(1);
    kmclipm_vector_set(vec, 0, 5.0);
    /* kmo_fits_check_print_vector(vec); */
    cpl_test_error(CPL_ERROR_NONE);
    kmclipm_vector_delete(vec);
    kmo_test_verbose_on();
}

/**
    @brief   test for kmo_fits_check_print_image()
*/
void test_kmo_fits_check_print_image(void)
{
    /* --- invalid tests --- */
    kmo_test_verbose_off();
    kmo_fits_check_print_image(NULL);
    cpl_test_error(CPL_ERROR_NULL_INPUT);

    /* --- valid tests --- */
    cpl_image *image = cpl_image_new(1, 1, CPL_TYPE_FLOAT);
    /* kmo_fits_check_print_image(image); */
    cpl_test_error(CPL_ERROR_NONE);
    cpl_image_delete(image);
    kmo_test_verbose_on();
}

/**
    @brief   test for kmo_fits_check_print_imagelist()
*/
void test_kmo_fits_check_print_imagelist(void)
{
    /* --- invalid tests --- */
    kmo_test_verbose_off();
    kmo_fits_check_print_imagelist(NULL);
    cpl_test_error(CPL_ERROR_NULL_INPUT);

    /* --- valid tests --- */
    cpl_imagelist *imagelist = cpl_imagelist_new();
    /* kmo_fits_check_print_imagelist(imagelist); */
    cpl_test_error(CPL_ERROR_NONE);
    cpl_imagelist_delete(imagelist);
    kmo_test_verbose_on();
}

/**
    @brief   test for kmo_fits_check_print_table()
*/
void test_kmo_fits_check_print_table(void)
{
    /* --- invalid tests --- */
    kmo_test_verbose_off();
    kmo_fits_check_print_table(NULL);
    cpl_test_error(CPL_ERROR_NULL_INPUT);

    /* --- valid tests --- */
    cpl_table *table = cpl_table_new(1);
    /* kmo_fits_check_print_table(table); */
    cpl_test_error(CPL_ERROR_NONE);
    cpl_table_delete(table);
    kmo_test_verbose_on();
}

/**
    @brief   test for kmo_fits_check_print_info()
*/
void test_kmo_fits_check_print_info(void)
{
    main_fits_desc desc;

    /* --- invalid tests --- */
    kmo_test_verbose_off();
    kmo_fits_check_print_info(2, NULL, NULL);
    cpl_test_error(CPL_ERROR_NULL_INPUT);
    kmo_init_fits_desc(&desc);
    kmo_fits_check_print_info(2, &desc, NULL);
    cpl_test_error(CPL_ERROR_NULL_INPUT);
    cpl_frame *f = cpl_frame_new();
    kmo_fits_check_print_info(2, &desc, f);
    cpl_test_error(CPL_ERROR_ILLEGAL_INPUT);

    /* --- valid tests --- */

//    cpl_test_eq(CPL_ERROR_NONE, kmo_fits_check_print_info(FALSE, &desc, f));

    cpl_frame_delete(f);
    kmo_free_fits_desc(&desc);

    cpl_test_error(CPL_ERROR_NONE);
    kmo_test_verbose_on();
}

/**
 * @brief   Test of helper functions for kmo_checkfits
 */
int main(void)
{
    cpl_test_init("<usd-help@eso.org>", CPL_MSG_WARNING);

    test_kmo_fits_check_print_header();
    test_kmo_fits_check_print_vector();
    test_kmo_fits_check_print_image();
    test_kmo_fits_check_print_imagelist();
    test_kmo_fits_check_print_table();
    test_kmo_fits_check_print_info();

    return cpl_test_end(0);
}

/** @} */
