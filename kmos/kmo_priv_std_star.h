/* $Id: kmo_priv_std_star.h,v 1.12 2013-07-29 18:10:08 aagudo Exp $
 *
 * This file is part of the KMOS Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: aagudo $
 * $Date: 2013-07-29 18:10:08 $
 * $Revision: 1.12 $
 * $Name: not supported by cvs2svn $
 */

#ifndef KMOS_PRIV_STD_STAR_H
#define KMOS_PRIV_STD_STAR_H

/*-----------------------------------------------------------------------------
 *                              Includes
 *----------------------------------------------------------------------------*/

#include <cpl.h>

#include "kmclipm_constants.h"

/*-----------------------------------------------------------------------------
 *              Types
 *-----------------------------------------------------------------------------*/
extern int print_warning_once_stdstar;
extern int cnter;

typedef struct {
   cpl_frame *objectFrame;
   cpl_frame *skyframes[KMOS_NR_IFUS];
} objSkyFrameTableStruct;

typedef struct {
    cpl_frame *skyFrame1;
    cpl_frame *skyFrame2;
} skySkyPair;

typedef struct {
    int nrSkyPairs;
    skySkyPair *skyPairs;
} skySkyStruct;

#define NO_CORRESPONDING_SKYFRAME ((cpl_frame* ) -1)

/*------------------------------------------------------------------------------
 *                        Prototypes
 *----------------------------------------------------------------------------*/

double kmos_get_temperature(
        cpl_frameset        *   fset,
        const char          *   spec_type,
        char                *   star_type) ;

cpl_vector*             kmo_interpolate_vector_wcs(
                                    const cpl_frame *frame,
                                    cpl_vector *x_out);

cpl_error_code          kmo_divide_blackbody(
                                    cpl_vector *ydata,
                                    cpl_vector *xdata,
                                    double temperature);

cpl_error_code          kmo_remove_line(cpl_vector *spectrum,
                                    const cpl_vector *lambda,
                                    const cpl_vector *atmos_model,
                                    double line_center,
                                    double line_width);

cpl_error_code          kmo_calc_counts(
                                    const cpl_vector *data,
                                    const char *filter_id,
                                    double crpix,
                                    double crval,
                                    double cdelt,
                                    double *counts_band1,
                                    double *counts_band2);

double                  kmo_calc_zeropoint(
                                    double magnitude1,
                                    double magnitude2,
                                    double counts_band1,
                                    double counts_band2,
                                    double cdelt,
                                    const char *filter_id);

double                  kmo_calc_throughput(
                                    double magnitude1,
                                    double magnitude2,
                                    double counts_band1,
                                    double counts_band2,
                                    double gain,
                                    const char *filter_id);

cpl_error_code          kmo_calc_mean_throughput(
                                    double *throughput,
                                    int size_throughput,
                                    double *mean,
                                    double *stdev);

cpl_error_code          kmo_calculate_std_trace(
                                    cpl_imagelist *cube,
                                    const char * fmethod,
                                    double *std_trace);

cpl_error_code          kmo_calc_band_mean(
                                    const cpl_propertylist *pl,
                                    const char *filter_id,
                                    const cpl_vector *data,
                                    const cpl_vector *noise,
                                    double *mean_data,
                                    double *mean_noise);

skySkyStruct*           kmo_create_skySkyStruct(
                                    cpl_frameset *frameset);

void                    kmo_delete_skySkyStruct(
                                    skySkyStruct *sky_sky_struct);
#endif
