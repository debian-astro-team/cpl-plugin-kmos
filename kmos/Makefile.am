## Process this file with automake to produce Makefile.in

##   This file is part of the KMOS Pipeline
##   Copyright (C) 2002,2003 European Southern Observatory

##   This library is free software; you can redistribute it and/or modify
##   it under the terms of the GNU General Public License as published by
##   the Free Software Foundation; either version 2 of the License, or
##   (at your option) any later version.

##   This program is distributed in the hope that it will be useful,
##   but WITHOUT ANY WARRANTY; without even the implied warranty of
##   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##   GNU General Public License for more details.

##   You should have received a copy of the GNU General Public License
##   along with this program; if not, write to the Free Software
##   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

AUTOMAKE_OPTIONS = 1.8 foreign

DISTCLEANFILES = *~

SUBDIRS = tests

if MAINTAINER_MODE

MAINTAINERCLEANFILES = Makefile.in

endif

INCLUDES = $(all_includes) $(MOLECFIT_INCLUDES) -I../kmclipm/include/

noinst_HEADERS =         kmo_constants.h \
                         kmo_cpl_extensions.h \
                         kmo_debug.h \
                         kmo_dfs.h \
                         kmo_error.h \
                         kmo_functions.h \
                         kmos_pfits.h \
                         kmo_utils.h \
                         kmo_priv_arithmetic.h \
                         kmo_priv_combine.h \
                         kmo_priv_copy.h \
                         kmo_priv_dark.h \
                         kmo_priv_extract_spec.h \
                         kmo_priv_fit_profile.h \
                         kmo_priv_fits_check.h \
                         kmo_priv_fits_stack.h \
                         kmo_priv_flat.h \
                         kmo_priv_functions.h \
                         kmo_priv_make_image.h \
                         kmo_priv_noise_map.h \
                         kmo_priv_reconstruct.h \
                         kmo_priv_rotate.h \
                         kmo_priv_shift.h \
                         kmo_priv_sky_mask.h \
                         kmo_priv_stats.h \
                         kmo_priv_wave_cal.h \
                         kmo_priv_std_star.h \
                         kmo_priv_lcorr.h \
                         kmos_priv_sky_tweak.h \
                         ../kmclipm/include/kmclipm_compatibility_cpl.h \
                         ../kmclipm/include/kmclipm_constants.h \
                         ../kmclipm/include/kmclipm_functions.h \
                         ../kmclipm/include/kmclipm_math.h \
                         ../kmclipm/include/kmclipm_priv_constants.h \
                         ../kmclipm/include/kmclipm_priv_error.h \
                         ../kmclipm/include/kmclipm_priv_functions.h \
                         ../kmclipm/include/kmclipm_priv_reconstruct.h \
                         ../kmclipm/include/kmclipm_priv_splines.h \
                         ../kmclipm/include/kmclipm_vector.h \
                         kmos_oscan.h \
                         kmos_molecfit.h
                        
pkginclude_HEADERS =

privatelib_LTLIBRARIES = libkmos.la

libkmos_la_SOURCES =     kmo_cpl_extensions.c \
                         kmo_debug.c \
                         kmo_dfs.c \
                         kmo_functions.c \
                         kmos_pfits.c \
                         kmo_utils.c \
                         kmo_priv_arithmetic.c \
                         kmo_priv_copy.c \
                         kmo_priv_combine.c \
                         kmo_priv_dark.c \
                         kmo_priv_extract_spec.c \
                         kmo_priv_fit_profile.c \
                         kmo_priv_fits_check.c \
                         kmo_priv_fits_stack.c \
                         kmo_priv_flat.c \
                         kmo_priv_functions.c \
                         kmo_priv_make_image.c \
                         kmo_priv_noise_map.c \
                         kmo_priv_reconstruct.c \
                         kmo_priv_rotate.c \
                         kmo_priv_shift.c \
                         kmo_priv_sky_mask.c \
                         kmo_priv_stats.c \
                         kmo_priv_wave_cal.c \
                         kmo_priv_std_star.c \
                         kmo_priv_lcorr.c \
                         kmos_priv_sky_tweak.c \
                         ../kmclipm/src/kmclipm_functions.c \
                         ../kmclipm/src/kmclipm_math.c \
                         ../kmclipm/src/kmclipm_priv_constants.c \
                         ../kmclipm/src/kmclipm_priv_error.c \
                         ../kmclipm/src/kmclipm_priv_functions.c \
                         ../kmclipm/src/kmclipm_priv_reconstruct.c \
                         ../kmclipm/src/kmclipm_priv_splines.c \
                         ../kmclipm/src/kmclipm_vector.c \
                         kmos_oscan.c \
                         kmos_molecfit.c

  
libkmos_la_LDFLAGS = $(EXTRA_LDFLAGS) $(CPL_LDFLAGS)  $(MOLECFIT_LDFLAGS) -version-info $(LT_CURRENT):$(LT_REVISION):$(LT_AGE)
libkmos_la_LIBADD = $(LIBIRPLIB) $(LIBCPLDFS) $(LIBCPLUI) $(LIBCPLDRS) $(LIBCPLCORE) $(LIBMOLECFIT)
libkmos_la_DEPENDENCIES = $(LIBIRPLIB)
