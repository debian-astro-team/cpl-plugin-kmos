/*
 * This file is part of the KMOS Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef KMOS_PRIV_WAVE_CAL_H
#define KMOS_PRIV_WAVE_CAL_H

/*------------------------------------------------------------------------------
 *                        Includes
 *----------------------------------------------------------------------------*/

#include <cpl.h>
#include "kmclipm_vector.h"

typedef enum {
    ARGON, 
    NEON, 
    ARGON_NEON
} lampConfiguration;

/*------------------------------------------------------------------------------
 *                        Prototypes
 *----------------------------------------------------------------------------*/

cpl_bivector * kmos_get_lines(
        const cpl_table         *   arclines,
        lampConfiguration           lamp_config) ;

cpl_error_code kmos_calc_wave_calib(
        const cpl_image     *   data,
        const cpl_image     *   bad_pix,
        const char          *   filter_id,
        lampConfiguration       lamp_config,
        const int               detector_nr,
        cpl_array           *   ifu_inactive,
        cpl_table           **  edge_table,
        cpl_bivector        *   lines,
        cpl_table           *   reflines,
        cpl_image           **  lcal,
        double              *   qc_ar_eff,
        double              *   qc_ne_eff,
        const int               fit_order,
        const int               line_estimate_method) ;

cpl_image * kmo_reconstructed_arc_image(
        cpl_frameset    *   frameset,
        cpl_image       *   arcDetImg_data,
        cpl_image       *   darkDetImg_data,
        cpl_image       *   xcalDetImg,
        cpl_image       *   ycalDetImg,
        cpl_image       *   lcalDetImg,
        cpl_array       *   ifu_inactive,
        int                 flip,
        int                 device_nr,
        const char      *   suffix,
        const char      *   filter_id,
        lampConfiguration   lamp_config,
        cpl_propertylist ** qc_header) ;

double kmo_calc_fitted_slitlet_edge(
        cpl_table   *   edge_table,
        int             row,
        int             y) ;

int kmo_image_get_saturated(
        const cpl_image     *   data,
        float                   threshold) ;

char ** kmo_get_filter_setup(
        const cpl_propertylist  *   pl,
        int                         nr_devices,
        int                         check_return) ;

#endif
