/* $Id: kmo_priv_noise_map.c,v 1.3 2013-06-17 07:52:26 aagudo Exp $
 *
 * This file is part of the KMOS Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: aagudo $
 * $Date: 2013-06-17 07:52:26 $
 * $Revision: 1.3 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

/*-----------------------------------------------------------------------------
 *                              Includes
 *----------------------------------------------------------------------------*/

#include <string.h>
#include <math.h>

#include <cpl.h>

#include "kmo_debug.h"
#include "kmo_utils.h"
#include "kmo_dfs.h"
#include "kmo_cpl_extensions.h"
#include "kmo_priv_noise_map.h"
#include "kmo_error.h"

/*----------------------------------------------------------------------------*/
/**
    @defgroup kmos_priv_noise_map     Helper functions for recipe kmo_noise_map.

    @{
 */
/*----------------------------------------------------------------------------*/

int print_warning_once_noise = TRUE;

/**
    @brief
        Calculates the estimated noise-map of an image.

    @param data         The input image.
    @param gain         The gain-value.
    @param readnoise    The readnoise-value.

    @return             The noise map.
*/
cpl_image* kmo_calc_noise_map(cpl_image *data,
                              double gain,
                              double readnoise)
{
    cpl_image       *noise_img      = NULL;

    KMO_TRY
    {
        KMO_TRY_ASSURE(data != NULL,
                       CPL_ERROR_NULL_INPUT,
                       "Not all input data is provided!");

        KMO_TRY_ASSURE(gain >= 0.0,
                       CPL_ERROR_ILLEGAL_INPUT,
                       "No negative gain!");

        KMO_TRY_ASSURE(readnoise >= 0.0,
                       CPL_ERROR_ILLEGAL_INPUT,
                       "No negative readnoise!");

        /* calculate initial noise estimate
            sigma = sqrt(counts * gain + readnoise^2) / gain */
        KMO_TRY_EXIT_IF_NULL(
            noise_img = cpl_image_multiply_scalar_create(data, gain));

        KMO_TRY_EXIT_IF_ERROR(
            cpl_image_add_scalar(noise_img, readnoise * readnoise));

        if (cpl_image_get_min (noise_img) < 0.0) {
            if (print_warning_once_noise == TRUE) {
                cpl_msg_warning(cpl_func, "Negative pixels are set to zero in"
                                          " order to calculate noise map!");
                  print_warning_once_noise = FALSE;
            }
            float *pnoise_img = cpl_image_get_data_float(noise_img);
            int i = 0;
            for (i = 0;
                 i < cpl_image_get_size_x(noise_img)*cpl_image_get_size_y(noise_img);
                 i++)
            {
                if (pnoise_img[i] < 0.0) {
                    pnoise_img[i] = 0.0;
                }
            }
        }

        KMO_TRY_EXIT_IF_ERROR(
            cpl_image_power(noise_img, 0.5));

        if (gain != 0.0) {
            KMO_TRY_EXIT_IF_ERROR(
                cpl_image_divide_scalar(noise_img, gain));
        } else {
            KMO_TRY_EXIT_IF_ERROR(
                kmo_image_divide_scalar(noise_img, gain));
        }
    }
    KMO_CATCH
    {
        KMO_CATCH_MSG();
        cpl_image_delete(noise_img); noise_img = NULL;
    }

    return noise_img;
}

/**
    @brief
        Calculates the readnoise based on ndsamples.

    @param ndsamples    The number of samples

    @return             The estimated readnoise.
*/
double kmo_calc_readnoise_ndr(int ndsamples)
{
    double  readnoise       = 0,
            max_readnoise   = 10.1;

    KMO_TRY
    {
        KMO_TRY_ASSURE(ndsamples > 0,
                       CPL_ERROR_ILLEGAL_INPUT,
                       "ndsamples must be greater than 0!");

        readnoise = sqrt(pow(5.9, 2) + pow(15.8, 2)/pow(ndsamples, 0.9));

        if (readnoise > max_readnoise) {
            readnoise = max_readnoise;
        }
    }
    KMO_CATCH
    {
        KMO_CATCH_MSG();
        readnoise = 0;
    }

    return readnoise;
}

/** @{ */
