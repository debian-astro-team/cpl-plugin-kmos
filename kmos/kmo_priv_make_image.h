/*
 * This file is part of the KMOS Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef KMOS_PRIV_MAKE_IMAGE_H
#define KMOS_PRIV_MAKE_IMAGE_H

/*-----------------------------------------------------------------------------
 *                              Includes
 *----------------------------------------------------------------------------*/

#include <cpl.h>

/*------------------------------------------------------------------------------
 *                              Prototypes
 *----------------------------------------------------------------------------*/

cpl_vector * kmo_identify_slices_with_oh(
        cpl_vector  *   spec_data_in,
        cpl_vector  *   spec_lambda_in,
        cpl_vector  *   ranges,
        double          threshold,
        double          ifu_crpix,
        double          ifu_crval,
        double          ifu_cdelt,
        int             size_ifu) ;

#endif
