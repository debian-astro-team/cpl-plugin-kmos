/* $Id: kmo_cpl_extensions.h,v 1.4 2013-03-27 10:58:56 aagudo Exp $
 *
 * This file is part of the KMOS Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: aagudo $
 * $Date: 2013-03-27 10:58:56 $
 * $Revision: 1.4 $
 * $Name: not supported by cvs2svn $
 */

#ifndef KMOS_CPL_EXTENSIONS_H
#define KMOS_CPL_EXTENSIONS_H

/*------------------------------------------------------------------------------
 *                        Includes
 *----------------------------------------------------------------------------*/

#include <cpl.h>

// ---------------------- missing in CPL ---------------------------------------
// --- vector ---
cpl_error_code      kmo_vector_get_maxpos_old(const cpl_vector *vec,
                                      int *x);

cpl_error_code      kmo_vector_get_minpos_old(const cpl_vector *vec,
                                      int *x);

cpl_vector*         kmo_vector_histogram_old(const cpl_vector *d,
                                      int nbins);

double              kmo_vector_get_max_old(const cpl_vector *v, int *pos);

cpl_error_code      kmo_vector_flip_old(cpl_vector* v);

double              kmo_vector_get_mean_old(const cpl_vector *v);

// --- image ---
cpl_vector*         kmo_image_histogram(const cpl_image *d,
                                      int nbins);

cpl_vector*         kmo_image_sort(const cpl_image *d);

cpl_error_code      kmo_image_fill(cpl_image *img,
                                      double value);

double              kmo_image_get_flux(const cpl_image *img);

cpl_error_code      kmo_image_reject_from_mask(cpl_image *img,
                                               const cpl_image *mask);

int                 kmo_image_get_rejected(const cpl_image *img);
// --- imagelist ---
cpl_error_code      kmo_imagelist_power(cpl_imagelist *imglist,
                                      double exponent);

double              kmo_imagelist_get_mean(const cpl_imagelist *cube);

double              kmo_imagelist_get_flux(const cpl_imagelist *cube);

double              kmo_imagelist_get_mode(const cpl_imagelist *cube,
                                           double *noise);

cpl_error_code      kmo_imagelist_turn(cpl_imagelist *cube, int rot);

cpl_error_code      kmo_imagelist_shift(cpl_imagelist *cube, int dx, int dy);

// --- array ---
cpl_error_code      kmo_array_fill_int(cpl_array *arr,
                                      int value);

// ---------------------- CPL-overrides ----------------------------------------
// --- vector ---
cpl_error_code      kmo_vector_divide(cpl_vector *vec1,
                                      cpl_vector *vec2);

// --- image ---
cpl_error_code      kmo_image_divide_scalar(cpl_image *image,
                                      float divisor);

cpl_error_code      kmo_image_divide(cpl_image *img1,
                                      const cpl_image *img2);

cpl_error_code      kmo_image_power(cpl_image *img,
                                      double exponent);

// --- imagelist ---
cpl_error_code      kmo_imagelist_divide_scalar(cpl_imagelist *imglist,
                                      double divisor);

cpl_error_code      kmo_imagelist_divide(cpl_imagelist *imglist,
                                      cpl_imagelist *divisor);


#endif
