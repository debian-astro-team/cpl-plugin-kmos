/* $Id: kmo_priv_copy.h,v 1.1.1.1 2012-01-18 09:31:59 yjung Exp $
 *
 * This file is part of the KMOS Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: yjung $
 * $Date: 2012-01-18 09:31:59 $
 * $Revision: 1.1.1.1 $
 * $Name: not supported by cvs2svn $
 */

#ifndef KMOS_PRIV_COPY_H
#define KMOS_PRIV_COPY_H

/*-----------------------------------------------------------------------------
 *                              Includes
 *----------------------------------------------------------------------------*/

#include <cpl.h>

/*------------------------------------------------------------------------------
 *                        Prototypes
 *----------------------------------------------------------------------------*/

/* ----- extract scalar ----- */
double        kmo_copy_scalar_F1I(kmclipm_vector *data,
                            int x);

float         kmo_copy_scalar_F2I(cpl_image *data,
                            int x,
                            int y);

float         kmo_copy_scalar_F3I(cpl_imagelist *data,
                            int x,
                            int y,
                            int z);

/* ----- extract vector ----- */
kmclipm_vector*   kmo_copy_vector_F1I(kmclipm_vector *data,
                            int x1, int x2);

kmclipm_vector*   kmo_copy_vector_F2I_x(cpl_image *data,
                            int x1, int x2,
                            int y);

kmclipm_vector*   kmo_copy_vector_F2I_y(cpl_image *data,
                            int x,
                            int y1, int y2);

kmclipm_vector*   kmo_copy_vector_F3I_x(cpl_imagelist *data,
                            int x1, int x2,
                            int y,
                            int z);

kmclipm_vector*   kmo_copy_vector_F3I_y(cpl_imagelist *data,
                            int x,
                            int y1, int y2,
                            int z);

kmclipm_vector*   kmo_copy_vector_F3I_z(cpl_imagelist *data,
                            int x,
                            int y,
                            int z1, int z2);

/* ----- extract image ----- */

cpl_image*   kmo_copy_image_F2I(cpl_image *data,
                            int x1, int x2,
                            int y1, int y2);

cpl_image*   kmo_copy_image_F3I_x(cpl_imagelist *data,
                            int x,
                            int y1, int y2,
                            int z1, int z2);

cpl_image*   kmo_copy_image_F3I_y(cpl_imagelist *data,
                            int x1, int x2,
                            int y,
                            int z1, int z2);

cpl_image*   kmo_copy_image_F3I_z(cpl_imagelist *data,
                            int x1, int x2,
                            int y1, int y2,
                            int z);

/* ----- extract cube ----- */
cpl_imagelist*   kmo_copy_cube_F3I(cpl_imagelist *data,
                            int x1, int x2,
                            int y1, int y2,
                            int z1, int z2);

#endif
