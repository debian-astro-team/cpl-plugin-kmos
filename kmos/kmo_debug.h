/* $Id: kmo_debug.h,v 1.5 2013-07-04 08:07:34 aagudo Exp $
 *
 * This file is part of the KMOS Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: aagudo $
 * $Date: 2013-07-04 08:07:34 $
 * $Revision: 1.5 $
 * $Name: not supported by cvs2svn $
 */

#ifndef KMOS_DEBUG_H
#define KMOS_DEBUG_H

/*------------------------------------------------------------------------------
 *                        Includes
 *----------------------------------------------------------------------------*/

#include <cpl.h>

#include "kmo_utils.h"

//cpl_msg_set_level(CPL_MSG_DEBUG);kmo_debug_();cpl_msg_set_level(CPL_MSG_INFO);

//cpl_vector_save(vec, "vec.fits", CPL_BPP_IEEE_DOUBLE, NULL, CPL_IO_CREATE);
//cpl_image_save(img, "img.fits", CPL_BPP_IEEE_FLOAT, NULL, CPL_IO_CREATE);
//cpl_imagelist_save(cube, "cube.fits", CPL_BPP_IEEE_FLOAT, NULL, CPL_IO_CREATE);
//cpl_propertylist_save(pl, "ttt.fits", CPL_IO_CREATE);

//vec = cpl_vector_load("vec.fits", 0);
//img = cpl_image_load("img.fits", CPL_TYPE_FLOAT, 0, 0);
//cube = cpl_imagelist_load("cube.fits", CPL_TYPE_FLOAT, 0);
//pl = cpl_propertylist_load(filename, xtnum);

// pre: "set term x11;"
//      "set title 'aaa';"
// opt: "w l;" (with lines)
//      "w l t 'data1'" (t: sets label)
//      "w p lc rgbcolor \"blue\" t 'data2'"
//      "w p lc rgbcolor \"chartreuse\" t 'data3'",
//      "w p lc rgbcolor \"red\" t 'data4'"
// post:
//cpl_msg_set_level(CPL_MSG_DEBUG);
//kmo_plot_vector("", "w l;", "", x);
//kmo_plot_vectors_xy("", "w l;", "", x, y);
//cpl_msg_set_level(CPL_MSG_ERROR);

// KMO_TRY_SET_ERROR(5);
// KMO_TRY_CHECK_ERROR_STATE();

/*-----------------------------------------------------------------------------
                                Functions prototypes
 -----------------------------------------------------------------------------*/
cpl_error_code     kmo_debug_header(const cpl_propertylist *header);

cpl_error_code     kmo_debug_frameset(const cpl_frameset *frameset);

cpl_error_code     kmo_debug_frame(const cpl_frame *frame);

cpl_error_code     kmo_debug_cube(const cpl_imagelist *imglist);

cpl_error_code     kmo_debug_image(const cpl_image *img);

cpl_error_code     kmo_debug_vector(const cpl_vector *vec);

cpl_error_code     kmo_debug_array(const cpl_array *arr);

cpl_error_code     kmo_debug_desc(const main_fits_desc desc);

cpl_error_code     kmo_debug_table(const cpl_table *table);

cpl_error_code     kmo_plot_vector(const char *pre, const char *opt,
                                   const cpl_vector *vector);

cpl_error_code     kmo_plot_vectors_xy(const char *pre, const char *opt,
                                       cpl_vector *x,
                                       cpl_vector *y);

cpl_error_code     kmo_plot_vectors2(const char *pre, const char **opt,
                                     cpl_vector *x,
                                     cpl_vector *y1,
                                     cpl_vector *y2);

cpl_error_code     kmo_plot_image(const char *pre, const char *opt,
                                  const cpl_image *image);

void                kmo_debug_unused_ifus(cpl_array *unused);

#endif
