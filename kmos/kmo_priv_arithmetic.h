/* $Id: kmo_priv_arithmetic.h,v 1.3 2012-05-16 10:53:32 aagudo Exp $
 *
 * This file is part of the KMOS Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: aagudo $
 * $Date: 2012-05-16 10:53:32 $
 * $Revision: 1.3 $
 * $Name: not supported by cvs2svn $
 */

#ifndef KMOS_PRIV_ARITHMETIC_H
#define KMOS_PRIV_ARITHMETIC_H

/*-----------------------------------------------------------------------------
 *                              Includes
 *----------------------------------------------------------------------------*/

#include <cpl.h>

/*------------------------------------------------------------------------------
 *                        Prototypes
 *----------------------------------------------------------------------------*/

// ----- 1D -----
cpl_error_code  kmo_arithmetic_1D_1D(
                            kmclipm_vector *op1,
                            const kmclipm_vector *op2,
                            kmclipm_vector *op1_noise,
                            const kmclipm_vector *op2_noise,
                            const char *op);

cpl_error_code  kmo_arithmetic_1D_scalar(
                            kmclipm_vector *op1,
                            double op2,
                            kmclipm_vector *op1_noise,
                            const char *op);

// ----- 2D -----
cpl_error_code  kmo_arithmetic_2D_2D(
                            cpl_image *op1,
                            const cpl_image *op2,
                            cpl_image *op1_noise,
                            const cpl_image *op2_noise,
                            const char *op);

cpl_error_code  kmo_arithmetic_2D_scalar(
                            cpl_image *op1,
                            double op2,
                            cpl_image *op1_noise,
                            const char *op);

// ----- 3D -----
cpl_error_code  kmo_arithmetic_3D_3D(
                            cpl_imagelist *op1,
                            const cpl_imagelist *op2,
                            cpl_imagelist *op1_noise,
                            const cpl_imagelist *op2_noise,
                            const char *op);

cpl_error_code  kmo_arithmetic_3D_2D(
                            cpl_imagelist *op1,
                            const cpl_image *op2,
                            cpl_imagelist *op1_noise,
                            const cpl_image *op2_noise,
                            const char *op);

cpl_error_code  kmo_arithmetic_3D_1D(
                            cpl_imagelist *op1,
                            const kmclipm_vector *op2,
                            cpl_imagelist *op1_noise,
                            const kmclipm_vector *op2_noise,
                            const char *op);

cpl_error_code  kmo_arithmetic_3D_scalar(
                            cpl_imagelist *op1,
                            double op2,
                            cpl_imagelist *op1_noise,
                            const char *op);

#endif
