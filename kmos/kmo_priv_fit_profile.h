/* $Id: kmo_priv_fit_profile.h,v 1.1.1.1 2012-01-18 09:31:59 yjung Exp $
 *
 * This file is part of the KMOS Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: yjung $
 * $Date: 2012-01-18 09:31:59 $
 * $Revision: 1.1.1.1 $
 * $Name: not supported by cvs2svn $
 */

#ifndef KMOS_PRIV_FIT_PROFILE_H
#define KMOS_PRIV_FIT_PROFILE_H

/*-----------------------------------------------------------------------------
 *                              Includes
 *----------------------------------------------------------------------------*/

#include <stdio.h>
#include <string.h>

#include <cpl.h>

/*------------------------------------------------------------------------------
 *                        Prototypes
 *----------------------------------------------------------------------------*/

cpl_vector*     kmo_fit_profile_1D(
                        cpl_vector *lambda_in,
                        const cpl_vector *data_in,
                        cpl_vector *noise_in,
                        const char *method,
                        cpl_vector **data_out,
                        cpl_propertylist **pl);

cpl_vector*     kmo_fit_profile_2D(
                        const cpl_image *data_in,
                        const cpl_image *noise_in,
                        const char *method,
                        cpl_image **data_out,
                        cpl_propertylist **pl);

cpl_vector*     kmo_vector_fit_gauss(
                        const cpl_vector *x,
                        const cpl_vector *y,
                        cpl_vector *noise,
                        double max_pos,
                        cpl_propertylist **pl);

cpl_vector*     kmo_vector_fit_moffat(
                        cpl_vector *x,
                        const cpl_vector *y,
                        cpl_vector *noise,
                        double max_pos,
                        double max_val,
                        cpl_propertylist **pl);

cpl_vector * kmo_vector_fit_lorentz(
        cpl_vector          *   x,
        const cpl_vector    *   y,
        cpl_vector          *   noise,
        double                  peak_pos,
        double                  peak_val,
        double                  background_val,
        cpl_propertylist    **  pl,
        int                     fit_linear) ;

cpl_vector*     kmo_image_fit_gauss(
                        const cpl_image *img,
                        const cpl_image *noise,
                        cpl_propertylist **pl);

cpl_vector*      kmo_image_fit_moffat(
                        const cpl_image *img,
                        const cpl_image *noise,
                        cpl_propertylist **pl);

int             kmo_priv_gauss1d_fnc(
                        const double x[],
                        const double a[],
                        double *result);

int             kmo_priv_moffat1d_fnc(
                        const double x[],
                        const double a[],
                        double *result);

int             kmo_priv_moffat1d_fncd(
                        const double x[],
                        const double a[],
                        double result[]);

int             kmo_priv_lorentz1d_fnc(
                        const double x[],
                        const double a[],
                        double *result);

int             kmo_priv_lorentz1d_fncd(
                        const double x[],
                        const double a[],
                        double result[]);

int             kmo_priv_gauss2d_fnc(
                        const double x[],
                        const double a[],
                        double *result);

int             kmo_priv_gauss2d_fncd(
                        const double x[],
                        const double a[],
                        double result[]);

int             kmo_priv_moffat2d_fnc(
                        const double x[],
                        const double a[],
                        double *result);

int             kmo_priv_moffat2d_fncd(
                        const double x[],
                        const double a[],
                        double result[]);

#endif
