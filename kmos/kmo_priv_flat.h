/*
 * This file is part of the KMOS Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef KMOS_PRIV_FLAT_H
#define KMOS_PRIV_FLAT_H

/*------------------------------------------------------------------------------
 *                        Includes
 *----------------------------------------------------------------------------*/

#include <cpl.h>

#include "kmclipm_vector.h"

extern double slit_tol;

/*------------------------------------------------------------------------------
 *                        Prototypes
 *----------------------------------------------------------------------------*/

int * kmo_split_frame(const cpl_image *xcal) ;

int kmo_imagelist_get_saturated(
        const cpl_imagelist     *   data,
        float                       threshold,
        int                         sat_min) ;

cpl_image * kmo_create_bad_pix_flat_thresh(
        const cpl_image *   data,
        int                 surrounding_pixels,
        int                 badpix_thresh) ;

cpl_error_code kmo_calc_curvature(
        cpl_image   *   combined_data,
        cpl_image   *   combined_noise,
        cpl_array   *   ifu_inactive,
        cpl_image   *   badpixel_mask,
        const int       detector_nr,
        cpl_image   **  xcal,
        cpl_image   **  ycal,
        double      *   gapmean,
        double      *   gapsdv,
        double      *   gapmaxdev,
        double      *   slitmean,
        double      *   slitsdv,
        double      *   slitmaxdev,
        cpl_table   *** edgepars_tbl) ;

cpl_error_code kmos_calc_edgepars(
        const cpl_image     *   combined_data,
        cpl_array           *   ifu_inactive,
        const cpl_image     *   badpixel_mask,
        const int               detector_nr,
        cpl_vector          *** slitlet_ids,
        cpl_matrix          *** edgepars) ;

cpl_table ** kmo_edgepars_to_table(
        cpl_vector  **  slitlet_ids,
        cpl_matrix  **  edgepars) ;

#endif
