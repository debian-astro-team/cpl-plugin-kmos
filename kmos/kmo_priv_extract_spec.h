/* $Id: kmo_priv_extract_spec.h,v 1.2 2013-01-11 08:12:24 aagudo Exp $
 *
 * This file is part of the KMOS Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: aagudo $
 * $Date: 2013-01-11 08:12:24 $
 * $Revision: 1.2 $
 * $Name: not supported by cvs2svn $
 */

#ifndef KMOS_PRIV_EXTRACT_SPEC_H
#define KMOS_PRIV_EXTRACT_SPEC_H

/*-----------------------------------------------------------------------------
 *                              Includes
 *----------------------------------------------------------------------------*/

#include <cpl.h>

/*------------------------------------------------------------------------------
 *                        Prototypes
 *----------------------------------------------------------------------------*/

cpl_error_code      kmo_priv_extract_spec(
                                const cpl_imagelist *data_in,
                                const cpl_imagelist *noise_in,
                                cpl_image     *mask,
                                cpl_vector    **spec_data_out,
                                cpl_vector    **spec_noise_out);

cpl_propertylist*   kmo_priv_update_header(
                                cpl_propertylist *header);

#endif
