/* $Id: kmo_priv_noise_map.h,v 1.2 2013-01-23 08:12:01 aagudo Exp $
 *
 * This file is part of the KMOS Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: aagudo $
 * $Date: 2013-01-23 08:12:01 $
 * $Revision: 1.2 $
 * $Name: not supported by cvs2svn $
 */

#ifndef KMOS_PRIV_NOISE_MAP_H
#define KMOS_PRIV_NOISE_MAP_H

/*-----------------------------------------------------------------------------
 *                              Includes
 *----------------------------------------------------------------------------*/

#include <cpl.h>

/*------------------------------------------------------------------------------
 *                        Prototypes
 *----------------------------------------------------------------------------*/
extern int print_warning_once_noise;

cpl_image*  kmo_calc_noise_map(cpl_image *data,
                               double gain,
                               double readnoise);

double      kmo_calc_readnoise_ndr(int ndsamples);

#endif
