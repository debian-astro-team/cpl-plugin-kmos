common KMO_WAVE_DATA, $
  cal_dir, data_dir, data_prefix, inputTable, cal_file_pattern, $
  table_changed, selected_lambda
  
function printUsage
  COMPILE_OPT IDL2, HIDDEN
  ind="    "
  tab=string(9b)
  usage=[$
  "This command line arguments are requested:",$
  ind+"--data_dir"+tab+"directory were data files produced by the KMO_WAVE recipe reside",$
  ind+"--prefix"+tab+"file prefix choosen with the the KMO_WAVE recipe",$
  ind+"--cal_dir"+tab+"directory to find the line list FITS tables",$
  "",$
  "Following options can be used",$
  ind+"--table"+tab+"set of reference line to start with",$
  ind+"--pattern"+tab+"file pattern for line list FITS tables",$
  ""]
  for i =0,n_elements(usage)-1 do print,usage[i]
  msg=''
  for i =0,n_elements(usage)-1 do msg = msg + usage[i] + string(10b)
  return,msg
end

;pro kmo_wave_calib, cal_dir_arg, data_dir_arg, data_prefix_arg, inputTable_arg, cal_file_pattern_arg
;kmo_wave_calib,"/home/erw/kmos/data/edinburgh/kmos-calib/kmos/","/home/erw/kmos/data/edinburgh/pipeline2/",".wave_cal_data","/home/erw/kmos/data/edinburgh/kmos-calib/kmos/kmo_wave_ref_table.fits","/home/erw/kmos/data/edinburgh/kmos-calib/kmos/"
pro kmo_wave_calib, _extra=params
  COMPILE_OPT IDL2, HIDDEN
  common KMO_WAVE_DATA
;  common KMO_WAVE, test
  table_changed=0
  selected_lambda=0.

  device,retain=2

  if n_elements(params) ne 0 then begin
    param_cnt = n_tags(params)
    print, 'Found following parameters in the call of KMO_WAVE_CALIB:'
    keys=tag_names(params)
    values=params.(0)
    params_array = strarr(param_cnt)
    for i=0,param_cnt-1 do begin
      params_array[i] = keys[i] + '=' + params.(i)
      print,'    ', params_array[i]
    endfor
  endif else begin
    param_cnt = 0
  endelse

;
; either command line args or the arguments for this procedure must be used
;
  args = command_line_args(count=arg_cnt)
  if (arg_cnt ne 0) then begin
    print, 'Found following command line arguments (which will override the parameters):'
    for i=0,arg_cnt-1 do print, '    ', args[i]
  endif
  
  if            (param_cnt eq 0 and arg_cnt ne 0) then begin
    options = args
  endif else if (param_cnt ne 0 and arg_cnt eq 0) then begin
    options = params_array
  endif else if (param_cnt ne 0 and arg_cnt ne 0) then begin
    options = [params_array, args]
  endif
;
; parse command line arguments
;
  options_error = 0
  band = ''
  for ix=0,(param_cnt+arg_cnt)-1 do begin
    arg = options[ix]
    if (strmid(arg,0,2) eq "--") then begin
      arg = strmid(arg,2)
    endif else if (strmid(arg,0,1) eq "-") then begin
      arg = strmid(arg,1)
    endif
    field = strsplit(arg,"=",count=cnt,/extract)
    if (cnt eq 1) then begin
      print, 'command line argument/option must have the form key=value, missing "="'
      options_error = 1
    endif
    if (cnt gt 2) then for n = 2,cnt-1 do field[1] = field[1] + "=" + field[n]
    case strlowcase(field[0]) of
      'data-dir': data_dir = field[1] + "/"
      'data_dir': data_dir = field[1] + "/"
      'cal-dir':  cal_dir = field[1] + "/"
      'cal_dir':  cal_dir = field[1] + "/"
      'prefix':   data_prefix = field[1]
      'pattern':  cal_file_pattern = field[1]
      'table':    inputTable = field[1]
      'print':    band = field[1]
      else: begin
        print, 'Unknown command line argument/option "' + field[0] + '"'
        options_error = 1
      endelse
    endcase
  endfor
;
; parse parameters for this procedure
;
  if (n_elements(cal_dir) eq 0) then begin
    print, "Missing command line argument: cal_dir'
    options_error = 1
  endif
  if (n_elements(data_dir) eq 0) then begin
    print, "Missing command line argument: data_dir'
    options_error = 1
  endif
  if (n_elements(data_prefix) eq 0) then begin
    print, "Missing command line argument: prefix'
    options_error = 1
  endif
 

  if (n_elements(cal_file_pattern) eq 0) then begin
    cal_file_pattern = "kmos_ar_ne_list_%s.fits"
    print, 'File pattern for line list tables defaults to ', cal_file_pattern
  endif
  pattern_pos = strsplit(CAL_FILE_PATTERN,'%[sS]',/regex)
  if (n_elements(pattern_pos) ne 2) then begin
    print, 'File pattern for line list FITS tables must contain one and only one "%s" (or "%S") character pair' 
    options_error = 1
  endif

  if (options_error) then message,printUsage()
  
  if (n_elements(inputTable) eq 0) then inputtable=''
  if (inputtable eq '' or inputtable eq ' ' ) then begin
    init_reference_lines
  endif else begin
    loadfits, inputTable
  endelse
 
  if (band eq '') then begin
    line_edit_menu, base, inputTable
    xmanager,'line_edit_menu',base
  endif else begin
    print_band, band
  endelse

end
;
;kmo_wave_calib
;end