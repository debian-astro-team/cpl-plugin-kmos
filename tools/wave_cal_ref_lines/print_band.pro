pro print_band, band

  common KMO_WAVE_DATA
  
  old_device = !D.NAME
  set_plot,'ps'
  device, file='band_'+band+'.ps',/landscape
  !p.multi=[0,0,3,0,0]

  title_fmt='("prefix: ",A," , band: ",A," , detector: ",i1)'

  for detector=1,3 do begin
    traces=read_traces(band,detector)
    size = size(traces)
    n_traces=size[1]
    xsize=size[2]

    title=string(format=title_fmt,data_prefix, band, detector)
    tmp=traces[63,*]
    plot,title=title, $
      xrange=[0,xsize-1],yrange=[-50,100],xstyle=1,ystyle=1,tmp
  endfor
  goto, RESET_GRAPHICS

  for detector=1,3 do begin
    traces=read_traces(band,detector)
    size = size(traces)
    n_traces=size[1]
    xsize=size[2]

    title=string(format=title_fmt,data_prefix, band, detector)
    plot,title=title, $
      xrange=[0,xsize-1],yrange=[min(traces),max(traces)],xstyle=1,ystyle=1,$
      traces[0,*]
    for t=1,n_traces-1 do oplot,traces[t,*]
    
  endfor
  
  for detector=1,3 do begin
    traces=read_traces(band,detector)
    size = size(traces)
    n_traces=size[1]
    xsize=size[2]

    title=string(format=title_fmt,data_prefix, band, detector)
    tmp=traces[63,*]
    plot,title=title, $
      xrange=[0,xsize-1],yrange=[min(tmp),max(tmp)],xstyle=1,ystyle=1,tmp
  endfor

  for detector=1,3 do begin
    traces=read_traces(band,detector)
    size = size(traces)
    n_traces=size[1]
    xsize=size[2]
   
    tmp=traces[0,*]
    tmp[where(tmp lt 1.0)]=1.0
    plot,title=title, $
      xrange=[0,xsize-1],yrange=[min(tmp),max(tmp)],xstyle=1,ystyle=1,$
      tmp,/ylog
    for t=1,n_traces-1 do begin
      tmp=traces[t,*]
      tmp[where(tmp lt 1.0)]=1.0
      oplot,tmp
    endfor
  endfor

  for detector=1,3 do begin
    traces=read_traces(band,detector)
    size = size(traces)
    n_traces=size[1]
    xsize=size[2]

    title=string(format=title_fmt,data_prefix, band, detector)
    tmp=traces[63,*]
    tmp[where(tmp lt 1.0)]=1.0
    plot,title=title, $
      xrange=[0,xsize-1],yrange=[min(tmp),max(tmp)],xstyle=1,ystyle=1,tmp,/ylog
    
  endfor

RESET_GRAPHICS:
  device,/close
  !p.multi=0
  set_plot,old_device
end
