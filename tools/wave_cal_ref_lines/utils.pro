common KMO_WAVE_RL,                                                     $
  reference_lines_H_1, reference_lines_H_2, reference_lines_H_3,    $
  reference_lines_K_1, reference_lines_K_2, reference_lines_K_3,    $
  reference_lines_HK_1, reference_lines_HK_2, reference_lines_HK_3, $
  reference_lines_IZ_1, reference_lines_IZ_2, reference_lines_IZ_3, $
  reference_lines_YJ_1, reference_lines_YJ_2, reference_lines_YJ_3

pro reference_line_struct__define
  COMPILE_OPT IDL2, HIDDEN
  tmp = {reference_line_struct, $
                  lambda:0.0, ref:-1, offset:0, range:200, cut:1000}
end

pro save_reference_lines, band, detector, reference_lines
  COMPILE_OPT IDL2, HIDDEN
  common KMO_WAVE_DATA
  common KMO_WAVE_RL
  case band of
    'H': begin
        case detector of
          1: reference_lines_H_1 = reference_lines
          2: reference_lines_H_2 = reference_lines
          3: reference_lines_H_3 = reference_lines
        endcase
      end
    'K': begin
        case detector of
          1: reference_lines_K_1 = reference_lines
          2: reference_lines_K_2 = reference_lines
          3: reference_lines_K_3 = reference_lines
        endcase
      end
    'HK': begin
        case detector of
          1: reference_lines_HK_1 = reference_lines
          2: reference_lines_HK_2 = reference_lines
          3: reference_lines_HK_3 = reference_lines
        endcase
      end
    'IZ': begin
        case detector of
          1: reference_lines_IZ_1 = reference_lines
          2: reference_lines_IZ_2 = reference_lines
          3: reference_lines_IZ_3 = reference_lines
        endcase
      end
    'YJ': begin
        case detector of
          1: reference_lines_YJ_1 = reference_lines
          2: reference_lines_YJ_2 = reference_lines
          3: reference_lines_YJ_3 = reference_lines
        endcase
      end
  endcase
  table_changed = 1
end

function load_reference_lines, band, detector
  COMPILE_OPT IDL2, HIDDEN
  common KMO_WAVE_RL
  case band of
    'H': begin
        case detector of
          1: reference_lines = reference_lines_H_1
          2: reference_lines = reference_lines_H_2
          3: reference_lines = reference_lines_H_3
        endcase
      end
    'K': begin
        case detector of
          1: reference_lines = reference_lines_K_1
          2: reference_lines = reference_lines_K_2
          3: reference_lines = reference_lines_K_3
        endcase
      end
    'HK': begin
        case detector of
          1: reference_lines = reference_lines_HK_1
          2: reference_lines = reference_lines_HK_2
          3: reference_lines = reference_lines_HK_3
        endcase
      end
    'IZ': begin
        case detector of
          1: reference_lines = reference_lines_IZ_1
          2: reference_lines = reference_lines_IZ_2
          3: reference_lines = reference_lines_IZ_3
        endcase
      end
    'YJ': begin
        case detector of
          1: reference_lines = reference_lines_YJ_1
          2: reference_lines = reference_lines_YJ_2
          3: reference_lines = reference_lines_YJ_3
        endcase
      end
  endcase
  return, reference_lines
end

pro init_reference_lines
  COMPILE_OPT IDL2, HIDDEN
  common KMO_WAVE_DATA
  common KMO_WAVE_RL

  reference_line = {reference_line_struct}
  reference_line.lambda=0.
  reference_line.ref=-1
  reference_line.offset=1000
  reference_line.range=200
  reference_line.cut=200

  reference_lines_H_1 = replicate(reference_line,1)
  reference_lines_H_2 = replicate(reference_line,1)
  reference_lines_H_3 = replicate(reference_line,1)

  reference_lines_K_1 = replicate(reference_line,1)
  reference_lines_K_2 = replicate(reference_line,1)
  reference_lines_K_3 = replicate(reference_line,1)

  reference_lines_HK_1 = replicate(reference_line,1)
  reference_lines_HK_2 = replicate(reference_line,1)
  reference_lines_HK_3 = replicate(reference_line,1)

  reference_lines_IZ_1 = replicate(reference_line,1)
  reference_lines_IZ_2 = replicate(reference_line,1)
  reference_lines_IZ_3 = replicate(reference_line,1)

  reference_lines_YJ_1 = replicate(reference_line,1)
  reference_lines_YJ_2 = replicate(reference_line,1)
  reference_lines_YJ_3 = replicate(reference_line,1)
  table_changed = 0
end

pro save2c
  COMPILE_OPT IDL2, HIDDEN
  bands = ['H', 'K' , 'HK', 'IZ', 'YJ']
  dets = [1, 2, 3]
  
;  csavefile = dialog_pickfile(default='h',DIALOG_PARENT=base)
  csavefile = '/home/erw/kmos/workspace/kmosp/kmos/kmo_priv_wave_cal_lines.h'
  openw, saveunit, csavefile, /get_lun
  first=1
  fmt_band_switch_if = '("if  (strcmp(filter_id, ",A,A,A,") == 0) {")'
  fmt_band_switch_else = '("} else if  (strcmp(filter_id, ",A,A,A,") == 0) {")'
  fmt_det_switch = '(T4,A)'
  fmt_n_rl = '(T8,"n_rl = ",I0,";")'
  fmt_double_array_label = '(T8,"double ",A,A,I1,"[] = { ",$)'
  fmt_double_array = '(D8.6,", ",$)'
  fmt_double_array_last = '(D8.6,"};")'
  fmt_int_array_label = '(T8,"int ",A,A,I1,"[] = { ",$)'
  fmt_int_array = '(I0,", ",$)'
  fmt_int_array_last = '(I0,"};")'
  fmt_ref = '(T8,A," = ",A,"_",A,I1,";")'
  fmt_last_det = '(T4,"}")'
  fmt_last_band = '("}")'
 
  fmt_repl='("reference_lines_",A,"_",I1,"  = replicate(reference_line, ",I0,")")'
  fmt_def='("reference_lines_",A,"_",I1,"[",I0,"] = {reference_line_struct, ",D8.6,", ",I0,", ",I0,", ",I0,", ",I0,"}")'
  fmt_band_switch = fmt_band_switch_if
  for b = 0,n_elements(bands)-1 do begin
    printf, saveunit, format=fmt_band_switch, '"', bands[b], '"'
    for det = 1,n_elements(dets) do begin
      rlx = load_reference_lines (bands[b], det)
      case det of
        1: printf, saveunit, format=fmt_det_switch, 'if (global_ifu_nr < 9) {'
        2: printf, saveunit, format=fmt_det_switch, '} else if (global_ifu_nr < 17) {'
        3: printf, saveunit, format=fmt_det_switch, '} else {'
      endcase
      printf, saveunit, format=fmt_n_rl, n_elements(rlx)
      printf, saveunit, format=fmt_double_array_label, 'rl_wl_',bands[b],det
      for i=0,n_elements(rlx)-2 do $
        printf, saveunit, format=fmt_double_array, rlx[i].lambda
      printf, saveunit, format=fmt_double_array_last, rlx[i].lambda
      
      printf, saveunit, format=fmt_int_array_label, 'rl_ref_',bands[b],det
      for i=0,n_elements(rlx)-2 do $
        printf, saveunit, format=fmt_int_array, rlx[i].ref
      printf, saveunit, format=fmt_int_array_last, rlx[i].ref
      
      
      printf, saveunit, format=fmt_int_array_label, 'rl_offset_',bands[b],det
      for i=0,n_elements(rlx)-2 do $
        printf, saveunit, format=fmt_int_array, rlx[i].offset
      printf, saveunit, format=fmt_int_array_last, rlx[i].offset
      
      
      printf, saveunit, format=fmt_int_array_label, 'rl_range_',bands[b],det
      for i=0,n_elements(rlx)-2 do $
        printf, saveunit, format=fmt_int_array, rlx[i].range
      printf, saveunit, format=fmt_int_array_last, rlx[i].range
      
      
      printf, saveunit, format=fmt_int_array_label, 'rl_cut_',bands[b],det
      for i=0,n_elements(rlx)-2 do $
        printf, saveunit, format=fmt_int_array, rlx[i].cut
      printf, saveunit, format=fmt_int_array_last, rlx[i].cut
      
      printf, saveunit, format=fmt_ref, 'rl_wl', 'rl_wl', bands[b], det
      printf, saveunit, format=fmt_ref, 'rl_ref', 'rl_ref', bands[b], det
      printf, saveunit, format=fmt_ref, 'rl_offset', 'rl_offset', bands[b], det
      printf, saveunit, format=fmt_ref, 'rl_range', 'rl_range', bands[b], det
      printf, saveunit, format=fmt_ref, 'rl_cut', 'rl_cut', bands[b], det
    endfor
    printf, saveunit,  format=fmt_last_det
    fmt_band_switch = fmt_band_switch_else
  endfor
  printf, saveunit,  format=fmt_last_band

  close, saveunit
end

pro save2idl
  COMPILE_OPT IDL2, HIDDEN
  bands = ['H', 'K' , 'HK', 'IZ', 'YJ']
  dets = [1, 2, 3]
  
;  idlsavefile = dialog_pickfile(default='pro',DIALOG_PARENT=base)
  idlsavefile = '/home/erw/reference_lines.pro'
  openw, saveunit, idlsavefile, /get_lun
  printf, saveunit, 'common KMO_WAVE_RL, $'
  printf, saveunit, '      reference_lines_H_1, reference_lines_H_2, reference_lines_H_3, $'
  printf, saveunit, '      reference_lines_K_1, reference_lines_K_2, reference_lines_K_3, $'
  printf, saveunit, '      reference_lines_HK_1, reference_lines_HK_2, reference_lines_HK_3, $'
  printf, saveunit, '      reference_lines_IZ_1, reference_lines_IZ_2, reference_lines_IZ_3, $'
  printf, saveunit, '      reference_lines_YJ_1, reference_lines_YJ_2, reference_lines_YJ_3'
  printf, saveunit, 'reference_line = {reference_line_struct, $'
  printf, saveunit, '          lambda:0.0, ref:-1, offset:0, range:2047, cut:1000}'
  printf, saveunit, ''
  fmt_repl='("reference_lines_",A,"_",I1,"  = replicate(reference_line, ",I0,")")'
  fmt_def='("reference_lines_",A,"_",I1,"[",I0,"] = {reference_line_struct, ",D8.6,", ",I0,", ",I0,", ",I0,", ",I0,"}")'
  for b = 0,n_elements(bands)-1 do begin
    for det = 1,n_elements(dets) do begin
      rlx = load_reference_lines (bands[b], det)
      printf, saveunit, format=fmt_repl, bands[b], det, n_elements(rlx)
      for i=0,n_elements(rlx)-1 do $
        printf, saveunit, format=fmt_def, bands[b], det, i, $
          rlx[i].lambda, rlx[i].ref, rlx[i].offset, rlx[i].range, rlx[i].cut
    endfor
    printf, saveunit, ''
  endfor
  printf, saveunit,  ''

  close, saveunit
end

pro open_ref_table
  COMPILE_OPT IDL2, HIDDEN
  common KMO_WAVE_DATA
  filename = dialog_pickfile(title='Select reference line table FITS file',path=cal_dir,filter='*.fits',/MUST_EXIST)
  if filename ne '' then loadfits, filename
  return
end

pro save2fits
  COMPILE_OPT IDL2, HIDDEN
  common KMO_WAVE_DATA
  
  bands = ['H', 'K' , 'HK', 'IZ', 'YJ']
  dets = [1, 2, 3]
  n_rows = 0
  for b = 0,n_elements(bands)-1 do begin
    for d = 0,n_elements(dets)-1 do begin
      rlx = load_reference_lines (bands[b], dets[d])
      n_rows = n_rows + n_elements(rlx)
    endfor
  endfor
  fitstable = replicate({filter:'H', detector:0, wavelength:0.D, reference:0, offset:0, range:0, cut:0}, n_rows)
  rix=0
  for b = 0,n_elements(bands)-1 do begin
    for d = 0,n_elements(dets)-1 do begin
      rlx = load_reference_lines (bands[b], dets[d])
      for r = 0,n_elements(rlx)-1 do begin
        fitstable[rix].filter = bands[b]
        fitstable[rix].detector = dets[d]
        fitstable[rix].wavelength = rlx[r].lambda
        fitstable[rix].reference = rlx[r].ref
        fitstable[rix].offset = rlx[r].offset
        fitstable[rix].range = rlx[r].range
        fitstable[rix].cut = rlx[r].cut
        rix++
      endfor
    endfor
  endfor
  pheader=[ $
  "HIERARCH ESO PRO CATG = 'REF_LINES'", $
  "INSTRUME = 'KMOS'", $
  "DATE    = '" + $
    string(FORMAT='(C(CYI4.4,"-",CMOI2.2,"-",CDI2.2,"T",CHI2.2,":",CMI2.2,":",CSI2.2))',SYSTIME(/JULIAN,/utc)) + $
    "' / file creation date (YYYY-MM-DDThh:mm:ss UT)", $
  "END     "]
  eheader=[ $
  "EXTNAME = 'LIST    '", $
  "COMMENT   This extension holds the reference lines for KMO wava_cal", $
  "END     "]
  fitsfilename = dialog_pickfile(title='Select file to store reference lines in a FITS file',path=cal_dir,filter='*.fits',/OVERWRITE_PROMPT,/write)
  if fitsfilename ne '' then begin
    mwrfits, undefinedInput, fitsfilename, pheader, /create, /silent
    mwrfits, fitstable, fitsfilename, eheader, /silent
  endif
end

pro loadfits, filename
  COMPILE_OPT IDL2, HIDDEN
  common KMO_WAVE_DATA

  reference_line = {reference_line_struct}
  fitstable = mrdfits(filename,1,/silent)
  bands = ['H ', 'K ' , 'HK', 'IZ', 'YJ']
  dets = [1, 2, 3]
  n_rows = 0
  for b = 0,n_elements(bands)-1 do begin
    for d = 0,n_elements(dets)-1 do begin
      rx = where((fitstable.filter eq bands[b]) and (fitstable.detector eq dets[d]),cnt)
      if (cnt ne 0) then begin
        reference_lines = replicate(reference_line,cnt)
        for ix = 0, cnt-1 do begin
          reference_lines[ix].LAMBDA = fitstable[rx[ix]].WAVELENGTH
          reference_lines[ix].REF = fitstable[rx[ix]].REFERENCE
          reference_lines[ix].OFFSET = fitstable[rx[ix]].OFFSET
          reference_lines[ix].RANGE = fitstable[rx[ix]].RANGE
          reference_lines[ix].CUT = fitstable[rx[ix]].CUT
        endfor
      endif else begin
        reference_lines = replicate(reference_line,1)
      endelse
      save_reference_lines, strtrim(bands[b],2), dets[d], reference_lines
    endfor
  endfor
  table_changed = 0
end

function read_traces, band, detector
  COMPILE_OPT IDL2, HIDDEN
  common KMO_WAVE_DATA
  
  prefix= data_dir + data_prefix + '_' + band + '_ifu_'
  case detector of
    1: file_pattern = prefix + '{01,02,03,04,05,06,07,08}*'
    2: file_pattern = prefix + '{09,10,11,12,13,14,15,16}*'
    3: file_pattern = prefix + '{17,18,19,20,21,22,23,24}*'
  endcase
  files = file_search(file_pattern)
  if (n_elements(files) ne 1) then begin
    n_traces=n_elements(files)
    traces=dindgen(n_traces,2048)
    if (1) then begin
      for i = 0,n_traces-1 do traces[i,*]=mrdfits(files[i],"trace",/silent)
    endif
  endif else begin
    traces=0
  endelse
  
  return, traces
end

function read_line_list, band
  COMPILE_OPT IDL2, HIDDEN
  common KMO_WAVE_DATA
  
  pattern_pos = strsplit(CAL_FILE_PATTERN,'%[sS]',/regex)
  if (n_elements(pattern_pos) ne 2) then return,0
  pat1 = strmid(CAL_FILE_PATTERN,pattern_pos[0],pattern_pos[1]-2)
  pat2 = strmid(CAL_FILE_PATTERN,pattern_pos[1])
  bcase = strmid(CAL_FILE_PATTERN,pattern_pos[1]-1,1)
  if (bcase eq 's') then begin
    file = pat1 + string(format='(A)',strlowcase(band)) + pat2
  endif else begin
    file = pat1 + string(format='(A)',strupcase(band)) + pat2
  endelse
  ll_table=mrdfits(cal_dir+file,1,/silent)
  return, ll_table
end
