pro edit_reference_lines, band, detector
  COMPILE_OPT IDL2, HIDDEN
  ref_lines = load_reference_lines(band, detector)
  traces = read_traces(band, detector)
  size = size(traces)
  n_traces=size[1]
   
  base = widget_base(column=1, /tlb_size_events, $
    title=string(format='("Find reference lines for band: ",A,"   detector: ",I1)',band,detector))
  geom =  widget_info(base,/geometry)
  
  draw = widget_draw(base,/button_events,event_pro=edit_reference_lines_event)
  widget_control, draw, get_value=draw_idx

  base2 = widget_base(base,/row)
  table = widget_table(base2,/row_major,value=ref_lines,column_labels=tag_names(ref_lines))
  base3 = widget_base(base2,/column)
  base4 = widget_base(base3,/row)
  base6 = widget_base(base3,/row)
  base7 = widget_base(base3,/row)
  base5 = widget_base(base3,/row)
  b_new = widget_button(base4,value='New',uvalue='NEW')
  b_edit = widget_button(base4,value='Edit',uvalue='EDIT')
  b_delete = widget_button(base4,value='Delete',uvalue='DELETE')
  b_copy = widget_button(base4,value='Copy',uvalue='COPY')
  
  abs_idx=where(ref_lines.ref eq -1, rcnt)
  if (rcnt eq 0) then rval=[-1] else rval=[-1,abs_idx]
  ref_label=widget_label(base6,value='reference line in plot')
  ref_dl = widget_droplist(base6,value=string(rval,format='(I-3)'),uname='REF_LINE_DL')
  
  b_plot_refpos = widget_button(base7,value='Plot ref. pos.',uvalue='PLOTREFPOS')
  
  b_save = widget_button(base5,value='Save',uvalue='SAVE')
  b_quit = widget_button(base5,value='Quit',uvalue='QUIT')

;  state = {edit_reference_lines_state_struct, band:band, detector:detector, $
  state = { band:band, detector:detector, $
    rl:replicate({reference_line_struct},20) , n_rl:0, rl_ix:0, modified:0, $
    traces:traces, n_traces:n_traces, $
    base_wid:base, draw_wid:draw, base_bottom_wid:base2, table_wid:table, $
    REFPOS_WINDOW:-3L, dlx:0, ref_line:-1}
  state.rl = ref_lines
  state.n_rl = n_elements(ref_lines)
  widget_control, base, set_uvalue=state
  

  basegeom = widget_Info(base, /Geometry)   
;  widget_control,draw,xsize=basegeom.xsize,ysize=400
  widget_control,draw,xsize=1900,ysize=400
  widget_control, base,/realize
  
;  state.refpos_window = -3
  edit_reference_lines_plot, state
  xmanager,'edit_reference_lines',base
end

pro edit_reference_lines_plot,state
  COMPILE_OPT IDL2, HIDDEN

;  geom =  widget_info(base,/geometry)
;  draw = widget_draw(base,/button_events,event_pro=edit_reference_lines_event,xsize=geom.xsize,ysize=geom.ysize)
  widget_control, state.draw_wid, get_value=draw_idx
  wset,draw_idx

  refidx = state.ref_line
  traces = state.traces
  n_traces = state.n_traces
  xdim = n_elements(traces[1,*])
  if (state.n_rl gt 0) then $
    rp = get_reference_positions(state.rl[0:state.n_rl-1], state.n_rl, traces, n_traces)
  
  if (refidx eq -1) then begin
    xrng = [0,xdim]
  endif else begin
    xrng = [-max(rp[*,refidx]),xdim-min(rp[*,refidx])]
  endelse

  plot,[0,0],xrange=xrng,yrange=[min(traces),max(traces)],xstyle=1,ystyle=1
  for i = 0,n_traces-1 do begin
    if (refidx eq -1) then toffset = 0 else toffset = rp[i,refidx]
    oplot,indgen(xdim)-toffset, traces[i,*]
  endfor
  if (refidx eq -1) then toffset = 0 else   toffset = mean(rp[*,refidx])
  for i = 0,state.n_rl-1 do begin
    cut = state.rl[i].cut
    if (state.rl[i].ref eq -1) then begin
      xstart = state.rl[i].offset
      xend = xstart + state.rl[i].range
      rcol = 'ff'x
      for j=0,n_traces-1 do begin
        if (refidx eq -1) then toffset = 0 else   toffset = rp[j,refidx]
        oplot,[xstart,xend]-toffset, [cut, cut], color=rcol
      endfor
    endif else begin
;      xstart = min(rp[*,state.rl[i].ref]) + state.rl[i].offset
;      xend = max(rp[*,state.rl[i].ref]) + state.rl[i].offset + state.rl[i].range
      rcol = 'ff00'x
      for j=0,n_traces-1 do begin
        xstart = min(rp[j,state.rl[i].ref]) + state.rl[i].offset
        xend = max(rp[j,state.rl[i].ref]) + state.rl[i].offset + state.rl[i].range
        if (refidx eq -1) then toffset = 0 else toffset = rp[j,refidx]
        oplot,[xstart,xend]-toffset, [cut, cut], color=rcol
      endfor
    endelse
  endfor
 
  widget_control, state.base_wid,/realize
  
  if (state.n_rl eq 0) then widget_control,state.table_wid,/delete_rows $
  else  widget_control,state.table_wid,set_value=state.rl[0:state.n_rl-1]
  
  edit_reference_lines_plot_refpos, state
end

pro edit_reference_lines_event, ev
  COMPILE_OPT IDL2, HIDDEN

;  print,'edit_reference_lines_event',ev.top,ev.id
;  help,ev,/struct
  widget_control, ev.top, get_uvalue=state
  evName=tag_names(ev,/struct)
  if (evName eq "WIDGET_BASE") then begin
    widget_control,ev.top,xsize=ev.x,ysize=ev.y
    base2geom = widget_Info(state.base_bottom_wid, /Geometry)   
    widget_control,state.draw_wid,xsize=ev.x,ysize=ev.y-base2geom.ysize,/real
  endif else if (evName eq "WIDGET_BUTTON") then begin
      widget_control,ev.id,get_uvalue=uval
      selection = widget_info(state.table_wid,/table_select)
      row_start = selection[1]
      row_end   = selection[3]
      case uval of
        'NEW' : begin
            if (state.n_rl lt n_elements(state.rl)) then begin
              state.modified = 1
              state.rl[state.n_rl] = replicate({reference_line_struct},1)
              state.n_rl = state.n_rl + 1;
              rl_ix = state.n_rl-1
              state.rl[rl_ix].lambda = 0.
              if state.ref_line eq -1 $
                then state.rl[rl_ix].offset = 1000 $
                else state.rl[rl_ix].offset = 0
              state.rl[rl_ix].range = 200
              state.rl[rl_ix].ref = state.ref_line
              state.rl[rl_ix].cut = 1000
              state.rl_ix = rl_ix
              widget_control, state.table_wid, insert_row=1, USE_TABLE_SELECT=[0,state.n_rl-1,0,state.n_rl]
              edit_single_line, ev.top, rl_ix, state
            endif else begin
              r=dialog_message(string(format='("No more than ",I0," reference lines can be defined")',n_elements(state.rl)))
            endelse
          end         
        'DELETE' : begin
            if (row_start lt 0 or row_end ge n_elements(state.rl)) then begin
              user = dialog_message('Please select one or more rows of the table',/info)
            endif else begin
              if (row_start eq row_end) then begin
                user = dialog_message('Do you really want to delete row ' + string(row_start),/question,/DEFAULT_NO)
              endif else begin
                user = dialog_message('Do you really want to delete rows ' + string(row_start) + ' to ' + string(row_end),/question,/DEFAULT_NO)
              endelse
              if (user eq "Yes") then begin
                state.modified = 1
                n_rl = state.n_rl
                x = indgen(n_rl)
                print,x
                del_ix = x[where(x ge row_start and x le row_end,cnt)]
                if (cnt eq n_rl) then begin
                  state.n_rl = 1
                  rl_ix = state.n_rl-1
                  state.rl[rl_ix].lambda = 0.
                  state.rl[rl_ix].offset = 1000
                  state.rl[rl_ix].range = 200
                  state.rl[rl_ix].ref = -1
                  state.rl[rl_ix].cut = 1000
                  state.rl_ix = rl_ix
                    endif else begin
                  keep_ix = x[where(x lt row_start or  x gt row_end)]
                  found_ref = 0
                  errmsg=''
                  for dx = 0,n_elements(del_ix)-1 do begin
                    for kx = 0,n_elements(keep_ix)-1 do begin
                      if (del_ix[dx] eq state.rl[keep_ix[kx]].ref) then begin
                        found_ref = 1
                        errmsg = errmsg + $
                          string(format='("line ",I0," is referenced by line ",I0,A)',$
                            del_ix[dx],keep_ix[kx],string(10b))
                      endif
                    endfor
                  endfor
                  if (found_ref) then begin
                    user = dialog_message("Cannot delete line(s) because" + string(10b) + errmsg) 
                  endif else begin
                    state.n_rl = n_elements(keep_ix)
                    state.rl[0:state.n_rl-1] = state.rl[keep_ix]
                    for kx = 0,n_elements(keep_ix)-1 do begin
                      if state.rl[kx].ref ne -1 then begin
                        newref = where(keep_ix eq state.rl[kx].ref,rcnt)
                        if rcnt eq 1 then state.rl[kx].ref = newref[0] $
                          else state.rl[kx].ref = -1
                      endif
                    endfor
                    newref = where(keep_ix eq state.ref_line,rcnt)
                    if rcnt eq 1 then state.ref_line = newref[0] $
                      else state.ref_line = -1
                  endelse
                endelse
              endif
            endelse
          end
        'EDIT' : begin
            if (row_start lt 0 or row_end ge n_elements(state.rl)) then begin
              user = dialog_message('Please select one row of the table',/info)
            endif else if (row_start ne row_end) then begin
              user = dialog_message('Only one row can be selected for editing',/info)
            endif else begin
              state.modified = 1
              rl_ix = row_start
              edit_single_line, ev.top, row_start, state
            endelse
          end
        'COPY' : begin
            form_desc = [ $
              '0,LABEL,Select band and detector from which you want to copy the reference lines', $
                '1,BASE,,ROW,FRAME', $
                  '0,LABEL,band:', $
                  '2,DROPLIST,H|K|HK|IZ|YJ,TAG=BAND', $
                '1,BASE,,ROW,FRAME', $
                  '0,LABEL,detector:', $
                  '2,DROPLIST,1|2|3,TAG=DETECTOR', $
                '1,BASE,,ROW,FRAME', $
                  '0,BUTTON,Quit,QUIT', $
                  '2,BUTTON,OK/Exit,QUIT,tag=OK']
              cwr = cw_form(form_desc,/col,title='Select band/detector')
              if cwr.OK eq 1 then begin
                 state.modified = 1
                 bands=['H','K','HK','IZ','YJ']
                 dets=[1,2,3]
                 ref_lines = load_reference_lines(bands[cwr.BAND], dets[cwr.DETECTOR])
                 state.rl = ref_lines
                 state.n_rl = n_elements(ref_lines)
               endif
          end
        'PLOTREFPOS' : begin
            if state.REFPOS_WINDOW eq -3 then state.REFPOS_WINDOW = -2
            edit_reference_lines_plot_refpos, state
          end
        'SAVE' : begin
            reference_lines = state.rl[0:state.n_rl-1]
            save_reference_lines, state.band , state.detector, reference_lines
            if (state.refpos_window ge 0) then begin
              wdelete, state.refpos_window
              state.refpos_window = -3
            endif
            widget_control, ev.top, /destroy
            return
          end
        'QUIT' : begin
            if state.modified then begin
              question = 'Reference lines have been changed but are not saved yet' +$
                string(10b) + 'Do you want to save them now?'
                user = dialog_message(question,/question)
                if (user eq "Yes") then begin
                  reference_lines = state.rl[0:state.n_rl-1]
                  save_reference_lines, state.band , state.detector, reference_lines
                endif
            endif
            widget_control, state.base_wid, /destroy
            if (state.refpos_window ge 0) then begin
              wdelete, state.refpos_window
              state.refpos_window = -3
            endif
            return
          end
      endcase
    endif else if (evName eq "WIDGET_DROPLIST") then begin
      widget_control, ev.id, get_value=arr
      state.ref_line = arr[ev.index]
      state.dlx = ev.index
    endif else begin
;      help, ev,/struct
  endelse
  
  abs_idx=where(state.rl[0:state.n_rl-1].ref eq -1, cnt)
  if (cnt eq 0) then newval=[-1] else newval=[-1,abs_idx]
  newidx = where(newval eq state.ref_line,icnt)
  if icnt eq 1 then newsel = newidx[0] else newsel = 0
  dl = widget_info(state.base_wid,find_by_uname='REF_LINE_DL')
  widget_control, dl, set_value=string(newval,format='(I-3)')
  widget_control, dl, set_droplist_select=newsel
  state.dlx = newsel
  state.ref_line = newval[newsel]
  
  edit_reference_lines_plot, state
  widget_control, ev.top, set_uvalue=state
end

pro edit_reference_lines_plot_refpos, state
  COMPILE_OPT IDL2, HIDDEN
 
  if (state.refpos_window eq -3) then return
  if (state.refpos_window eq -2) then begin
    window,/free
    state.refpos_window = !D.WINDOW
  endif
  catch,theError
  if theError ne 0 then begin   ; error selecting window, may be deleted by user
    state.refpos_window = -3
    catch,/cancel
    return
  endif
  wset, state.refpos_window
  catch,/cancel
  
  rp = get_reference_positions(state.rl[0:state.n_rl-1], state.n_rl, state.traces, state.n_traces)
  minp=min(rp)
  maxp=max(rp)
  x=(maxp-minp)/10.
  plot,[0,0],xrange=[0,n_elements(state.traces[*,0])-1],yrange=[minp-x,maxp+x], $
    xstyle=1,ystyle=1, $
    title=string(format='("Reference lines for band: ",A,"   detector: ",I1)',state.band,state.detector)
  for i = 0, state.n_rl-1 do $
    oplot,rp[*,i]

end

pro edit_reference_lines_update_line, base, line, line_idx
  COMPILE_OPT IDL2, HIDDEN
  widget_control, base, get_uvalue=state
  state.rl[line_idx] = line
  widget_control, base, set_uvalue=state
end