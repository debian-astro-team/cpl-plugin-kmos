pro plot_all_traces, band, detector
  COMPILE_OPT IDL2, HIDDEN

  reflines = load_reference_lines(band, detector)
  n_reflines = n_elements(reflines)
  if (n_reflines lt 4) then begin
    r=dialog_message('At least four reference lines are required',/error)
    return
  endif

  device,get_screen_size=screen_size
  xsize=screen_size[0]/2
  ysize=screen_size[1]/2
  fmt_string='("Plot of all traces for band: ",A," detector ",I)'
  base=widget_base(/tlb_size_events, xsize=xsize,ysize=ysize, $
    title=string(format=fmt_string,band,detector),/column)
  draw_wid=widget_draw(base,retain=2, xsize=xsize,ysize=ysize-100)
  
  button_base = widget_base(base,/row,/BASE_ALIGN_CENTER )
;  b1 = widget_button(button_base,value='Play backward',uvalue='PB')
  b2 = widget_button(button_base,value='Step backward',uvalue='SB')
;  b3 = widget_button(button_base,value='Stop',uvalue='STOP')
  b4 = widget_button(button_base,value='Step forward',uvalue='SF')
;  b5 = widget_button(button_base,value='Play forward',uvalue='PF')

  geom=widget_info(button_base,/geom)
  button_base_height=geom.ysize
  widget_control, draw_wid, xsize=xsize,ysize=ysize-button_base_height
  
  widget_control,base,/realize
  widget_control, draw_wid,get_value=draw_idx
  wset,draw_idx
  
  ll_table = read_line_list(band)
  
  traces = read_traces(band, detector)
  size = size(traces)
  n_traces=size[1]
  rp = get_reference_positions(reflines, n_reflines, traces, n_traces)
  lpos = get_line_positions(reflines, n_reflines, rp, traces, n_traces, ll_table)

  rl_nr=0
  timer_inc=0
  timer_delay=0.2
  state = {base:base, draw_wid:draw_wid, bbh:button_base_height, $
    timer_delay:timer_delay, timer_inc:timer_inc, $
    band:band, detector:detector, rl_nr:rl_nr, $
    traces:traces, n_traces:n_traces, $
    reflines:reflines, n_reflines:n_reflines, $
    rp:rp, lpos:lpos}
  widget_control, base, set_uvalue=state
  update_all_traces_plot,state
  
  xmanager,'plot_all_traces',base
end

pro plot_all_traces_event, ev
  COMPILE_OPT IDL2, HIDDEN
  common KMO_WAVE_DATA
  
  widget_control, ev.top, get_uvalue=state
  evname = tag_names(ev,/struct)
  if (evName eq "WIDGET_BASE") then begin
    widget_control, ev.id, xsize=ev.x,ysize=ev.y
    widget_control, state.draw_wid, xsize=ev.x,ysize=ev.y-state.bbh
    widget_control, state.draw_wid, get_value=draw_idx
    widget_control, ev.id, /real
  endif else if (evName eq "WIDGET_BUTTON") then begin
      widget_control,ev.id,get_uvalue=uval
      case uval of
        'SF' : state.rl_nr++
        'SB' : state.rl_nr--
        'PF' : state.timer_inc=1
        'PB' : state.timer_inc=-1
        'STOP': state.timer_inc=0
      endcase
      if (state.timer_inc ne 0) then widget_control, ev.top, timer=state.timer_delay
  endif else if (evName eq "WIDGET_TIMER") then begin
    if (state.timer_inc ne 0) then begin
      state.rl_nr += state.timer_inc
      if (state.timer_inc ne 0) then widget_control, ev.top, timer=state.timer_delay
    endif    
  endif
  rl_nr_max=state.n_reflines
  if (state.rl_nr lt 0) then begin
    state.rl_nr=0
    state.timer_inc=0
  endif
  if (state.rl_nr ge rl_nr_max) then begin
    state.rl_nr=rl_nr_max-1
    state.timer_inc=0
  endif
  widget_control, ev.top, set_uvalue=state
  update_all_traces_plot, state
end

pro update_all_traces_plot, state
  COMPILE_OPT IDL2, HIDDEN
  common KMO_WAVE_DATA
  
  fmt_string='("Plot of all traces for band ",A,", detector ",I1, ", reference line ",I1)'
  widget_control,state.base,BASE_SET_TITLE=string(format=fmt_string,$
    state.band,state.detector,state.rl_nr)

  widget_control, state.draw_wid,get_value=draw_idx
  wset,draw_idx
  
  !p.multi=[0,14,8,0,0]
  
  traces=state.traces
  rl_nr=state.rl_nr
  
  for i=0, 111 do begin
    if (state.reflines[rl_nr].ref eq -1) then begin
      s=state.reflines[rl_nr].offset
      e=s + state.reflines[rl_nr].range
    endif else begin
      s=state.rp[i,state.reflines[rl_nr].ref] + state.reflines[rl_nr].offset
       e=s + state.reflines[rl_nr].range     
    endelse
    plot,traces[i,s:e],title=string(i)
  endfor
  
 !p.multi=0
end
