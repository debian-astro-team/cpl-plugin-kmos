function get_selected_detector, base
  COMPILE_OPT IDL2, HIDDEN
  dlname = widget_info(base, find_by_uname='DETS')
  widget_control, dlname, get_value=list
  index = widget_info(dlname,/DROPLIST_SELECT)
  return,fix(list[index])
end

function get_selected_band, base
  COMPILE_OPT IDL2, HIDDEN
  dlname = widget_info(base, find_by_uname='BANDS')
  widget_control, dlname, get_value=list
  index = widget_info(dlname,/DROPLIST_SELECT)
  return,strtrim(list[index],2)
end

pro line_edit_menu, base, fitsTableFilename
  COMPILE_OPT IDL2, HIDDEN
;  common KMO_WAVE
  bands = ['H', 'K' , 'HK', 'IZ', 'YJ']
  dets = [1, 2, 3]
  
  base = widget_base(column=1,title='line_edit_menu')

  b_open=widget_button(base,value='Open table',uvalue='OPEN_TABLE')
  base_band = widget_base(base,/row,frame=1)
  tmp = widget_label(base_band,value='    band:')
  dl_band=widget_droplist(base_band,value=bands,uvalue=bands,uname='BANDS')
  widget_control,dl_band,SET_DROPLIST_SELECT=0
      
  base_det = widget_base(base,/row,frame=1)
  tmp = widget_label(base_det,value='detector:')
  dl_det=widget_droplist(base_det,value=string(format='(I1)',dets),uvalue=dets,uname='DETS')
  widget_control, dl_det, SET_DROPLIST_SELECT=0

  b_edit=widget_button(base,value='Edit reference lines',uvalue='EDIT')
  b_plot_pos=widget_button(base,value='Plot line positions',uvalue='PLOT_POS')
  b_plot_trace=widget_button(base,value='Plot single trace',uvalue='PLOT_TRACE')
  b_plot_all_traces=widget_button(base,value='Plot all traces',uvalue='PLOT_ALL_TRACES')
  b_plot_ll=widget_button(base,value='Plot line list',uvalue='PLOT_LL')
;  b_plot_fit=widget_button(base,value='Plot line fits',uvalue='PLOT_FIT')
;  b_idl=widget_button(base,value='Save to IDL',uvalue='SAVE_IDL')
;  b_c=widget_button(base,value='Save to C',uvalue='SAVE_C')
  b_fits=widget_button(base,value='Save to Fits',uvalue='SAVE_FITS')
  b_done=widget_button(base,value='Done',uvalue='DONE')
  widget_control,base,/realize
  plotpos_window = 0
end

pro line_edit_menu_event, ev
  COMPILE_OPT IDL2, HIDDEN
  common KMO_WAVE_DATA
;  help,ev,/struct
  evName=tag_names(ev,/struct)
  if (evName eq "WIDGET_BUTTON") then begin
    widget_control,ev.id,get_uvalue=uval
    case uval of
      'OPEN_TABLE': open_ref_table
      'EDIT' : begin
          tmp=read_traces(get_selected_band(ev.top),1)
          if (n_elements(tmp) ne 1) then begin
            edit_reference_lines, get_selected_band(ev.top), get_selected_detector(ev.top)
          endif else begin
            msg="no trace files found for the band " + get_selected_band(ev.top)
            r=dialog_message(msg,/error,title="User input error")
          end
        end
      'DONE' : begin
          if table_changed then begin
            question = 'Table of reference lines has been changed but is not saved yet' +$
              string(10b) + 'Do you want to save it now?'
            user = dialog_message(question,/question)
            if (user eq "Yes") then save2fits
          endif
          widget_control, ev.top, /destroy
        end
      'SAVE_IDL' : save2idl
      'SAVE_C' : save2c
      'SAVE_FITS': save2fits
      'PLOT_POS' : begin
          tmp=read_traces(get_selected_band(ev.top),1)
          if (n_elements(tmp) ne 1) then begin
            plot_positions, get_selected_band(ev.top), get_selected_detector(ev.top)
          endif else begin
            msg="no trace files found for the band " + get_selected_band(ev.top)
            r=dialog_message(msg,/error,title="User input error")
          end
        end          
      'PLOT_TRACE' : begin
          tmp=read_traces(get_selected_band(ev.top),1)
          if (n_elements(tmp) ne 1) then begin
            plot_trace, get_selected_band(ev.top), get_selected_detector(ev.top)
          endif else begin
            msg="no trace files found for the band " + get_selected_band(ev.top)
            r=dialog_message(msg,/error,title="User input error")
          end
        end         
      'PLOT_ALL_TRACES' : begin
          tmp=read_traces(get_selected_band(ev.top),1)
          if (n_elements(tmp) ne 1) then begin
            plot_all_traces, get_selected_band(ev.top), get_selected_detector(ev.top)
          endif else begin
            msg="no trace files found for the band " + get_selected_band(ev.top)
            r=dialog_message(msg,/error,title="User input error")
          end
        end          
      'PLOT_LL': plot_linelist, get_selected_band(ev.top)
      'PLOT_FIT' : ;plot_fits
    endcase
  endif else if (evName eq "WIDGET_DROPLIST") then begin
;    band = get_selected_band(ev.top)
;    detector = get_selected_detector(ev.top)
;    print,'droplist select: ', band, detector
;    reference_lines = load_reference_lines(band, detector)
;    line_list = read_line_list(band)
  endif
end

function get_line_positions, ref_lines, n_ref_lines, ref_pos, traces, n_traces, line_list
  COMPILE_OPT IDL2, HIDDEN
;  common KMO_WAVE
 
  ll = line_list.wavelength
  n_ll = n_elements(ll)
  degree=3
  coeffs=dindgen(n_traces,degree+1)
  fp=dindgen(n_traces,n_ll)
  sx=sort(ref_lines.lambda)
  x = ref_lines[sx].lambda
  for i = 0,n_traces-1 do begin
    y = ref_pos[i,sx]
    fin=where(finite(y))
    coeffs[i,*] = poly_fit(x[fin],y[fin],degree)
    fp[i,*] = poly(ll,coeffs[i,*])
  end
  return, fp
end

function get_reference_positions, ref_lines, n_ref_lines, traces, n_traces
  COMPILE_OPT IDL2, HIDDEN
;  common KMO_WAVE

  if (n_ref_lines eq 0) then return, dindgen(n_traces,1)
  
  ; sort index with absolute positions first
  sx1=where(ref_lines[*].ref eq -1, cnt)
  if (cnt eq 0) then begin
    message, "There must be reference line with an absolute position"
    return, dindgen(n_traces,1)
  endif
  sx2=where(ref_lines[*].ref ne -1,cnt)
  if (cnt eq 0) then sx=sx1 else sx=[sx1,sx2]
  
  ref_pos=dindgen(n_traces,n_ref_lines)
  for i = 0,n_traces-1 do begin
    for k = 0,n_ref_lines-1 do begin
      s=sx[k]
      if (ref_lines[s].ref eq -1) then offset=ref_lines[s].offset $
         else offset=round(ref_pos[i,ref_lines[s].ref]) + ref_lines[s].offset
      range=indgen(ref_lines[s].range)+offset
      t=where(traces[i,range] gt ref_lines[s].cut,cnt)
      if (cnt gt 1) then ref_pos[i,s] = range[0] + (t[cnt-1]+t[0])/2. $
        else if (cnt eq 1) then ref_pos[i,s] = range[0] + t[0] $
        else ref_pos[i,s] = !VALUES.F_NAN
;    tmp=gaussfit(range,traces[i,range],gp,nterm=4)
;    ref_pos[i,s] = gp[1]
;   gw[i+n_traces*0] = gp[2]
    endfor
  endfor  
  return, ref_pos
end

