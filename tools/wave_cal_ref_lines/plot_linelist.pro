pro plot_linelist, band
  COMPILE_OPT IDL2, HIDDEN

  device,get_screen_size=screen_size
  base=widget_base(/tlb_size_events, xsize=screen_size[0],ysize=400, $
    title=string(format='("line list for band: ",A)',band))
  draw_wid=widget_draw(base,retain=2,/button_events)
  widget_control,base,/realize
  widget_control, draw_wid,get_value=draw_idx
  wset,draw_idx
  
  ll = read_line_list(band)
             
  lx = where(strupcase(strmid(ll.gas,0,2)) eq 'AR',cnt)
  if cnt ne 0 then begin
    lp_ar=line_list_sharper(ll[lx].wavelength,ll[lx].strength)
  endif else begin
    lp_ar=fltarr(1,1)
  endelse
  lx = where(strupcase(strmid(ll.gas,0,2)) eq 'NE',cnt)
  if cnt ne 0 then begin
    lp_ne=line_list_sharper(ll[lx].wavelength,ll[lx].strength)
  endif else begin
    lp_ne=fltarr(1,1)
  endelse
  
  xrange = [min(ll.wavelength), max(ll.wavelength)]
  dx=(xrange[1]-xrange[0]) * 0.03
  xrange[0] -= dx
  xrange[1] += dx
  yrange = [-5,   max(ll.strength)]
 
  state = {draw_wid:draw_wid, xrange:xrange, yrange:yrange, $
    lp_ar:lp_ar, lp_ne:lp_ne, ll:ll}
  widget_control, base, set_uvalue=state
  update_linelist_plot,state
  
  xmanager,'plot_linelist',base
end

pro plot_linelist_event, ev
  COMPILE_OPT IDL2, HIDDEN
  common KMO_WAVE_DATA
  
  widget_control, ev.top, get_uvalue=state
  evname = tag_names(ev,/struct)
  if (evName eq "WIDGET_BASE") then begin
    widget_control, ev.id, xsize=ev.x,ysize=ev.y
    widget_control, state.draw_wid, xsize=ev.x,ysize=ev.y
    widget_control, state.draw_wid, get_value=draw_idx
    widget_control, ev.id, /real
    wset,draw_idx
    update_linelist_plot, state
  endif else if (evName eq "WIDGET_DRAW") then begin
    if ev.type eq 0 then begin
      data_coord = convert_coord(ev.x,ev.y,/device,/to_data)
      tmp=min(abs(state.ll.wavelength-data_coord[0]), min_ix)
      selected_lambda = state.ll[min_ix].wavelength
      update_linelist_plot, state
    endif
  endif
end

pro update_linelist_plot, state
  COMPILE_OPT IDL2, HIDDEN
  common KMO_WAVE_DATA
  
  widget_control, state.draw_wid,get_value=draw_idx
  wset,draw_idx
  
  plot,[0,0], xrange=state.xrange, yrange=state.yrange, xstyle=1, ystyle=1
  oplot,state.lp_ar[0,*],state.lp_ar[1,*],color='ff'x
  oplot,state.lp_ne[0,*],state.lp_ne[1,*],color='ff00'x
  if (selected_lambda ge state.xrange[0]) and (selected_lambda le state.xrange[1]) then $
    oplot,[selected_lambda,selected_lambda],[0,-5],color='ffff'x
end

function line_list_sharper, lambda, strength, offset=offset
  COMPILE_OPT IDL2, HIDDEN
  
  if (n_elements(lambda) ne n_elements(strength)) then begin
    print,"number of elements for both input parameters must match:",n_elements(lambda),n_elements(strength)
    return, findgen(2,1)
  endif
  
  if keyword_set(offset) then strength_offset = offset else strength_offset = 0.0
  ll= findgen(2,3*n_elements(lambda))
  
  delta=0.000001
  for i=0,n_elements(lambda)-1 do begin
    ix=i*3
    ll[0,ix+0]=lambda[i] - delta
    ll[0,ix+1]=lambda[i]
    ll[0,ix+2]=lambda[i] + delta
    ll[1,ix+0]=strength_offset
    ll[1,ix+1]=strength[i]
    ll[1,ix+2]=strength_offset
  end
  
  return, ll
end
