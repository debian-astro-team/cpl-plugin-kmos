pro edit_single_line, call_base, rl_ix, parent_state
  COMPILE_OPT IDL2, HIDDEN

  srl = parent_state.rl[rl_ix]
  base = widget_base(title='Edit single reference line',/column,/tlb_size_events)
  
  draw_wid = widget_draw(base,/button_events,/keyboard_events,/wheel_events,/motion_events)
  widget_control, draw_wid, get_value=draw_wid_idx

  base_wid = widget_base(base,/column,xsize=500)
  base2e = widget_base(base_wid,/row)
  baseb = widget_base(base_wid,/row)
  b_save = widget_button(baseb,value='Save',uvalue='SAVE')
  b_quit = widget_button(baseb,value='Quit',uvalue='QUIT')
  base3e = widget_base(base2e,/column)
  base4e = widget_base(base2e,/column)
  t_lambda = widget_text(base4e, /KBRD_FOCUS_EVENTS , /EDITABLE, value=string(srl.lambda), uname='T_LAMBDA', /all_events)
  t_ref =    widget_text(base4e, /KBRD_FOCUS_EVENTS , /EDITABLE, value=string(srl.ref), uname='T_REF')
  t_offset = widget_text(base4e, /KBRD_FOCUS_EVENTS , /EDITABLE, value=string(srl.offset), uname='T_OFFSET')
  t_range =  widget_text(base4e, /KBRD_FOCUS_EVENTS , /EDITABLE, value=string(srl.range), uname='T_RANGE')
  t_cut =    widget_text(base4e, /KBRD_FOCUS_EVENTS , /EDITABLE, value=string(srl.cut), uname='T_CUT')
  geom = widget_info(t_lambda,/geom)
;  help,geom,/struct
  dummy = widget_label(base3e,value='lambda',ysize=geom.scr_ysize)
  dummy = widget_label(base3e,value='reference line',ysize=geom.scr_ysize)
  dummy = widget_label(base3e,value='offset',ysize=geom.scr_ysize)
  dummy = widget_label(base3e,value='range',ysize=geom.scr_ysize)
  dummy = widget_label(base3e,value='cut',ysize=geom.scr_ysize)
 
  basegeom = widget_Info(base, /Geometry)   
  widget_control,draw_wid,xsize=basegeom.xsize,ysize=200
  widget_control,base,/realize
  
  state = {call_base:call_base, rl_ix:rl_ix, srl:srl, pstate:parent_state, $
    draw_wid:draw_wid, base_wid:base_wid, $
    track:0, start_x:0., start_y:0., start_offset:0, start_cut:0, $
    pstart:0, pend:0, ymin:0L, ymax:0L, left_margin:0, right_margin:0 }
  
  edit_single_line_plot, state, /first
  widget_control, base, set_uvalue=state
  
  xmanager,'edit_single_line',base
end

pro edit_single_line_event, ev
  COMPILE_OPT IDL2, HIDDEN
  common KMO_WAVE_DATA

  update_plot = 1
  widget_control, ev.top, get_uvalue=state
  evName=tag_names(ev,/struct)
;  print,">",tag_names(ev,/struct),"<"
  if (evName eq "WIDGET_BASE") then begin
    widget_control,ev.top,xsize=ev.x,ysize=ev.y
    base_wid_geom = widget_Info(state.base_wid, /Geometry)   
    widget_control,state.draw_wid,xsize=ev.x,ysize=ev.y-base_wid_geom.ysize,/real
  endif else if (evName eq "WIDGET_BUTTON") then begin
      widget_control,ev.id,get_uvalue=uval
      if (uval eq "QUIT") then begin
        widget_control, ev.top, /destroy
        return
      endif else if (uval eq "SAVE") then begin      
        state.pstate.rl[state.rl_ix] = state.srl
        edit_reference_lines_update_line, state.call_base, state.srl, state.rl_ix
        edit_reference_lines_plot, state.pstate
        widget_control, ev.top, /destroy
        return
      endif
  endif else if (strmatch(evName,"WIDGET_TEXT*") or evname eq "WIDGET_KBRD_FOCUS") then begin
    uname = widget_info(ev.id,/uname)
    if (evName eq "WIDGET_TEXT_CH" and uname eq 'T_LAMBDA') then $
      if ev.ch eq 22 then widget_control,ev.id,set_value=string(selected_lambda)
    on_ioerror, BAD
    widget_control,ev.id,get_value=input
    if (uname eq 'T_LAMBDA') then value=double(input) else value=long(input)
    widget_control,ev.id,set_value=string(value)
    case uname of
      'T_LAMBDA' : state.srl.lambda = value
      'T_REF' :    state.srl.ref = value
      'T_OFFSET' : state.srl.offset = value
      'T_RANGE' :  state.srl.range = value
      'T_CUT' :    state.srl.cut = value
    endcase
    BAD: on_ioerror, NULL
     case uname of
      'T_LAMBDA' : widget_control,ev.id,set_value=string(state.srl.lambda)
      'T_REF' :    widget_control,ev.id,set_value=string(state.srl.ref)
      'T_OFFSET' : widget_control,ev.id,set_value=string(state.srl.offset)
      'T_RANGE' :  widget_control,ev.id,set_value=string(state.srl.range)
      'T_CUT' :    widget_control,ev.id,set_value=string(state.srl.cut)
    endcase
  endif else if (evName eq "WIDGET_DRAW") then begin
    widget_control, state.draw_wid, get_value=draw_wid_idx
    wset,draw_wid_idx
     case ev.type of
      0: begin     ;button press
        plot,[0,0],xrange=[state.pstart,state.pend],yrange=[state.ymin,state.ymax],xstyle=1,ystyle=1
        data_coord = convert_coord(ev.x,ev.y,/device,/to_data)
        state.start_x = data_coord[0]
        state.start_y = data_coord[1]
        state.start_offset = state.srl.offset
        state.start_cut = state.srl.cut
        state.track = 1
      end
      1: begin     ;button release
        state.track = 0
        plot,[0,0],xrange=[state.pstart,state.pend],yrange=[state.ymin,state.ymax],xstyle=1,ystyle=1
        data_coord = convert_coord(ev.x,ev.y,/device,/to_data)
        delta_x = data_coord[0] - state.start_x
        delta_y = data_coord[1] - state.start_y
        state.srl.offset = fix(state.start_offset + delta_x)
        state.srl.cut = fix(state.start_cut + delta_y)
        widget_control, widget_info(ev.top,find_by_uname='T_OFFSET'), $
            set_value=string(state.srl.offset)
        widget_control, widget_info(ev.top,find_by_uname='T_CUT'), $
            set_value=string(state.srl.cut)
      end
      2: begin     ;motion events
        if state.track then begin
          plot,[0,0],xrange=[state.pstart,state.pend],yrange=[state.ymin,state.ymax],xstyle=1,ystyle=1
          data_coord = convert_coord(ev.x,ev.y,/device,/to_data)
          delta_x = data_coord[0] - state.start_x
          delta_y = data_coord[1] - state.start_y
          state.srl.offset = fix(state.start_offset + delta_x)
          state.srl.cut = fix(state.start_cut + delta_y)
;        widget_control, widget_info(ev.top,find_by_uname='T_OFFSET'), $
;            set_value=string(state.srl.offset)
;        widget_control, widget_info(ev.top,find_by_uname='T_CUT'), $
;            set_value=string(state.srl.cut)
        endif else begin
          update_plot = 0
        endelse
      end
      7: begin     ;wheel events
        if ((ev.modifiers and 2) ne 0) then begin    ;CTLR key pressed
          if (ev.clicks gt 0) then inc = 5 else inc = -5
          if (state.srl.range gt 10 or inc gt 0) then begin
            state.srl.offset = state.srl.offset - inc
            state.srl.range = state.srl.range + (2 * inc)
            widget_control, widget_info(ev.top,find_by_uname='T_OFFSET'), $
              set_value=string(state.srl.offset)
            widget_control, widget_info(ev.top,find_by_uname='T_RANGE'), $
              set_value=string(state.srl.range)
          endif
        endif else if ((ev.modifiers and 1) ne 0) then begin ; SHIFT key pressed
          if (ev.clicks gt 0) then inc = 10 else inc = -10
          state.left_margin = max([0,state.left_margin + inc])
          state.right_margin = max([0,state.right_margin + inc])
        endif else begin
          update_plot = 0
        endelse
      end
      else:    ;help,/str,ev
    endcase
  endif
  if update_plot then edit_single_line_plot, state
  widget_control, ev.top, set_uvalue=state
end

pro edit_single_line_plot, state, first=first
  COMPILE_OPT IDL2, HIDDEN

;  common edit_reference_lines_common_ps, left_margin, right_margin
   
  widget_control, state.draw_wid, get_value=draw_wid_idx
  wset,draw_wid_idx
  rp = get_reference_positions(state.pstate.rl, state.pstate.n_rl, $
                              state.pstate.traces, state.pstate.n_traces)
  
  if (keyword_set(first)) then begin
    state.left_margin = 50
    state.right_margin = 50
  endif
  
  trace_len = n_elements(state.pstate.traces[0,*])
  offset = state.srl.offset
  range = state.srl.range
  cut = state.srl.cut
  
  if not state.track then begin
    if (state.srl.ref eq -1) then begin
      state.pstart = offset - state.left_margin
      state.pend =  offset + range + state.right_margin
      if (state.pstart lt 0) then state.pstart = 0
      if (state.pend ge trace_len) then state.pend = trace_len - 1 
      state.ymin = min([cut,min(state.pstate.traces[*,state.pstart:state.pend])])
      state.ymax = max([cut,max(state.pstate.traces[*,state.pstart:state.pend])])
    endif else begin
      state.pstart = offset - state.left_margin
      state.pend = offset + range + state.right_margin
      ref= state.srl.ref
      state.ymin=cut
      state.ymax=cut
      for n = 0,state.pstate.n_traces-1 do begin
        pnstart = fix(rp[n,ref] + offset - state.left_margin)
        pnend = fix(rp[n,ref] + offset + range + state.right_margin)
        if (pnstart lt 0) then pnstart = 0
        if (pnstart ge trace_len) then pnstart = trace_len - 2
        if (pnend lt 1) then pnend = 1
        if (pnend ge trace_len) then pnend = trace_len - 1
        if (pnend lt pnstart) then pnend = pnstart
        state.ymin=min([state.ymin,min(state.pstate.traces[n,pnstart:pnend])])
        state.ymax=max([state.ymax,max(state.pstate.traces[n,pnstart:pnend])])
      endfor
    endelse
  endif

  plot,[0,0],xrange=[state.pstart,state.pend],yrange=[state.ymin,state.ymax],xstyle=1,ystyle=1
    
  for n = 0,state.pstate.n_traces-1 do begin
       xvec = indgen(state.pend-state.pstart+1)+state.pstart
    if (state.srl.ref eq -1) then begin
      oplot, xvec, state.pstate.traces[n,state.pstart:state.pend]
    endif else begin
      ref= state.srl.ref
      pnstart = fix(rp[n,ref] + offset - state.left_margin)
      pnend = fix(rp[n,ref] + offset + range +  state.right_margin)   
      if (pnstart lt 0) then begin
        xveca = xvec[-pnstart:*]
        pnstart = 0
      endif else xveca = xvec
      if (pnstart lt 0) then pnstart = 0
      if (pnstart ge trace_len) then pnstart = trace_len - 2
      if (pnend lt 1) then pnend = 1
      if (pnend ge trace_len) then pnend = trace_len - 1
      if (pnend lt pnstart) then pnend = pnstart
      oplot, xveca, state.pstate.traces[n,pnstart:pnend]
    endelse
  endfor
  oplot,[offset,offset + range],[cut,cut],color='ff'x
  widget_control, widget_info(state.draw_wid,/parent) ,/realize
end