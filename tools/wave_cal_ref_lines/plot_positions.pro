pro plot_positions_old, band, detector
  COMPILE_OPT IDL2, HIDDEN

  reference_lines = load_reference_lines(band, detector)
  n_reflines = n_elements(reference_lines)
  if (n_reflines lt 4) then begin
    r=dialog_message('At least four reference lines are required',/error)
    return
  endif
 
  window,/free
  plotpos_window = !D.WINDOW

  wset, plotpos_window
  catch,/cancel
   
  ll_table = read_line_list(band)
  
  traces = read_traces(band, detector)
  size = size(traces)
  n_traces=size[1]

  rp = get_reference_positions(reference_lines, n_reflines, traces, n_traces)
  plot,[0,0],xrange=[0,n_elements(traces[*,0])-1],yrange=[0,2048], $
    xstyle=1,ystyle=1,title=string(format='("band: ",A,"   detector: ",I1)',band,detector)
  for i = 0, n_reflines-1 do $
    oplot,rp[*,i],color='ff'x
    
  lpos = get_line_positions(reference_lines, n_reflines, rp, traces, n_traces, ll_table)
  for i = 0,n_elements(lpos[0,*])-1 do $
    oplot,lpos[*,i]
end

pro plot_positions, band, detector
  COMPILE_OPT IDL2, HIDDEN

  reference_lines = load_reference_lines(band, detector)
  n_reflines = n_elements(reference_lines)
  if (n_reflines lt 4) then begin
    r=dialog_message('At least four reference lines are required',/error)
    return
  endif

  device,get_screen_size=screen_size
  xsize=screen_size[0]/2
  ysize=screen_size[1]/2
  fmt_string='("Position of lines of the list for band: ",A," detector ",I1)'
  base=widget_base(/tlb_size_events, xsize=xsize,ysize=ysize, $
    title=string(format=fmt_string,band,detector))
  draw_wid=widget_draw(base,retain=2, xsize=xsize,ysize=ysize)
  widget_control,base,/realize
  widget_control, draw_wid,get_value=draw_idx
  wset,draw_idx
  
  ll_table = read_line_list(band)
  
  traces = read_traces(band, detector)
  size = size(traces)
  n_traces=size[1]
  rp = get_reference_positions(reference_lines, n_reflines, traces, n_traces)
  lpos = get_line_positions(reference_lines, n_reflines, rp, traces, n_traces, ll_table)

  state = {draw_wid:draw_wid, band:band, detector:detector, traces:traces, $
    n_reflines:n_reflines, rp:rp, lpos:lpos}
  widget_control, base, set_uvalue=state
  update_positions_plot,state
  
  xmanager,'plot_positions',base
end

pro plot_positions_event, ev
  COMPILE_OPT IDL2, HIDDEN
  common KMO_WAVE_DATA
  
  widget_control, ev.top, get_uvalue=state
  evname = tag_names(ev,/struct)
  if (evName eq "WIDGET_BASE") then begin
    widget_control, ev.id, xsize=ev.x,ysize=ev.y
    widget_control, state.draw_wid, xsize=ev.x,ysize=ev.y
    widget_control, state.draw_wid, get_value=draw_idx
    widget_control, ev.id, /real
    wset,draw_idx
    update_positions_plot, state
  endif
end

pro update_positions_plot, state
  COMPILE_OPT IDL2, HIDDEN
  common KMO_WAVE_DATA
  
  widget_control, state.draw_wid,get_value=draw_idx
  wset,draw_idx

  plot,[0,0],xrange=[0,n_elements(state.traces[*,0])-1],yrange=[0,2048], $
    xstyle=1,ystyle=1,title=string(format='("band: ",A,"   detector: ",I1)', $
    state.band, state.detector)
  for i = 0, state.n_reflines-1 do $
    oplot,state.rp[*,i],color='ff'x
  for i = 0,n_elements(state.lpos[0,*])-1 do $
    oplot,state.lpos[*,i]
end
