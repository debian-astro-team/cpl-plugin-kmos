function read_positions, band, detector
  COMPILE_OPT IDL2, HIDDEN
  common KMO_WAVE_DATA
  
  prefix= data_dir + data_prefix + '_' + band + '_ifu_'
  case detector of
    1: file_pattern = prefix + '{01,02,03,04,05,06,07,08}*'
    2: file_pattern = prefix + '{09,10,11,12,13,14,15,16}*'
    3: file_pattern = prefix + '{17,18,19,20,21,22,23,24}*'
  endcase
  files = file_search(file_pattern)
  if (n_elements(files) ne 1) then begin
    n_positions=n_elements(files)
    n_elem=n_elements(mrdfits(files[20],"find_positions",/silent)) > $
           n_elements(mrdfits(files[40],"find_positions",/silent)) > $
           n_elements(mrdfits(files[60],"find_positions",/silent)) > $
           n_elements(mrdfits(files[80],"find_positions",/silent))
    positions=dindgen(n_positions,n_elem)
    if (1) then begin
      for i = 0,n_positions-1 do begin
              tmp=mrdfits(files[i],"find_positions",/silent)
              if (n_elements(tmp) eq n_elem) then begin
                positions[i,*]=tmp
              endif else if (n_elements(tmp) lt n_elem) then begin
                print, $
                 format='("Not all lines detected in trace",I3,", file ",A," , found",I3," lines instead of",I3)',$
                 i, files[i],n_elements(tmp),n_elem
                positions[i,*]=[tmp,fltarr(n_elem-n_elements(tmp))]
              endif else begin
                print, $
                 format='("More lines detected in trace",I3,", file ",A," , found",I3," lines instead of",I3)',$
                 i, files[i],n_elements(tmp),n_elem
                positions[i,*]=tmp[0:nelem-1]
              endelse
      endfor
    endif
  endif else begin
    positions=0
  endelse
  
  return, positions
end


pro plot_trace, band, detector
  COMPILE_OPT IDL2, HIDDEN

  reflines = load_reference_lines(band, detector)
  n_reflines = n_elements(reflines)
  if (n_reflines lt 4) then begin
    r=dialog_message('At least four reference lines are required',/error)
    return
  endif

  device,get_screen_size=screen_size
  xsize=screen_size[0]/2
  ysize=screen_size[1]/2
  fmt_string='("Plot of a single trace for band: ",A," detector ",I)'
  base=widget_base(/tlb_size_events, xsize=xsize,ysize=ysize, $
    title=string(format=fmt_string,band,detector),/column)
  draw_wid=widget_draw(base,retain=2, xsize=xsize,ysize=ysize-100)
  
  button_base = widget_base(base,/row,/BASE_ALIGN_CENTER )
  b1 = widget_button(button_base,value='Play backward',uvalue='PB')
  b2 = widget_button(button_base,value='Step backward',uvalue='SB')
  b3 = widget_button(button_base,value='Stop',uvalue='STOP')
  b4 = widget_button(button_base,value='Step forward',uvalue='SF')
  b5 = widget_button(button_base,value='Play forward',uvalue='PF')

  geom=widget_info(button_base,/geom)
  button_base_height=geom.ysize
  widget_control, draw_wid, xsize=xsize,ysize=ysize-button_base_height
  
  widget_control,base,/realize
  widget_control, draw_wid,get_value=draw_idx
  wset,draw_idx
  
  ll_table = read_line_list(band)
  
  traces = read_traces(band, detector)
  size = size(traces)
  n_traces=size[1]
  rp = get_reference_positions(reflines, n_reflines, traces, n_traces)
  lpos = get_line_positions(reflines, n_reflines, rp, traces, n_traces, ll_table)

  positions = read_positions(band, detector)
  
  trace_nr=0
  timer_inc=0
  timer_delay=0.2
  state = {draw_wid:draw_wid, bbh:button_base_height, $
    timer_delay:timer_delay, timer_inc:timer_inc, $
    band:band, detector:detector, trace_nr:trace_nr, $
    traces:traces, n_traces:n_traces, positions:positions, $
    reflines:reflines, n_reflines:n_reflines, $
    rp:rp, lpos:lpos}
  widget_control, base, set_uvalue=state
  update_trace_plot,state
  
  xmanager,'plot_trace',base
end

pro plot_trace_event, ev
  COMPILE_OPT IDL2, HIDDEN
  common KMO_WAVE_DATA
  
  widget_control, ev.top, get_uvalue=state
  evname = tag_names(ev,/struct)
  if (evName eq "WIDGET_BASE") then begin
    widget_control, ev.id, xsize=ev.x,ysize=ev.y
    widget_control, state.draw_wid, xsize=ev.x,ysize=ev.y-state.bbh
    widget_control, state.draw_wid, get_value=draw_idx
    widget_control, ev.id, /real
  endif else if (evName eq "WIDGET_BUTTON") then begin
      widget_control,ev.id,get_uvalue=uval
      case uval of
        'SF' : state.trace_nr++
        'SB' : state.trace_nr--
        'PF' : state.timer_inc=1
        'PB' : state.timer_inc=-1
        'STOP': state.timer_inc=0
      endcase
      if (state.timer_inc ne 0) then widget_control, ev.top, timer=state.timer_delay
  endif else if (evName eq "WIDGET_TIMER") then begin
    if (state.timer_inc ne 0) then begin
      state.trace_nr += state.timer_inc
      if (state.timer_inc ne 0) then widget_control, ev.top, timer=state.timer_delay
    endif    
  endif
  trace_nr_max=n_elements(state.traces[*,0])
  if (state.trace_nr lt 0) then begin
    state.trace_nr=0
    state.timer_inc=0
  endif
  if (state.trace_nr ge trace_nr_max) then begin
    state.trace_nr=trace_nr_max-1
    state.timer_inc=0
  endif
  widget_control, ev.top, set_uvalue=state
  update_trace_plot, state
end

pro update_trace_plot, state
  COMPILE_OPT IDL2, HIDDEN
  common KMO_WAVE_DATA
  
  widget_control, state.draw_wid,get_value=draw_idx
  wset,draw_idx

  pos_line_length=-300
  fmt_str='("band: ",A,"   detector: ",I1,"   trace: ",I3)'
  traces=state.traces
  plot,traces[state.trace_nr,*], $
    xrange=[0,n_elements(traces[1,*])-1], $
    yrange=[min(traces)<pos_line_length,max(traces)], xstyle=1, ystyle=1, $
    title=string(format=fmt_str, state.band, state.detector, state.trace_nr)

  if (state.n_reflines gt 0) then begin
    for i = 0,state.n_reflines-1 do begin
      cut = state.reflines[i].cut
      if (state.reflines[i].ref eq -1) then begin
        xstart = state.reflines[i].offset
        xend = xstart + state.reflines[i].range
        rcol = 'ff'x
        oplot,[xstart,xend], [cut, cut], color=rcol
      endif else begin
        rcol = 'ff00'x
        xstart = state.rp[state.trace_nr,state.reflines[i].ref] + state.reflines[i].offset
        xend = xstart + state.reflines[i].range
        oplot,[xstart,xend], [cut, cut], color=rcol
      endelse
    endfor
  endif
  
  pos = state.positions[state.trace_nr,*]
  for i=0,n_elements(pos)-1 do begin
    oplot,[pos[i],pos[i]],[0,pos_line_length],color='ffff00'x
  endfor

end
