!PATH=expand_path('+/home/yjung/P_kmos/REFLINES/astron')+':'+!PATH
quiet=!QUIET
;!QUIET = 1
.compile kmo_wave_calib, utils, line_edit_menu, edit_reference_lines, edit_single_line, plot_linelist
.compile plot_all_traces, plot_positions, plot_trace, print_band
.compile kmo_wave_calib, utils, line_edit_menu, edit_reference_lines, edit_single_line, plot_linelist
.compile plot_all_traces, plot_positions, plot_trace, print_band
RESOLVE_ROUTINE, "STRSPLIT",/IS_FUNCTION
RESOLVE_ROUTINE, "XMANAGER"
!QUIET = quiet

kmo_wave_calib $
    ,cal_dir="/home/yjung/P_kmos/REFLINES/kmos-calib/" $
    ,data_dir="/home/yjung/P_kmos/REFLINES/ref_lines/" $
    ,prefix=".wave_cal_data" $
    ,table="/home/yjung/P_kmos/REFLINES/kmos-calib/kmos_wave_ref_table.fits"

exit
