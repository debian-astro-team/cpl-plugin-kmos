#!/bin/bash

#####################################################################
#   This is a script to create all static calibration files except
#       --- kmos_wave_ref_table.fits ---
#   This file has to be created interactively using the IDL tool
#   to be found in kmosp/tools/wave_cal_ref_lines --> main.pro
#####################################################################

# create ARC_LIST (F2L, XTENSION: BINTABLE)
./calib_creator.sh ARC_LIST ref/ar_ne_list_h.txt  H
./calib_creator.sh ARC_LIST ref/ar_ne_list_k.txt  K
./calib_creator.sh ARC_LIST ref/ar_ne_list_hk.txt HK
./calib_creator.sh ARC_LIST ref/ar_ne_list_iz.txt IZ
./calib_creator.sh ARC_LIST ref/ar_ne_list_yj.txt YJ

# create SPEC_TYPE_LOOKUP (F2L, XTENSION: BINTABLE)
./calib_creator.sh SPEC_TYPE_LOOKUP ref/spec_type.txt

# create WAVE_BAND (F2L, XTENSION: BINTABLE)
./calib_creator.sh WAVE_BAND ref/wave_band.txt

# create ATMOS_MODEL (F1S, XTENSION: IMAGE) 
./calib_creator.sh ATMOS_MODEL ref/atmos_h.fits  H
./calib_creator.sh ATMOS_MODEL ref/atmos_k.fits  K
./calib_creator.sh ATMOS_MODEL ref/atmos_hk.fits HK
./calib_creator.sh ATMOS_MODEL ref/atmos_iz.fits IZ
./calib_creator.sh ATMOS_MODEL ref/atmos_yj.fits YJ

# create SOLAR_SPEC (F1S, XTENSION: IMAGE) 
./calib_creator.sh SOLAR_SPEC ref/solar_h_2400.fits  H  2400
./calib_creator.sh SOLAR_SPEC ref/solar_k_1700.fits  K  1700
./calib_creator.sh SOLAR_SPEC ref/solar_hk_1100.fits HK 1100

# create OH_SPEC (F1S, XTENSION: IMAGE) 
./calib_creator.sh  OH_SPEC  ref/ohspec_H.fits   H
./calib_creator.sh  OH_SPEC  ref/ohspec_K.fits   K
./calib_creator.sh  OH_SPEC  ref/ohspec_HK.fits  HK
./calib_creator.sh  OH_SPEC  ref/ohspec_IZ.fits  IZ
./calib_creator.sh  OH_SPEC  ref/ohspec_YJ.fits  YJ
