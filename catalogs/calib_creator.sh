#!/bin/bash

# $0: script name
# $1: either ARC_LIST | ATMOS_MODEL | SOLAR_SPEC | SPEC_TYPE_LOOKUP | WAVE_BAND | OH_SPEC
# $2: input filename
# $3: filter id
# $4: temperature

if [[ $1 == "" || $1 == "-h" || $1 == "--h" || $1 == "-help" || $1 == "--help" ]]
then
   echo "--------------------------------------------------------------------------------"
   echo "    calib_creator:  A tool to create KMOS compliant static calibration files"
   echo ""
   echo "    Usage:  calib_creator <ESO PRO CATG> <input filepath>"
   echo ""
   echo "            - the allowed ESO PRO CATG keywords are: ARC_LIST, ATMOS_MODEL"
   echo "              SOLAR_SPEC, SPEC_TYPE_LOOKUP, WAVE_BAND, OH_SPEC"
   echo "            - input files can e.g. be taken from the ref directory"
   echo ""
   echo "            - For ARC_LIST, ATMOS_MODEL, SOLAR_SPEC and OH_SPEC the filter has"
   echo "              additionally to be defined: H, K, HK, IZ, YJ"
   echo ""
   echo "            - For SOLAR_SPEC the temperature in degrees has to be defined"
   echo "              (only for output filenmame)"
   echo ""
   echo "    Input files for the lists can be generated using dtfits:"
   echo "       dtfits -s ' ' kmos_ar_ne_list_h.fits"
   echo "--------------------------------------------------------------------------------"
   exit
fi

###############################################################################
# check input $1 (needed for esorex kmo_fits_stack --type)
###############################################################################
# check if 1st argument provided
: ${1?"Usage: $0 < ARC_LIST | ATMOS_MODEL | SOLAR_SPEC | SPEC_TYPE_LOOKUP | WAVE_BAND | OH_SPEC >"}

if [[ $1 != ARC_LIST && $1 != ATMOS_MODEL && $1 != SOLAR_SPEC && $1 != SPEC_TYPE_LOOKUP && $1 != WAVE_BAND  && $1 != OH_SPEC ]]
then
   echo "ERROR: Wrong type provided!"
   echo "ERROR: Usage: $0 < ARC_LIST | ATMOS_MODEL | SOLAR_SPEC | SPEC_TYPE_LOOKUP | WAVE_BAND | OH_SPEC >"
   echo "ERROR: Exiting...."
   exit
fi

###############################################################################
# check input $2 (needed for esorex kmo_fits_stack --input)
# split filename into prefix and suffix
###############################################################################
# check if 2nd argument provided
: ${2?"Usage: $0 $1 <FILENAME>"}

###############################################################################
# check input $3 (filter) for ARC_LIST, ATMOS_MODEL, SOLAR_SPEC
###############################################################################
if [[ $1 = ARC_LIST || $1 = ATMOS_MODEL || $1 = SOLAR_SPEC || $1 = OH_SPEC ]]
then
   # check if 3rd argument provided
   : ${3?"Usage: $0 $1 $2 < H | HK | K | IZ | YJ >"}

   # turn input uppercase and lowercase
   filtUp=$(echo $3 |tr "a-z" "A-Z")
   filtLo=$(echo $3 |tr "A-Z" "a-z")
   
   if [[ $filtUp != "H" && $filtUp != "HK" && $filtUp != "K" && $filtUp != "IZ" && $filtUp != "YJ" ]]
   then
      echo "ERROR: Wrong filter provided!"
      echo "ERROR: Usage: $0  $1 $2 < H | HK | K | IZ | YJ >"
      echo "ERROR: Exiting...."
      exit
   fi
fi

###############################################################################
# check input $4 (temperature) for SOLAR_SPEC
###############################################################################
if [[ $1 = SOLAR_SPEC ]]
then
   # check if 4th argument provided
   : ${4?"Usage: $0 $1 $2 $3 <int temperature>"}
fi

###############################################################################
# set appropriate parameters
###############################################################################
KMOSmainkey="ESO PRO CATG;string;$1"
if [ $1 = ARC_LIST ]
then
   KMOStype="F2L"
   KMOSfilename="kmos_ar_ne_list_$filtLo"
   KMOSformat="--format=%f;%f;%s"
   KMOStitle="--title=wavelength;strength;gas"
   KMOSfilt=";ESO FILT ID;string;$filtUp"
fi
if [ $1 = ATMOS_MODEL ]
then
   KMOStype="F1S"
   KMOSfilename="kmos_atmos_$filtLo"
   KMOSformat=""
   KMOStitle=""
   KMOSfilt=";ESO FILT ID;string;$filtUp"
fi
if [ $1 = SOLAR_SPEC ]
then
   KMOStype="F1S"
   KMOSfilename="kmos_solar_"$filtLo"_"$4
   KMOSformat=""
   KMOStitle=""
   KMOSfilt=";ESO FILT ID;string;$filtUp"
fi
if [ $1 = SPEC_TYPE_LOOKUP ]
then
   KMOStype="F2L"
   KMOSfilename="kmos_spec_type"
   KMOSformat="--format=%f;%f;%f;%f;%f"
   KMOStitle="--title=I;II;III;IV;V"
   KMOSfilt=""
fi
if [ $1 = WAVE_BAND ]
then
   KMOStype="F2L"
   KMOSfilename="kmos_wave_band"
   KMOSformat="--format=%f;%f;%f;%f;%f" 
   KMOStitle="--title=H;HK;IZ;K;YJ"
   KMOSfilt=""
fi
if [ $1 = OH_SPEC ]
then
   KMOStype="F1S"
   KMOSfilename="kmos_oh_spec_$filtLo"
   KMOSformat=""
   KMOStitle=""
   KMOSfilt=";ESO FILT ID;string;$filtUp"
fi

###############################################################################
# run esorex here 
###############################################################################
esorex kmo_fits_stack --type="$KMOStype" $KMOSformat $KMOStitle --filename="$KMOSfilename" --mainkey="INSTRUME;string;KMOS;ESO PRO CATG;string;$1$KMOSfilt"  --valid="none" --input="$2"
