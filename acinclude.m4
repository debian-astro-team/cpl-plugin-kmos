# KMOS_SET_PREFIX(PREFIX)
#---------------------------
AC_DEFUN([KMOS_SET_PREFIX],
[
    unset CDPATH
    # make $PIPE_HOME the default for the installation
    AC_PREFIX_DEFAULT($1)

    if test "x$prefix" = "xNONE"; then
        prefix=$ac_default_prefix
        ac_configure_args="$ac_configure_args --prefix $prefix"
    fi

    if test "x$exec_prefix" = "xNONE"; then
        exec_prefix=$prefix
    fi

])


# KMOS_SET_VERSION_INFO(VERSION, [CURRENT], [REVISION], [AGE])
#----------------------------------------------------------------
# Setup various version information, especially the libtool versioning
AC_DEFUN([KMOS_SET_VERSION_INFO],
[
    kmos_version=`echo "$1" | sed -e 's/[[a-z,A-Z]].*$//'`

    kmos_major_version=`echo "$kmos_version" | \
        sed 's/\([[0-9]]*\).\(.*\)/\1/'`
    kmos_minor_version=`echo "$kmos_version" | \
        sed 's/\([[0-9]]*\).\([[0-9]]*\)\(.*\)/\2/'`
    kmos_micro_version=`echo "$kmos_version" | \
        sed 's/\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\3/'`

    if test -z "$kmos_major_version"; then kmos_major_version=0
    fi

    if test -z "$kmos_minor_version"; then kmos_minor_version=0
    fi

    if test -z "$kmos_micro_version"; then kmos_micro_version=0
    fi

    KMOS_VERSION="$kmos_version"
    KMOS_MAJOR_VERSION=$kmos_major_version
    KMOS_MINOR_VERSION=$kmos_minor_version
    KMOS_MICRO_VERSION=$kmos_micro_version

    if test -z "$4"; then KMOS_INTERFACE_AGE=0
    else KMOS_INTERFACE_AGE="$4"
    fi

    KMOS_BINARY_AGE=`expr 100 '*' $KMOS_MINOR_VERSION + $KMOS_MICRO_VERSION`
    KMOS_BINARY_VERSION=`expr 10000 '*' $KMOS_MAJOR_VERSION + \
                          $KMOS_BINARY_AGE`

    AC_SUBST(KMOS_VERSION)
    AC_SUBST(KMOS_MAJOR_VERSION)
    AC_SUBST(KMOS_MINOR_VERSION)
    AC_SUBST(KMOS_MICRO_VERSION)
    AC_SUBST(KMOS_INTERFACE_AGE)
    AC_SUBST(KMOS_BINARY_VERSION)
    AC_SUBST(KMOS_BINARY_AGE)

    AC_DEFINE_UNQUOTED(KMOS_MAJOR_VERSION, $KMOS_MAJOR_VERSION,
                       [KMOS major version number])
    AC_DEFINE_UNQUOTED(KMOS_MINOR_VERSION, $KMOS_MINOR_VERSION,
                       [KMOS minor version number])
    AC_DEFINE_UNQUOTED(KMOS_MICRO_VERSION, $KMOS_MICRO_VERSION,
                       [KMOS micro version number])
    AC_DEFINE_UNQUOTED(KMOS_INTERFACE_AGE, $KMOS_INTERFACE_AGE,
                       [KMOS interface age])
    AC_DEFINE_UNQUOTED(KMOS_BINARY_VERSION, $KMOS_BINARY_VERSION,
                       [KMOS binary version number])
    AC_DEFINE_UNQUOTED(KMOS_BINARY_AGE, $KMOS_BINARY_AGE,
                       [KMOS binary age])

    ESO_SET_LIBRARY_VERSION([$2], [$3], [$4])
])

# KMOS_SET_PATHS
#------------------
# Define auxiliary directories of the installed directory tree.
AC_DEFUN([KMOS_SET_PATHS],
[
    if test -z "$plugindir"; then
        plugindir='${libdir}/esopipes-plugins/${PACKAGE}-${VERSION}'
    fi

    if test -z "$pipedocsdir"; then
        pipedocsdir='${datadir}/doc/esopipes/${PACKAGE}-${VERSION}/'
    fi

    if test -z "$apidocdir"; then
        apidocdir='${datadir}/doc/esopipes/${PACKAGE}-${VERSION}/html'
    fi

    if test -z "$privatelibdir"; then
        privatelibdir='${libdir}/${PACKAGE}-${VERSION}'
    fi

    if test -z "$configdir"; then
        configdir='${datadir}/esopipes/${PACKAGE}-${VERSION}/config'
    fi

    if test -z "$wkfextradir"; then
        wkfextradir='${datadir}/esopipes/${PACKAGE}-${VERSION}/reflex'
    fi

    if test -z "$wkfcopydir"; then
        wkfcopydir='${datadir}/reflex/workflows/${PACKAGE}-${VERSION}'
    fi


    AC_SUBST(plugindir)
    AC_SUBST(pipedocsdir)
    AC_SUBST(apidocdir)
    AC_SUBST(privatelibdir)
    AC_SUBST(configdir)
    AC_SUBST(wkfextradir)
    AC_SUBST(wkfcopydir)

    # Define a preprocesor symbol for the plugin search paths

    AC_DEFINE_UNQUOTED(KMOS_PLUGIN_DIR, "esopipes-plugins",
                       [Plugin directory tree prefix])

    eval plugin_dir="$plugindir"
    plugin_path=`eval echo $plugin_dir | \
                sed -e "s/\/${PACKAGE}-${VERSION}.*$//"`

    AC_DEFINE_UNQUOTED(KMOS_PLUGIN_PATH, "$plugin_path",
                       [Absolute path to the plugin directory tree])

])


# KMOS_CREATE_SYMBOLS
#-----------------------
# Define include and library related makefile symbols
AC_DEFUN([KMOS_CREATE_SYMBOLS],
[

    # Symbols for package include file and library search paths

    KMOS_INCLUDES='-I$(top_srcdir)/kmos -I$(top_srcdir)/irplib -I$(top_srcdir)/kmclipm/include'
    KMOS_LDFLAGS='-L$(top_srcdir)/kmos'

    all_includes='$(KMOS_INCLUDES) $(CPL_INCLUDES) $(EXTRA_INCLUDES)'
    all_ldflags='$(KMOS_LDFLAGS) $(CPL_LDFLAGS) $(EXTRA_LDFLAGS)'

    # Library aliases

    LIBKMOS='$(top_builddir)/kmos/libkmos.la'
    LIBIRPLIB='$(top_builddir)/irplib/libirplib.la'
    LIBKMCLIPMLIB='$(top_builddir)/kmclipm/libkmclipmlib.la'

    # Substitute the defined symbols

    AC_SUBST(KMOS_INCLUDES)
    AC_SUBST(KMOS_LDFLAGS)

    AC_SUBST(LIBKMOS)
    AC_SUBST(LIBIRPLIB)
    AC_SUBST(LIBKMCLIPMLIB)

    # Check for CPL and user defined libraries
    AC_REQUIRE([CPL_CHECK_LIBS])
    AC_REQUIRE([ESO_CHECK_EXTRA_LIBS])

    all_includes='$(KMOS_INCLUDES) $(CPL_INCLUDES) $(EXTRA_INCLUDES)'
    all_ldflags='$(KMOS_LDFLAGS) $(CPL_LDFLAGS) $(EXTRA_LDFLAGS)'

    AC_SUBST(all_includes)
    AC_SUBST(all_ldflags)
])
