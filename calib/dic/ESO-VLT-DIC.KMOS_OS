#******************************************************************************
# E.S.O. - VLT project
#
# "@(#) $Id: ESO-VLT-DIC.KMOS_OS,v 1.2 2012-01-31 13:46:39 yjung Exp $"
#
# KMOS_OS dictionary
#
# who       when       what
# --------  --------   --------------------------------------------------------
# alongino  01/09/00   created (copied from kmodic)
# mbernard  2006-10-09 keywords HIT modified
# mbernard  2006-11-17 small changes related to movement of transf. of
#                      coord from ICS to OS
# mbernard  2007-02-13 change from x/y to y/z
# mbernard  2007-03-XX GRFI, ARMi.POSY, ARMi.POSZ
# mbernard  2007-06-25 OCS.TARG.OFFSA, OCS.TARG.OFFSD, OCS.TARG.OFFSR,
#                      OCS.TARG.DITHA, OCS.TARG.DITHD
# mbernard  2007-07-19 added all INS keywords from Design Description
# mbernard  2007-11-20 added OCS.TARG.TELLUR and OCS.TARG.FLUXSTD
#                      (KMOS_spec_cal_stdstar)
# mbernard  2008-01-21 cleaned for review with DICB
# mbernard  2008-02-05 added OCS.ARM.OFFSY and OCS.ARM.OFFSZ
# mbernard  2008-02-21 OCS.ARM.OFFSY & OFFSZ -> OCS.ARMi.OFFSY & OFFSZ
# mbernard  2008-03-12 Small modifications in comments
# mbernard  2008-06-04 removed OCS.GRFI.NAME
# mwegner   2008-09-05 Keywords for additional sky during acquisition added.
# jschlichter 2008-10-27 Added keywords OCS.GRFI and OCS.GRFI.WLEN.XXX
#******************************************************************************
#
# This dictionary contains all keywords used by KMOS OS at runtime
# to execute and understand commands with -function option, such
# as SETUP and STATUS.
#
# IMPORTANT NOTE: The "Value Format" entry defines the precision of
#                 the keyword value, following the ANSI-C printf
#                 rules. Specially for float or double numbers,
#                 roundings are possible and affects all commands,
#                 in particular SETUP and STATUS.
#                 In order to avoid undesired roundings, the
#                 appropriate precision must be defined in this field.
#
#******************************************************************************

Dictionary Name:   ESO-VLT-DIC.KMOS_OS
Scope:             KMOS
Source:            ESO VLT
Version Control:   @(#) $Id: ESO-VLT-DIC.KMOS_OS,v 1.2 2012-01-31 13:46:39 yjung Exp $
Revision:          $Revision: 1.2 $
Date:              2012-01-25
Status:            Development
Description:       Template Instrument OS keywords

# Max. comment length ---------------------|
#                                          V
# -----------------------------------------+------------------

# General
# keywords in PAF file
# TPL.FILE.DIRNAME        %1024s  Path of the PAF file (s)
#
# 2006-11-17 for some reason the TPL dictionary is ignored by oslx for
# this reason all TPL keywords are located in OS (this) dictionary
Parameter Name:    TPL MODE OBS
Class:             header|setup
Context:           Instrument
Type:              string
Value Format:      %16s
Unit:              
Comment Format:    Specifies detailed observing mode
Description:       Specifies detailed observing mode

Parameter Name:    TPL MODE ACQ
Class:             header|setup
Context:           Instrument
Type:              string
Value Format:      %16s
Unit:              
Comment Format:    Specifies detailed acquisition mode
Description:       Specifies detailed acquisition mode

Parameter Name:    OCS SETUP TYPE
Class:             setup
Context:           Instrument
Type:              string
Value Format:      %16s
Unit:              
Comment Format:    Type of exposure within template
Description:       Type of exposure within template

# Telesc. alpha, delta coordinates for acquisition, science and sky exposures
Parameter Name:    OCS TARG ACQ ALPHA
Class:             setup
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              HHMMSS.TTT
Comment Format:    [HHMMSS.TTT] Telesc. alpha for acq. expo.
Description:       [HHMMSS.TTT] Telesc. alpha for acq. expo.

Parameter Name:    OCS TARG ACQ DELTA
Class:             setup
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              DDMMSS.TTT
Comment Format:    [DDMMSS.TTT] Telesc. delta for acq. expo.
Description:       [DDMMSS.TTT] Telesc. delta for acq. expo.

Parameter Name:    OCS ROT ACQ OFFANGLE
Class:             setup
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              +/-DDD.TTT
Comment Format:    [+/-DDD.TTT] Rotator offs.angle/acq.expo.
Description:       Rotator offset angle for acqusition exposure.

Parameter Name:    OCS TARG SKA ALPHA
Class:             setup
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              HHMMSS.TTT
Comment Format:    [HHMMSS.TTT] Tel. alpha for acq-sky expo.
Description:       [HHMMSS.TTT] Tel. alpha for acq-sky expo.

Parameter Name:    OCS TARG SKA DELTA
Class:             setup
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              DDMMSS.TTT
Comment Format:    [DDMMSS.TTT] Tel. delta for acq-sky expo.
Description:       [DDMMSS.TTT] Tel. delta for acq-sky expo.

Parameter Name:    OCS ROT SKA OFFANGLE
Class:             setup
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              +/-DDD.TTT
Comment Format:    [+/-DDD.TTT] Rotator offs.angle/sky.expo.
Description:       Rotator offset angle for sky exposure.

Parameter Name:    OCS TARG SCI ALPHA
Class:             setup
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              HHMMSS.TTT
Comment Format:    [HHMMSS.TTT] Telesc. alpha for sci. expo.
Description:       [HHMMSS.TTT] Telesc. alpha for sci. expo.

Parameter Name:    OCS TARG SCI DELTA
Class:             setup
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              DDMMSS.TTT
Comment Format:    [DDMMSS.TTT] Telesc. delta for sci. expo.
Description:       [DDMMSS.TTT] Telesc. delta for sci. expo.

Parameter Name:    OCS ROT SCI OFFANGLE
Class:             setup
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              +/-DDD.TTT
Comment Format:    [+/-DDD.TTT] Rotator offs.angle/sci.expo.
Description:       Rotator offset angle for science exposure.

Parameter Name:    OCS TARG SKY ALPHA
Class:             setup
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              HHMMSS.TTT
Comment Format:    [HHMMSS.TTT] Telesc. alpha for sky expo.
Description:       [HHMMSS.TTT] Telesc. alpha for sky expo.

Parameter Name:    OCS TARG SKY DELTA
Class:             setup
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              DDMMSS.TTT
Comment Format:    [DDMMSS.TTT] Telesc. delta for sky expo.
Description:       [DDMMSS.TTT] Telesc. delta for sky expo.

Parameter Name:    OCS ROT SKY OFFANGLE
Class:             setup
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              
Comment Format:    Rotator offs.angle/sky.expo.
Description:       Rotator offset angle for sky exposure.

Parameter Name:    OCS TARG ALPHA
Class:             header
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              HHMMSS.TTT
Comment Format:    [HHMMSS.TTT] Telesc. alpha coordinate
Description:       [HHMMSS.TTT] Telesc. alpha coordinate

Parameter Name:    OCS TARG DELTA
Class:             header
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              DDMMSS.TTT
Comment Format:    [DDMMSS.TTT] Telesc. delta coordinate
Description:       [DDMMSS.TTT] Telesc. delta coordinate

Parameter Name:    OCS ROT OFFANGLE
Class:             header
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              +/-DDD.TTT
Comment Format:    [+/-DDD.TTT] Rotator offset angle
Description:       [+/-DDD.TTT] Rotator offset angle

# Information belonging to arm i during acquisition exposure
Parameter Name:    OCS ARMi ACQ NAME
Class:             setup
Context:           Instrument
Type:              string
Value Format:      %16s
Unit:              
Comment Format:    Target name i/acq.
Description:       Target name i/acq.

Parameter Name:    OCS ARMi ACQ ALPHA
Class:             setup
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              HHMMSS.TTT
Comment Format:    [HHMMSS.TTT] Target alpha for arm i/acq.
Description:       [HHMMSS.TTT] Target alpha for arm i/acq.

Parameter Name:    OCS ARMi ACQ DELTA
Class:             setup
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              DDMMSS.TTT
Comment Format:    [DDMMSS.TTT] Target delta for arm i/acq.
Description:       [DDMMSS.TTT] Target delta for arm i/acq.

Parameter Name:    OCS ARMi ACQ Y
Class:             setup
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              mm
Comment Format:    [mm] Target y for arm i/acq.
Description:       [mm] Target y for arm i/acq.

Parameter Name:    OCS ARMi ACQ Z
Class:             setup
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              mm
Comment Format:    [mm] Target z for arm i/acq.
Description:       [mm] Target z for arm i/acq.

Parameter Name:    OCS ARMi ACQ R
Class:             setup
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              mm
Comment Format:    [mm] Target R for arm i/acq.
Description:       [mm] Target R for arm i/acq.

Parameter Name:    OCS ARMi ACQ THETA
Class:             setup
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              deg
Comment Format:    [deg] Target THETA for arm i/acq.
Description:       [deg] Target THETA for arm i/acq.

Parameter Name:    OCS ARMi ACQ PRIOR
Class:             setup
Context:           Instrument
Type:              integer
Value Format:      %d
Unit:              
Comment Format:    Priority for arm i/acq.
Description:       Priority for arm i/acq.

Parameter Name:    OCS ARMi ACQ TYPE
Class:             setup
Context:           Instrument
Type:              string
Value Format:      %1s
Unit:              
Comment Format:    Target type for arm i/acq.
Description:       Target type for arm i/acq.

Parameter Name:    OCS ARMi ACQ VIGNET
Class:             setup
Context:           Instrument
Type:              logical
Value Format:      %c
Unit:              
Comment Format:    Arm i vignetted / acq.
Description:       Arm i vignetted / acq.

Parameter Name:    OCS ARMi ACQ COMMENT
Class:             setup
Context:           Instrument
Type:              string
Value Format:      %45s
Unit:              
Comment Format:    Comment for arm i / acq.
Description:       Comment for arm i / acq.

Parameter Name:    OCS ARMi ACQ HIT
Class:             setup
Context:           Instrument
Type:              string
Value Format:      %32s
Unit:              
Comment Format:    Arm i is hit by bright object / acq
Description:       Arm i is hit by bright object / acq

# Information belonging to arm i during sky/acquisition exposure
Parameter Name:    OCS ARMi SKA NAME
Class:             setup
Context:           Instrument
Type:              string
Value Format:      %16s
Unit:              
Comment Format:    Target name i/acq-sky
Description:       Target name i/acq-sky

Parameter Name:    OCS ARMi SKA ALPHA
Class:             setup
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              HHMMSS.TTT
Comment Format:    [HHMMSS.TTT] Target alpha arm i/acq-sky
Description:       [HHMMSS.TTT] Target alpha arm i/acq-sky

Parameter Name:    OCS ARMi SKA DELTA
Class:             setup
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              DDMMSS.TTT
Comment Format:    [DDMMSS.TTT] Target delta arm i/acq-sky 
Description:       [DDMMSS.TTT] Target delta arm i/acq-sky 

Parameter Name:    OCS ARMi SKA Y
Class:             setup
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              mm
Comment Format:    [mm] Target y for arm i/acq-sky
Description:       [mm] Target y for arm i/acq-sky

Parameter Name:    OCS ARMi SKA Z
Class:             setup
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              mm
Comment Format:    [mm] Target z for arm i/acq-sky
Description:       [mm] Target z for arm i/acq-sky

Parameter Name:    OCS ARMi SKA R
Class:             setup
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              mm
Comment Format:    [mm] Target R for arm i/acq-sky
Description:       [mm] Target R for arm i/acq-sky

Parameter Name:    OCS ARMi SKA THETA
Class:             setup
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              deg
Comment Format:    [deg] Target THETA for arm i/acq-sky
Description:       [deg] Target THETA for arm i/acq-sky

Parameter Name:    OCS ARMi SKA PRIOR
Class:             setup
Context:           Instrument
Type:              integer
Value Format:      %d
Unit:              
Comment Format:    Priority for arm i/acq-sky
Description:       Priority for arm i/acq-sky

Parameter Name:    OCS ARMi SKA TYPE
Class:             setup
Context:           Instrument
Type:              string
Value Format:      %1s
Unit:              
Comment Format:    Target type for arm i/acq-sky
Description:       Target type for arm i/acq-sky

Parameter Name:    OCS ARMi SKA VIGNET
Class:             setup
Context:           Instrument
Type:              logical
Value Format:      %c
Unit:              
Comment Format:    Arm i vignetted /acq-sky
Description:       Arm i vignetted /acq-sky

Parameter Name:    OCS ARMi SKA COMMENT
Class:             setup
Context:           Instrument
Type:              string
Value Format:      %45s
Unit:              
Comment Format:    Comment for arm i/acq-sky
Description:       Comment for arm i/acq-sky

Parameter Name:    OCS ARMi SKA HIT
Class:             setup
Context:           Instrument
Type:              string
Value Format:      %32s
Unit:              
Comment Format:    Arm i is hit by bright object /acq-sky
Description:       Arm i is hit by bright object /acq-sky

# Information belonging to arm i during science exposure
Parameter Name:    OCS ARMi SCI NAME
Class:             setup
Context:           Instrument
Type:              string
Value Format:      %16s
Unit:              
Comment Format:    Target name i/sci.
Description:       Target name i/sci.

Parameter Name:    OCS ARMi SCI ALPHA
Class:             setup
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              HHMMSS.TTT
Comment Format:    [HHMMSS.TTT] Target alpha for arm i/sci.
Description:       [HHMMSS.TTT] Target alpha for arm i/sci.

Parameter Name:    OCS ARMi SCI DELTA
Class:             setup
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              DDMMSS.TTT
Comment Format:    [DDMMSS.TTT] Target delta for arm i/sci.
Description:       [DDMMSS.TTT] Target delta for arm i/sci.

Parameter Name:    OCS ARMi SCI Y
Class:             setup
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              mm
Comment Format:    [mm] Target y for arm i/sci.
Description:       [mm] Target y for arm i/sci.

Parameter Name:    OCS ARMi SCI Z
Class:             setup
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              mm
Comment Format:    [mm] Target z for arm i/sci.
Description:       [mm] Target z for arm i/sci.

Parameter Name:    OCS ARMi SCI R
Class:             setup
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              mm
Comment Format:    [mm] Target R for arm i/sci.
Description:       [mm] Target R for arm i/sci.

Parameter Name:    OCS ARMi SCI THETA
Class:             setup
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              deg
Comment Format:    [deg] Target THETA for arm i/sci
Description:       [deg] Target THETA for arm i/sci

Parameter Name:    OCS ARMi SCI PRIOR
Class:             setup
Context:           Instrument
Type:              integer
Value Format:      %d
Unit:              
Comment Format:    Priority for arm i/sci.
Description:       Priority for arm i/sci.

Parameter Name:    OCS ARMi SCI TYPE
Class:             setup
Context:           Instrument
Type:              string
Value Format:      %1s
Unit:              
Comment Format:    Target type for arm i/sci.
Description:       Target type for arm i/sci.

Parameter Name:    OCS ARMi SCI VIGNET
Class:             setup
Context:           Instrument
Type:              logical
Value Format:      %c
Unit:              
Comment Format:    Arm i vignetted / sci.
Description:       Arm i vignetted / sci.

Parameter Name:    OCS ARMi SCI COMMENT
Class:             setup
Context:           Instrument
Type:              string
Value Format:      %45s
Unit:              
Comment Format:    Comment for arm i / sci.
Description:       Comment for arm i / sci.

Parameter Name:    OCS ARMi SCI HIT
Class:             setup
Context:           Instrument
Type:              string
Value Format:      %32s
Unit:              
Comment Format:    Arm i is hit by bright object / sci.
Description:       Arm i is hit by bright object / sci.

# Information belonging to arm i during sky background exposure
Parameter Name:    OCS ARMi SKY NAME
Class:             setup
Context:           Instrument
Type:              string
Value Format:      %16s
Unit:              
Comment Format:    Target name i/sky
Description:       Target name i/sky

Parameter Name:    OCS ARMi SKY ALPHA
Class:             setup
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              HHMMSS.TTT
Comment Format:    [HHMMSS.TTT] Target alpha for arm i/sky
Description:       [HHMMSS.TTT] Target alpha for arm i/sky

Parameter Name:    OCS ARMi SKY DELTA
Class:             setup
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              DDMMSS.TTT
Comment Format:    [DDMMSS.TTT] Target delta for arm i/sky
Description:       [DDMMSS.TTT] Target delta for arm i/sky

Parameter Name:    OCS ARMi SKY Y
Class:             setup
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              mm
Comment Format:    [mm] Target y for arm i/sky
Description:       [mm] Target y for arm i/sky

Parameter Name:    OCS ARMi SKY Z
Class:             setup
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              mm
Comment Format:    [mm] Target z for arm i/sky
Description:       [mm] Target z for arm i/sky

Parameter Name:    OCS ARMi SKY R
Class:             setup
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              mm
Comment Format:    [mm] Target R for arm i/sky
Description:       [mm] Target R for arm i/sky

Parameter Name:    OCS ARMi SKY THETA
Class:             setup
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              deg
Comment Format:    [deg] Target THETA for arm i/sky
Description:       [deg] Target THETA for arm i/sky

Parameter Name:    OCS ARMi SKY PRIOR
Class:             setup
Context:           Instrument
Type:              integer
Value Format:      %d
Unit:              
Comment Format:    Priority for arm i/sky
Description:       Priority for arm i/sky

Parameter Name:    OCS ARMi SKY TYPE
Class:             setup
Context:           Instrument
Type:              string
Value Format:      %1s
Unit:              
Comment Format:    Target type for arm i/sky
Description:       Target type for arm i/sky

Parameter Name:    OCS ARMi SKY VIGNET
Class:             setup
Context:           Instrument
Type:              logical
Value Format:      %c
Unit:              
Comment Format:    Arm i vignetted /sky 
Description:       Arm i vignetted /sky 

Parameter Name:    OCS ARMi SKY COMMENT
Class:             setup
Context:           Instrument
Type:              string
Value Format:      %45s
Unit:              
Comment Format:    Comment for arm i/sky
Description:       Comment for arm i/sky

Parameter Name:    OCS ARMi SKY HIT
Class:             setup
Context:           Instrument
Type:              string
Value Format:      %32s
Unit:              
Comment Format:    Arm i is hit by bright object /sky
Description:       Arm i is hit by bright object /sky

Parameter Name:    OCS ARMi NAME
Class:             header
Context:           Instrument
Type:              string
Value Format:      %16s
Unit:              
Comment Format:    Target name hosted by arm i
Description:       Target name hosted by arm i

Parameter Name:    OCS ARMi ALPHA
Class:             header
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              HHMMSS.TTT
Comment Format:    [HHMMSS.TTT] Target alpha hosted by arm i
Description:       [HHMMSS.TTT] Target alpha hosted by arm i

Parameter Name:    OCS ARMi DELTA
Class:             header
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              DDMMSS.TTT
Comment Format:    [DDMMSS.TTT] Target delta hosted by arm i
Description:       [DDMMSS.TTT] Target delta hosted by arm i

Parameter Name:    OCS ARMi Y
Class:             header
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              mm
Comment Format:    [mm] y-pos of arm i
Description:       [mm] y-pos of arm i

Parameter Name:    OCS ARMi Z
Class:             header
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              mm
Comment Format:    [mm] z-pos of arm i
Description:       [mm] z-pos of arm i

Parameter Name:    OCS ARMi R
Class:             header
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              mm
Comment Format:    [mm] R-pos of arm i
Description:       [mm] R-pos of arm i

Parameter Name:    OCS ARMi THETA
Class:             header
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              deg
Comment Format:    [deg] THETA-pos of arm i
Description:       [deg] THETA-pos of arm i

Parameter Name:    OCS ARMi PRIOR
Class:             header
Context:           Instrument
Type:              integer
Value Format:      %d
Unit:              
Comment Format:    Target priority hosted by arm i
Description:       Target priority hosted by arm i

Parameter Name:    OCS ARMi TYPE
Class:             header
Context:           Instrument
Type:              string
Value Format:      %1s
Unit:              
Comment Format:    Target type hosted by arm i
Description:       Target type hosted by arm i

Parameter Name:    OCS ARMi VIGNET
Class:             header
Context:           Instrument
Type:              logical
Value Format:      %c
Unit:              
Comment Format:    If T - arm i is vignetted
Description:       If T - arm i is vignetted

Parameter Name:    OCS ARMi COMMENT
Class:             header
Context:           Instrument
Type:              string
Value Format:      %45s
Unit:              
Comment Format:    Comment for target hosted by arm i
Description:       Comment for target hosted by arm i

Parameter Name:    OCS ARMi HIT
Class:             header
Context:           Instrument
Type:              string
Value Format:      %32s
Unit:              
Comment Format:    Arm i is hit by listed bright object(s)
Description:       Arm i is hit by listed bright object(s)

Parameter Name:    OCS ARMi ORIGARM
Class:             header
Context:           Instrument
Type:              integer
Value Format:      %d
Unit:              
Comment Format:    Original arm number before rotator optimization
Description:       Original arm number before rotator optimization

Parameter Name:    OCS ARMi NOTUSED
Class:             header
Context:           Instrument
Type:              string
Value Format:      %45s
Unit:              
Comment Format:    Reason why arm was not used
Description:       Reason why arm was not used

Parameter Name:    OCS OSS VER
Class:             header|setup
Context:           Instrument
Type:              string
Value Format:      %16s
Unit:              
Comment Format:    OSS version number
Description:       OSS version number

Parameter Name:    OCS OSS REVISION
Class:             header|setup
Context:           Instrument
Type:              string
Value Format:      %16s
Unit:              
Comment Format:    OSS SVN revision
Description:       OSS SVN revision

Parameter Name:    OCS BRGHi NAME
Class:             header|setup
Context:           Instrument
Type:              string
Value Format:      %16s
Unit:              
Comment Format:    Name of the bright star
Description:       Name of the bright star

Parameter Name:    OCS BRGHi ALPHA
Class:             header|setup
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              HHMMSS.TTT
Comment Format:    [HHMMSS.TTT] Bright object alpha
Description:       [HHMMSS.TTT] Bright object alpha

Parameter Name:    OCS BRGHi DELTA
Class:             header|setup
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              DDMMSS.TTT
Comment Format:    [DDMMSS.TTT] Bright object delta
Description:       [DDMMSS.TTT] Bright object delta

Parameter Name:    OCS BRGHi MAG
Class:             header|setup
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              mag
Comment Format:    [mag] Bright object magnitude
Description:       [mag] Bright object magnitude

Parameter Name:    OCS BRGHi COMMENT
Class:             header|setup
Context:           Instrument
Type:              string
Value Format:      %45s
Unit:              
Comment Format:    Bright object comment
Description:       Bright object comment

Parameter Name:    OCS ARMi POSY
Class:             setup
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              mm
Comment Format:    [mm] SETUP y for arm
Description:       [mm] SETUP y for arm

Parameter Name:    OCS ARMi POSZ
Class:             setup
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              mm
Comment Format:    [mm] SETUP z for arm
Description:       [mm] SETUP z for arm

Parameter Name:    OCS TARG OFFSA
Class:             setup
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              arcsec
Comment Format:    [arcsec] Telescope offset in ALPHA
Description:       [arcsec] Telescope offset in ALPHA

Parameter Name:    OCS TARG OFFSD
Class:             setup
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              arcsec
Comment Format:    [arcsec] Telescope offset in DELTA
Description:       [arcsec] Telescope offset in DELTA

Parameter Name:    OCS TARG OFFSR
Class:             setup
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              deg
Comment Format:    [deg] Telescope offset in ROT
Description:       [deg] Telescope offset in ROT

Parameter Name:    OCS TARG DITHA
Class:             header|setup
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              arcsec
Comment Format:    [arcsec] Telescope dither in ALPHA
Description:       Telescope dither in ALPHA from nominal
                   science or sky position in arcsec.

Parameter Name:    OCS TARG DITHD
Class:             header|setup
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              arcsec
Comment Format:    [arcsec] Telescope dither in DELTA
Description:       Telescope dither in DELTA from nominal
                   science or sky position in arcsec.

Parameter Name:    OCS TARG DITHY
Class:             header|setup
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              mm
Comment Format:    [mm] Telescope dither in Y
Description:       Telescope dither in Y from nominal
                   science or sky position in mm.

Parameter Name:    OCS TARG DITHZ
Class:             header|setup
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              mm
Comment Format:    [mm] Telescope dither in Z
Description:       Telescope dither in Z from nominal
                   science or sky position in mm.

# special OS keyword used in KMOS_spec_acq_stdstar and KMOS_spec_cal_stdstar
Parameter Name:    OCS ARMCALIBi ALPHA
Class:             setup
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              
Comment Format:    ALPHA for calibration arm i
Description:       ALPHA for calibration arm i

Parameter Name:    OCS ARMCALIBi DELTA
Class:             setup
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              
Comment Format:    DELTA for calibration arm i
Description:       DELTA for calibration arm i

Parameter Name:    OCS ARMCALIB NEXT
Class:             setup
Context:           Instrument
Type:              integer
Value Format:      %d
Unit:              
Comment Format:    Next arm used for calibration
Description:       Next arm used for calibration

# used in KMOS_spec_cal_stdstar and KMOS_spec_cal_stdstarscipatt
Parameter Name:    OCS TARG TELLUR
Class:             header|setup
Context:           Instrument
Type:              logical
Value Format:      %c
Unit:              
Comment Format:    If T, it is a telluric star
Description:       If T, it is a telluric star

Parameter Name:    OCS TARG FLUXSTD
Class:             header|setup
Context:           Instrument
Type:              logical
Value Format:      %c
Unit:              
Comment Format:    If T, it is a flux standard
Description:       If T, it is a flux standard

# used for "small movements"
Parameter Name:    OCS ARMi OFFSY
Class:             setup
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              
Comment Format:    Pixel offset in y for arm
Description:       Pixel offset in y for arm from
                   centre of IFU

Parameter Name:    OCS ARMi OFFSZ
Class:             setup
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              
Comment Format:    Pixel offset in z for arm
Description:       Pixel offset in z for arm from
                   centre of IFU

# Dummy to allow OS to extract some data before passing GRFI on to ICS
Parameter Name:    OCS GRFI
Class:             header|setup
Context:           Instrument
Type:              string
Value Format:      %16s
Unit:              
Comment Format:    Combined name of grating and filter
Description:       Combined name of grating and filter

# Time offsets for precession/refraction calculations
Parameter Name:    OCS SOFW TPLACQ DURATION
Class:             setup
Context:           Instrument
Type:              integer
Value Format:      %d
Unit:              sec
Comment Format:    [sec] Duration of a acq template
Description:       [sec] Duration of a acq template

Parameter Name:    OCS SOFW TPLSCI DURATION
Class:             setup
Context:           Instrument
Type:              integer
Value Format:      %d
Unit:              sec
Comment Format:    [sec] Duration of a sci template
Description:       [sec] Duration of a sci template

# Rotator optimization
Parameter Name:    OCS ROT OFFSET
Class:             header
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              deg
Comment Format:    [deg] Angle used during rotator optimization
Description:       [deg] Angle used during rotator optimization

# Template ID
Parameter Name:    OCS TEMPL ID
Class:             setup
Context:           Instrument
Type:              string
Value Format:      %60s
Unit:              
Comment Format:    ID of the actual running template
Description:       ID of the actual running template

# Nasmyth angle
Parameter Name:    OCS ROT NAANGLE
Class:             header
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              deg
Comment Format:    [deg] Rotator relative to nasmyth platform
Description:       [deg] Rotator relative to nasmyth platform

# Keywords for LUT ATC (cluster)
Parameter Name:    OCS LUTARM INDEX
Class:             setup
Context:           Instrument
Type:              integer
Value Format:      %d
Unit:              
Comment Format:    Arm number
Description:       Arm number

Parameter Name:    OCS LUTPOSi NAME
Class:             setup
Context:           Instrument
Type:              string
Value Format:      %16s
Unit:              
Comment Format:    Target name
Description:       Target name

Parameter Name:    OCS LUTPOSi ALPHA
Class:             setup
Context:           Instrument
Type:              double
Value Format:      %.6f
Unit:              deg
Comment Format:    [deg] Right ascension
Description:       [deg] Right ascension

Parameter Name:    OCS LUTPOSi DELTA
Class:             setup
Context:           Instrument
Type:              double
Value Format:      %.6f
Unit:              deg
Comment Format:    [deg] Declination
Description:       [deg] Declination

Parameter Name:    OCS LUTPOSi MAG
Class:             setup
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              mag
Comment Format:    [mag] Magnitude
Description:       [mag] Magnitude

Parameter Name:    OCS LUTPOSi COMMENT
Class:             setup
Context:           Instrument
Type:              string
Value Format:      %45s
Unit:              
Comment Format:    Comment
Description:       Comment


##############################################################################
##############################################################################
##############################################################################
# Keywords temporarily included in dictionary for debugging
# purposes. After commissioning they can be removed. Right
# change in kmoHeaderKeywords.txt has to be made too (set
# all values equal to 0 for the keywords below).
#
Parameter Name:    OCS TARG ALPHA KARMA
Class:             header
Context:           Instrument
Type:              double
Value Format:      %.6f
Unit:              deg
Comment Format:    [deg] Telesc. pos. given by KARMA
Description:       [deg] Telesc. pos. given by KARMA

Parameter Name:    OCS TARG DELTA KARMA
Class:             header
Context:           Instrument
Type:              double
Value Format:      %.6f
Unit:              deg
Comment Format:    [deg] Telesc. pos. given by KARMA
Description:       [deg] Telesc. pos. given by KARMA

Parameter Name:    OCS ROT OFFANGLE KARMA
Class:             header
Context:           Instrument
Type:              double
Value Format:      %.6f
Unit:              deg
Comment Format:    [deg] Telesc. rotator angle
Description:       [deg] Telesc. rotator angle

Parameter Name:    OCS ARMi ALPHA KARMA
Class:             header
Context:           Instrument
Type:              double
Value Format:      %.6f
Unit:              deg
Comment Format:    [deg] Arm pos. given by KARMA
Description:       [deg] Arm pos. given by KARMA

Parameter Name:    OCS ARMi DELTA KARMA
Class:             header
Context:           Instrument
Type:              double
Value Format:      %.6f
Unit:              deg
Comment Format:    [deg] Arm pos. given by KARMA
Description:       [deg] Arm pos. given by KARMA

Parameter Name:    OCS ARMi Y KARMA
Class:             header
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              mm
Comment Format:    [mm] Arm pos. given by KARMA
Description:       [mm] Arm pos. given by KARMA

Parameter Name:    OCS ARMi Z KARMA
Class:             header
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              mm
Comment Format:    [mm] Arm pos. given by KARMA
Description:       [mm] Arm pos. given by KARMA

Parameter Name:    OCS ARMi R KARMA
Class:             header
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              mm
Comment Format:    [mm] Arm pos. given by KARMA
Description:       [mm] Arm pos. given by KARMA

Parameter Name:    OCS ARMi THETA KARMA
Class:             header
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              deg
Comment Format:    [deg] Arm pos. given by KARMA
Description:       [deg] Arm pos. given by KARMA

Parameter Name:    OCS TARG TIME PRRE
Class:             header
Context:           Instrument
Type:              double
Value Format:      %.3f
Unit:              s
Comment Format:    [s] Calculation time of prec. and refr.
Description:       [s] Calculation time of prec. and refr.

Parameter Name:    OCS TARG ALPHA PRRE
Class:             header
Context:           Instrument
Type:              double
Value Format:      %.6f
Unit:              deg
Comment Format:    [deg] Telesc. pos. with prec. and refr.
Description:       [deg] Telesc. pos. with prec. and refr.

Parameter Name:    OCS TARG DELTA PRRE
Class:             header
Context:           Instrument
Type:              double
Value Format:      %.6f
Unit:              deg
Comment Format:    [deg] Telesc. pos. with prec. and refr.
Description:       [deg] Telesc. pos. with prec. and refr.

Parameter Name:    OCS ROT OFFANGLE PRRE
Class:             header
Context:           Instrument
Type:              double
Value Format:      %.6f
Unit:              deg
Comment Format:    [deg] Telesc. rotator angle with prec. and refr.
Description:       [deg] Telesc. rotator angle with prec. and refr.

Parameter Name:    OCS ARMi ALPHA PRRE
Class:             header
Context:           Instrument
Type:              double
Value Format:      %.6f
Unit:              deg
Comment Format:    [deg] Arm pos. with prec. and refr.
Description:       [deg] Arm pos. with prec. and refr.

Parameter Name:    OCS ARMi DELTA PRRE
Class:             header
Context:           Instrument
Type:              double
Value Format:      %.6f
Unit:              deg
Comment Format:    [deg] Arm pos. with prec. and refr.
Description:       [deg] Arm pos. with prec. and refr.

# --- oOo ---










