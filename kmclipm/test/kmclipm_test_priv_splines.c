
/*********************************************************************
 * E.S.O. - VLT project
 *
 * "@(#) $Id: kmclipm_test_priv_splines.c,v 1.7 2013-10-08 11:12:02 aagudo Exp $"
 *
 * Tests for functions in src/kmclipm_priv_constants.c/.h
 *
 * who       when        what
 * --------  ----------  ----------------------------------------------
 * aagudo    2011-09-06  created
 */

/**
    @defgroup kmclipm_test_priv_splines Unit tests for the private functions in kmclipm_priv_splines

    Unit test for the functions in the module @ref kmclipm_priv_splines

    @{
*/

/*------------------------------------------------------------------------------
 *              Includes
 *----------------------------------------------------------------------------*/
#include <sys/stat.h>

#include "kmclipm_constants.h"
#include "kmclipm_priv_splines.h"
#include "kmclipm_priv_error.h"

cpl_msg_severity my_level= CPL_MSG_WARNING;

/*------------------------------------------------------------------------------
 *              Implementation
 *----------------------------------------------------------------------------*/

void test_bicubicspline_reg_reg()
{
/*erwtest*/
    /* invalid tests */

    /* valid tests */
    cpl_test_error(CPL_ERROR_NONE);
}

void test_bicubicspline_reg_irreg()
{
/*erwtest*/
    /* invalid tests */

    /* valid tests */
    cpl_test_error(CPL_ERROR_NONE);
}

void test_bicubicspline_reg_set()
{
/*erwtest*/
    /* invalid tests */

    /* valid tests */
    cpl_test_error(CPL_ERROR_NONE);
}

void test_bicubicspline_irreg_reg()
{
/*erwtest*/
    /* invalid tests */

    /* valid tests */
    cpl_test_error(CPL_ERROR_NONE);
}

void test_bicubicspline_irreg_irreg()
{
/*erwtest*/
    /* invalid tests */

    /* valid tests */
    cpl_test_error(CPL_ERROR_NONE);
}

void test_bicubicspline_irreg_set()
{
/*erwtest*/
    /* invalid tests */

    /* valid tests */
    cpl_test_error(CPL_ERROR_NONE);
}

void test_cubicspline_reg_reg()
{
/*erwtest*/
    /* invalid tests */

    /* valid tests */
    cpl_test_error(CPL_ERROR_NONE);
}

void test_cubicspline_irreg_reg()
{
/*erwtest*/
    /* invalid tests */

    /* valid tests */
    cpl_test_error(CPL_ERROR_NONE);
}

void test_cubicspline_reg_irreg()
{
/*erwtest*/
    /* invalid tests */

    /* valid tests */
    cpl_test_error(CPL_ERROR_NONE);
}

void test_cubicspline_irreg_irreg()
{
/*erwtest*/
    /* invalid tests */

    /* valid tests */
    cpl_test_error(CPL_ERROR_NONE);
}

void test_spline_reg_init()
{
/*erwtest*/
    /* invalid tests */

    /* valid tests */
    cpl_test_error(CPL_ERROR_NONE);
}

void test_spline_irreg_init()
{
/*erwtest*/
    /* invalid tests */

    /* valid tests */
    cpl_test_error(CPL_ERROR_NONE);
}

void test_spline_reg_interpolate()
{
/*erwtest*/
    /* invalid tests */

    /* valid tests */
    cpl_test_error(CPL_ERROR_NONE);
}

void test_spline_irreg_interpolate()
{
/*erwtest*/
    /* invalid tests */

    /* valid tests */
    cpl_test_error(CPL_ERROR_NONE);
}

void test_polynomial_irreg_reg()
{
/*erwtest*/
    /* invalid tests */

    /* valid tests */
    cpl_test_error(CPL_ERROR_NONE);
}

void test_vector()
{
/*erwtest*/
    /* invalid tests */

    /* valid tests */
    cpl_test_error(CPL_ERROR_NONE);
}

void test_blank_matrix()
{
/*erwtest*/
    /* invalid tests */

    /* valid tests */
    cpl_test_error(CPL_ERROR_NONE);
}

void test_matrix()
{
/*erwtest*/
    /* invalid tests */

    /* valid tests */
    cpl_test_error(CPL_ERROR_NONE);
}

void test_free_vector()
{
/*erwtest*/
    /* invalid tests */

    /* valid tests */
    cpl_test_error(CPL_ERROR_NONE);
}

void test_free_matrix()
{
/*erwtest*/
    /* invalid tests */

    /* valid tests */
    cpl_test_error(CPL_ERROR_NONE);
}

/**
    @brief  Executes the unit tests for all functions in the module @ref kmclipm_priv_splines

    @return Error code, 0 for NONE.
*/
int main()
{
    /* For DEVELOPERS:
     * to omit all CPL_MSG_INFO output, apply following statement in the shell
     * before executing "make check":
     *      setenv CPL_MSG_LEVEL warning
     *  (or export CPL_MSG_LEVEL=warning)
     */

    cpl_test_init("<usd-help@eso.org>", my_level);

    char *my_path = cpl_sprintf("%s/test_data", ".");
    mkdir(my_path, 0777);
    cpl_free(my_path);

    cpl_msg_set_level_from_env();
    my_level = cpl_msg_get_level();

    test_bicubicspline_reg_reg();
    test_bicubicspline_reg_irreg();
    test_bicubicspline_reg_set();
    test_bicubicspline_irreg_reg();
    test_bicubicspline_irreg_irreg();
    test_bicubicspline_irreg_set();
    test_cubicspline_reg_reg();
    test_cubicspline_irreg_reg();
    test_cubicspline_reg_irreg();
    test_cubicspline_irreg_irreg();
    test_spline_reg_init();
    test_spline_irreg_init();
    test_spline_reg_interpolate();
    test_spline_irreg_interpolate();
    test_polynomial_irreg_reg();
    test_vector();
    test_blank_matrix();
    test_matrix();
    test_free_vector();
    test_free_matrix();

    cpl_msg_info("","All tests done.");
    cpl_msg_set_level(CPL_MSG_WARNING);
    return cpl_test_end(0);
}

/** @} */
