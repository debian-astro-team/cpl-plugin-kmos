
/*********************************************************************
 * E.S.O. - VLT project
 *
 * "@(#) $Id: kmclipm_test_priv_constants.c,v 1.8 2013-10-08 11:12:02 aagudo Exp $"
 *
 * Tests for functions in src/kmclipm_priv_constants.c/.h
 *
 * who       when        what
 * --------  ----------  ----------------------------------------------
 * aagudo    2007-10-19  created
 * aagudo    2008-06-24  added tests for kmclipm_priv_get_rtd_width() and
 *                       kmclipm_priv_get_rtd_height()
 */

/**
    @defgroup kmclipm_test_priv_constants Unit tests for the private functions in kmclipm_priv_constants

    Unit test for the functions in the module @ref kmclipm_priv_constants

    @{
*/


/*------------------------------------------------------------------------------
 *              Includes
 *----------------------------------------------------------------------------*/
#include <sys/stat.h>

#include "kmclipm_constants.h"
#include "kmclipm_priv_constants.h"
#include "kmclipm_priv_error.h"

cpl_msg_severity my_level= CPL_MSG_WARNING;

/*------------------------------------------------------------------------------
 *              Implementation
 *----------------------------------------------------------------------------*/

void test_kmclipm_priv_ifu_pos_x()
{
    /* invalid tests */
    cpl_test_eq(kmclipm_priv_ifu_pos_x(-5), -1);
    cpl_test_eq(kmclipm_priv_ifu_pos_x(KMOS_NR_IFUS), -1);

    /* valid tests */
    cpl_test_eq(kmclipm_priv_ifu_pos_x(0), RTD_IFU_POS_1);
    cpl_test_eq(kmclipm_priv_ifu_pos_x(6), RTD_IFU_POS_2);
    cpl_test_eq(kmclipm_priv_ifu_pos_x(12), RTD_IFU_POS_3);
    cpl_test_eq(kmclipm_priv_ifu_pos_x(18), RTD_IFU_POS_4);
    cpl_test_eq(kmclipm_priv_ifu_pos_x(KMOS_NR_IFUS - 1), RTD_IFU_POS_4);
}

void test_kmclipm_priv_ifu_pos_y()
{
    /* invalid tests */
    cpl_test_eq(kmclipm_priv_ifu_pos_y(-99), -1);
    cpl_test_eq(kmclipm_priv_ifu_pos_y(KMOS_NR_IFUS + 15), -1);

    /* valid tests */
    cpl_test_eq(kmclipm_priv_ifu_pos_y(0), RTD_IFU_POS_5);
    cpl_test_eq(kmclipm_priv_ifu_pos_y(6), RTD_IFU_POS_4);
    cpl_test_eq(kmclipm_priv_ifu_pos_y(12), RTD_IFU_POS_3);
    cpl_test_eq(kmclipm_priv_ifu_pos_y(18), RTD_IFU_POS_2);
    cpl_test_eq(kmclipm_priv_ifu_pos_y(KMOS_NR_IFUS - 1), RTD_IFU_POS_1);
}

void test_kmclipm_priv_get_rtd_width()
{
    cpl_test_eq(kmclipm_priv_get_rtd_width(), 5 * KMOS_SLITLET_X + 6 * RTD_GAP);
}

void test_kmclipm_priv_get_rtd_height()
{
    cpl_test_eq(kmclipm_priv_get_rtd_height(), 5 * KMOS_SLITLET_Y + 6 * RTD_GAP);
}

void test_kmclipm_priv_get_output_path()
{
/*    cpl_test_eq(strcmp(kmclipm_priv_get_output_path(),""), 0);*/
}

void test_kmclipm_priv_set_output_path()
{
    char test_string[20] = "ansdd7390m&%G";
    kmclipm_priv_set_output_path(test_string);
    cpl_test_eq_string(kmclipm_priv_get_output_path(), test_string);
}

void test_kmclipm_priv_get_output_cubes()
{
    cpl_test_eq(kmclipm_priv_get_output_cubes(), FALSE);
}

void test_kmclipm_priv_set_output_cubes()
{
    kmclipm_priv_set_output_cubes(TRUE);
    cpl_test_eq(kmclipm_priv_get_output_cubes(), TRUE);
}

void test_kmclipm_priv_get_output_extracted_images()
{
    cpl_test_eq(kmclipm_priv_get_output_extracted_images(), FALSE);
}

void test_kmclipm_priv_set_output_extracted_images()
{
    kmclipm_priv_set_output_extracted_images(TRUE);
    cpl_test_eq(kmclipm_priv_get_output_extracted_images(), TRUE);
}

void test_kmclipm_priv_get_output_images()
{
    cpl_test_eq(kmclipm_priv_get_output_images(), FALSE);
}

void test_kmclipm_priv_set_output_images()
{
    kmclipm_priv_set_output_images(TRUE);
    cpl_test_eq(kmclipm_priv_get_output_images(), TRUE);
}

void test_kmclipm_priv_get_output_patrol()
{
    cpl_test_eq(kmclipm_priv_get_output_patrol(), TRUE);
}

void test_kmclipm_priv_set_output_patrol()
{
    kmclipm_priv_set_output_patrol(FALSE);
    cpl_test_eq(kmclipm_priv_get_output_patrol(), FALSE);
}

void test_kmclipm_priv_get_output_rtd()
{
    cpl_test_eq(kmclipm_priv_get_output_rtd(), TRUE);
}

void test_kmclipm_priv_set_output_rtd()
{
    kmclipm_priv_set_output_rtd(FALSE);
    cpl_test_eq(kmclipm_priv_get_output_rtd(), FALSE);
}


/**
    @brief  Executes the unit tests for all functions in the module @ref kmclipm_priv_constants

    @return Error code, 0 for NONE.
*/
int main()
{
    /* For DEVELOPERS:
     * to omit all CPL_MSG_INFO output, apply following statement in the shell
     * before executing "make check":
     *      setenv CPL_MSG_LEVEL warning
     *  (or export CPL_MSG_LEVEL=warning)
     */

    cpl_test_init("<usd-help@eso.org>", my_level);

    char *my_path = cpl_sprintf("%s/test_data", ".");
    mkdir(my_path, 0777);
    cpl_free(my_path);

    cpl_msg_set_level_from_env();
    my_level = cpl_msg_get_level();

    test_kmclipm_priv_ifu_pos_x();
    test_kmclipm_priv_ifu_pos_y();
    test_kmclipm_priv_get_rtd_width();
    test_kmclipm_priv_get_rtd_height();
    test_kmclipm_priv_get_output_path();
    test_kmclipm_priv_set_output_path();
    test_kmclipm_priv_get_output_cubes();
    test_kmclipm_priv_set_output_cubes();
    test_kmclipm_priv_get_output_extracted_images();
    test_kmclipm_priv_set_output_extracted_images();
    test_kmclipm_priv_get_output_patrol();
    test_kmclipm_priv_set_output_patrol();
    test_kmclipm_priv_get_output_rtd();
    test_kmclipm_priv_set_output_rtd();
    test_kmclipm_priv_get_output_images();
    test_kmclipm_priv_set_output_images();

    cpl_msg_info("","All tests done.");
    cpl_msg_set_level(CPL_MSG_WARNING);
    return cpl_test_end(0);
}

/** @} */
