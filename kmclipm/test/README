*******************************************************************************
***                      How to use & test kmclipm                          *** 
*******************************************************************************

1.) Install kmclipm
-------------------------------------------------------------------------------
- execute following commands:
        $ ./configure
        $ make
        $ make install

2.) Compiling the binary kmclipm_test_rtd
-------------------------------------------------------------------------------
- when calling (in kmclipm-folder)
        $ make check
  the kmclipm_test_rtd-binary is created and executed (with default configuration)
  This is the standard unit test. First test data is generated (raw object and sky
  images), then the data will be processed. At the end the previously generated
  reference RTD-image is compared with the final RTD-image.
        
- After this has been done once, kmclipm_test_rtd can be called directly with
        $ test/kmclipm_test_rtd
  For options see section 6.
  In fact, kmclipm_test_rtd calls 'test/.libs/lt-kmclipm_test_rtd'
  (i.e. when one wants to debug with gdb):
        
- for additional output, i.e. saving intermediate data to disk,
  parameters can be changed (see section 7.)

3.) Output data
-------------------------------------------------------------------------------
- All output is saved to folder test/data/output
  There are two files:
  - patrol.fits is the patrol field of the telescope with the
    images of the IFUs pasted into it
  - rtd.fits shows the images of the IFUs in a 5x5-grid.  When a IFU is disabled
    (corresponding value of IFU in vector "ifuIDs" in kmclipm_test.rtd.c is set
    to zero) a rectangle will appear as placeholder.
        
4.) Test data
-------------------------------------------------------------------------------
- Test data creates raw images of target and sky (fits-file with 3 extensions
  for each detector). They are saved in './test/data/output' (raw_image_obj.fits
  and raw_image_sky.fits). Furthermore reference RTD-images are generated and
  saved in './test/data/output' (ref_rtd.fits (target with subtracted sky),
  ref_rt_target.fits & ref_rtd_sky.fits).
  Additionally intermediate data can be saved if desired (see section 6.). All
  data will be saved in './test/data/output' and provided with a suffix 'ref_'
        
5.) Creating your own application using libkmclipm
-------------------------------------------------------------------------------
- the library containing kmclipm_rtd_image() is located in
        /kmclipm/src/.lib
        
6.) Options:
-------------------------------------------------------------------------------
- help
  ./kmclipm_test_rtd --help
        
- default configuration
  ./kmclipm_test_rtd
        or
  ./kmclipm_test_rtd ./data/raw_image_obj.fits TARG ./data/raw_image_sky.fits SKY
        
- other possible configurations
  ./kmclipm_test_rtd ./data/raw_image_sky.fits SKY ./data/raw_image_obj.fits TARG
  ./kmclipm_test_rtd ./data/raw_image_obj.fits TARG
  ./kmclipm_test_rtd ./data/raw_image_sky.fits SKY
        
- data generation
  ./kmclipm_test_rtd --generate
  Additional options:
  --cubes-out        Saves the cubes of each IFU to disk
                     (ref_cube_obj#.fits & ref_cube_sky#.fits))
  --image-cubes-out  Saves the collapsed images of cubes to disk
                     (ref_image_cube_obj#.fits & ref_image_cube_sky#.fits)

7.) Debug Parameters
-------------------------------------------------------------------------------
- in /data/parameter_config.txt following parameters can be defined
    
    rtd = TRUE          if RTD-image should be saved to disk
    patrol = TRUE       if patrol-view should be saved to disk
    cubes-out = FALSE   if reconstructed cubes of ervery IFU should be
                        saved to disk (time-consuming!) as 'cube_#.fits')
    images-out = FALSE  if flattened image of each reconstructed cube (image
                        pasted into RTD-image and patrol-view) should be saved
                        as cube to disk as 'image_cube.fits'
    
8.) Checking for memory leaks
-------------------------------------------------------------------------------
Use valgrind-3.2.3
With version 3.1.0 it wont run through (on a intel-based PC with 1GB RAM with
Kubuntu running)
