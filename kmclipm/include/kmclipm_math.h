
/*********************************************************************
 * E.S.O. - VLT project
 *
 * "@(#) $Id: kmclipm_math.h,v 1.2 2012-03-15 11:37:30 aagudo Exp $"
 *
 * Simple accessor functions to global variables.
 *
 * who       when        what
 * --------  ----------  ----------------------------------------------
 * aagudo    2008-06-10  created
 */

#ifndef KMCLIPM_MATH_H
#define KMCLIPM_MATH_H


/*------------------------------------------------------------------------------
 *                  Declaration Block
 *----------------------------------------------------------------------------*/

#ifdef __cplusplus
extern "C" {
#endif

#include <cpl.h>

/*------------------------------------------------------------------------------
 *                  math specific Defines
 *----------------------------------------------------------------------------*/

#define kmclipm_min(a, b)       ((a) < (b) ? (a) : (b))

#define kmclipm_max(a, b)       ((a) > (b) ? (a) : (b))

/*
   Size of the window used in kmclipm_priv_median_min() and
   kmclipm_priv_median_max()
*/
#define MEDIAN_WINDOW_SIZE      3

/*-----------------------------------------------------------------------------
 *              Prototypes
 *----------------------------------------------------------------------------*/
double      kmclipm_median_min(
                        const cpl_image* img,
                        int *xpos,
                        int *ypos);

double      kmclipm_median_max(
                        const cpl_image* img,
                        int *xpos,
                        int *ypos);

int         kmclipm_is_nan_or_inf(
                        double A);

int         kmclipm_is_inf(
                        double a);

#ifdef __cplusplus
}   /* extern "C" */
#endif

#endif
