
/*********************************************************************
 * E.S.O. - VLT project
 *
 * "@(#) $Id: kmclipm_constants.h,v 1.18 2013-10-08 11:11:00 aagudo Exp $"
 *
 * Simple accessor functions to global variables.
 *
 * who       when        what
 * --------  ----------  ----------------------------------------------
 * aaagudo    2008-06-25  created, from splitted kmclipm_priv_constants.h
 */

#ifndef KMCLIPM_CONSTANTS_H
#define KMCLIPM_CONSTANTS_H

/**
    @defgroup kmclipm_constants  KMCLIPM & KMOS specific defines

    @brief Definitions of instrument specific constants

    @par Synopsis:
    @code
      #include <kmclipm_constants.h>
    @endcode
    @{
*/

/*------------------------------------------------------------------------------
 *                  Declaration Block
 *----------------------------------------------------------------------------*/

#ifdef __cplusplus
extern "C" {
#endif

/*------------------------------------------------------------------------------
 *                  KMOS - hardware specific defines
 *----------------------------------------------------------------------------*/

/** @brief The width of a slitlet */
#define KMOS_SLITLET_X              14

/** @brief  The number of slitlets to generate a cube out of an IFU */
#define KMOS_SLITLET_Y              14

/** @brief  Maximum number of IFUs per detector */
#define KMOS_IFUS_PER_DETECTOR      8

/** @brief  Number of detectors */
#define KMOS_NR_DETECTORS           3

/** @brief  Number of IFUs */
#define KMOS_NR_IFUS                KMOS_NR_DETECTORS * KMOS_IFUS_PER_DETECTOR

/** @brief  Size of detector (in x- and y-direction) */
#define KMOS_DETECTOR_SIZE          2048

/** @brief  Field of view: 7.2 arcmin */
#define KMOS_FOV                    (double)7.2

/** @brief  The angular resolution of a pixel in arcsec ( ~ 7.2arcmin / DETECTOR_SIZE) */
#define KMOS_PIX_RESOLUTION         (double)0.2

/** @brief The range of the detector in milli-arcsec (in x- and y-direction).

    The Range is from center of first to center of last pixel.
    Total range is KMOS_PIXEL_RANGE + KMOS_PIXEL_RESOLUTION = 2800
*/
#define KMOS_PIXEL_RANGE            2600

/** @brief The resolution of the detector in milli-arcsec (in x- and y-direction).

    Total range is KMOS_PIXEL_RANGE + KMOS_PIXEL_RESOLUTION = 2800
*/
#define KMOS_PIXEL_RESOLUTION       200

/** @brief The pixel resolution in um */
#define KMOS_PIXEL_RESOLUTION_UM    0.0002

/** @brief The border of pixels on the detector used for electronic calibration */
#define KMOS_BADPIX_BORDER          4

/** @brief tolerance in degrees of rotator offsets between frames when calibrating */
#define KMOS_ROT_OFF_TOL_CAL        1.

/** @brief  Estimated gap width between slitlets of IFUs */
#define KMOS_GAP_WIDTH              4

/*----------------------------------------------------------------------------
 *                  FITS - standard keywords
 *--------------------------------------------------------------------------*/
/* standard fits-header keywords */
#define SIMPLE                      "SIMPLE"
#define BITPIX                      "BITPIX"
#define EXTEND                      "EXTEND"

#define DATE                        "DATE"
#define EXPTIME                     "EXPTIME"
#define EXTNAME                     "EXTNAME"
#define EXTVER                      "EXTVER"
#define XTENSION                    "XTENSION"
#define TFIELDS                     "TFIELDS"
#define NAXIS                       "NAXIS"
#define NAXIS1                      "NAXIS1"
#define NAXIS2                      "NAXIS2"
#define NAXIS3                      "NAXIS3"
#define CRPIX1                      "CRPIX1"
#define CRPIX2                      "CRPIX2"
#define CRPIX3                      "CRPIX3"
#define CRVAL1                      "CRVAL1"
#define CRVAL2                      "CRVAL2"
#define CRVAL3                      "CRVAL3"
#define CDELT1                      "CDELT1"
#define CDELT2                      "CDELT2"
#define CDELT3                      "CDELT3"
#define CTYPE1                      "CTYPE1"
#define CTYPE2                      "CTYPE2"
#define CTYPE3                      "CTYPE3"
#define CUNIT1                      "CUNIT1"
#define CUNIT2                      "CUNIT2"
#define CUNIT3                      "CUNIT3"
#define CD1_1                       "CD1_1"
#define CD1_2                       "CD1_2"
#define CD1_3                       "CD1_3"
#define CD2_1                       "CD2_1"
#define CD2_2                       "CD2_2"
#define CD2_3                       "CD2_3"
#define CD3_1                       "CD3_1"
#define CD3_2                       "CD3_2"
#define CD3_3                       "CD3_3"

/* valid values for XTENSION keyword*/
#define BINTABLE                    "BINTABLE"
#define IMAGE                       "IMAGE"

/*------------------------------------------------------------------------------
 *                  KMOS - specific keywords
 *----------------------------------------------------------------------------*/
#define ORIGFILE                    "ORIGFILE"
#define DATE_OBS                    "DATE-OBS"
#define GAIN                        "ESO DET CHIP GAIN"
#define RON                         "ESO DET CHIP RON"
#define NDIT                        "ESO DET NDIT"
#define MINDIT                      "ESO DET SEQ1 MINDIT"
#define CHIPINDEX                   "ESO DET CHIP INDEX"
#define INS_LAMP1_ST                "ESO INS LAMP1 ST"   /* ARC Ar */
#define INS_LAMP2_ST                "ESO INS LAMP2 ST"   /* ARC Ne */
#define INS_LAMP3_ST                "ESO INS LAMP3 ST"   /* FLAT */
#define INS_LAMP4_ST                "ESO INS LAMP4 ST"   /* FLAT */
#define DITHA                       "ESO OCS TARG DITHA"
#define DITHD                       "ESO OCS TARG DITHD"
#define READMODE                    "ESO DET READ CURNAME"
#define NDSAMPLES                   "ESO DET NDSAMPLES"
#define STDSTAR_MAG                 "ESO OCS STDSTAR MAG"
#define STDSTAR_TYPE                "ESO OCS STDSTAR TYPE"

#define IFU_VALID_PREFIX            "ESO OCS ARM"
#define IFU_VALID_POSTFIX           " NOTUSED"

#define IFU_IGNORE_PREFIX           "ESO PRO ARM"
#define IFU_IGNORE_POSTFIX          " NOTUSED"

#define IFU_ALPHA_PREFIX            "ESO OCS ARM"
#define IFU_ALPHA_POSTFIX           " ALPHA"

#define IFU_DELTA_PREFIX            "ESO OCS ARM"
#define IFU_DELTA_POSTFIX           " DELTA"

#define IFU_NAME_PREFIX             "ESO OCS ARM"
#define IFU_NAME_POSTFIX            " NAME"

#define IFU_TYPE_PREFIX             "ESO OCS ARM"
#define IFU_TYPE_POSTFIX            " TYPE"

#define IFU_FILTID_PREFIX           "ESO INS FILT"
#define IFU_FILTID_POSTFIX          " ID"

#define IFU_GRATID_PREFIX           "ESO INS GRAT"
#define IFU_GRATID_POSTFIX          " ID"

/* KMOS derotator, only used for choosing the correct calibration files
   (takes also in account the ESO OCS ROT OFFANGLE) */
#define ROTANGLE                    "ESO OCS ROT NAANGLE"
/* telescope rotation, only used in WCS calculation */
#define OFFANGLE                    "ESO OCS ROT OFFANGLE"
#define OBS_ID                      "ESO OBS ID"

#define BOUNDS_PREFIX               "ESO PRO BOUND IFU"

#define CAL_ROTANGLE                "ESO PRO ROT NAANGLE"
#define CAL_IFU_NR                  "ESO PRO IFU NR"

#define PRO_STD                     "ESO PRO STDSTAR"

/*----------------------------------------------------------------------------*/

#ifdef __cplusplus
}   /* extern "C" */
#endif

/** @} */

#endif  /* KMCLIPM_CONSTANTS_H */
