
/*********************************************************************
 * E.S.O. - VLT project
 *
 * "@(#) $Id: kmclipm_priv_splines.h,v 1.4 2013-07-29 18:08:45 aagudo Exp $"
 *
 * Functions that are implemented as recipes. They are also useedd in KMOS
 * data reduction pipeline.
 *
 * who       when        what
 * --------  ----------  ----------------------------------------------
 */

#ifndef KMCLIPM_PRIV_SPLINES_H
#define KMCLIPM_PRIV_SPLINES_H

/*-----------------------------------------------------------------------------
 *              Types
 *-----------------------------------------------------------------------------*/

/*erwdoc*/
/** @brief ??? */
enum boundary_mode {
    NATURAL,
    EXACT,
    ESTIMATED1,
    ESTIMATED2
};

/*-----------------------------------------------------------------------------
 *              Prototypes
 *----------------------------------------------------------------------------*/

double**    bicubicspline_reg_reg(
                        int nxi, double xi0, double dxi,
                        int nyi, double yi0, double dyi,
                        double **v,
                        int nxo, double xo0, double dxo,
                        int nyo, double yo0, double dyo,
                        enum boundary_mode mode);

double**    bicubicspline_reg_irreg(
                        int nxi, double xi0, double dxi,
                        int nyi, double yi0, double dyi,
                        double **vi,
                        int nxo, double *xo,
                        int nyo, double *yo,
                        enum boundary_mode mode);

double*     bicubicspline_reg_set(
                        int nxi, double xi0, double dxi,
                        int nyi, double yi0, double dyi,
                        double **vi,
                        int nso, double *xo, double *yo,
                        enum boundary_mode mode);

double**    bicubicspline_irreg_reg(
                        int nxi, double *xi,
                        int nyi, double *yi,
                        double **vi,
                        int nxo, double xo0, double dxo,
                        int nyo, double yo0, double dyo,
                        enum boundary_mode mode);

double**    bicubicspline_irreg_irreg(
                        int nxi, double *xi,
                        int nyi, double *yi,
                        double **vi,
                        int nxo, double *xo,
                        int nyo, double *yo,
                        enum boundary_mode mode);

double*     bicubicspline_irreg_set(
                        int nxi, double *xi,
                        int nyi, double *yi,
                        double **vi,
                        int nso, double *xo, double *yo,
                        enum boundary_mode mode);

double*     cubicspline_reg_reg(
                        int nin, double xi0, double dxi,
                        double *yi,
                        int nout, double xo0, double dxo,
                        enum boundary_mode mode, ...);

double*     cubicspline_irreg_reg(
                        int nin, double *xi,
                        double *yi,
                        int nout, double xo0, double dxo,
                        enum boundary_mode mode, ...);

double*     cubicspline_reg_irreg(
                        int nin, double xi0, double dxi,
                        double *yi,
                        int nout, double *xo,
                        enum boundary_mode mode, ...);

double*     cubicspline_irreg_reg_nonans(
                        int nin, double *xi,
                        double *yi,
                        int nout, double xo0, double dxo,
                        enum boundary_mode mode, ...);

double*     cubicspline_irreg_irreg(
                        int nin, double *xi,
                        double *yi,
                        int nout, double *xo,
                        enum boundary_mode mode, ...);

double*     spline_reg_init (
                        int nin, double delta_xin, double* yin,
                        enum boundary_mode mode, double y1start, double y1end);

double*     spline_irreg_init (
                        int nin, double* xin, double* yin,
                        enum boundary_mode mode, double y1start, double y1end);

double      spline_reg_interpolate(
                        int nin, double x0, double delta_x,
                        double *yin, double* y2in, double xnew);

double      spline_irreg_interpolate(
                        int nin, double *xin,
                        double *yin, double* y2in, double xnew);

double*     polynomial_irreg_reg(
                        int nin, double *xi,
                        double *yi,
                        int nout, double xo0, double dxo,
                        int order);

double*     polynomial_irreg_reg_nonans(
                        int nin, double *xi,
                        double *yi,
                        int nout, double xo0, double dxo,
                        int order);

double*     polynomial_irreg_irreg(
                        int nin, double *xi,
                        double *yi,
                        int nout, const double *xo,
                        int order);

double*     polynomial_irreg_irreg_nonans(
                        int nin, double *xi,
                        double *yi,
                        int nout, const double *xo,
                        int order);

double*     vector(int n);

double**    blank_matrix(int m);

double**    matrix(int m, int n);

void        free_vector(double* vector);

void        free_matrix(double** matrix, int m);

void remove_nans(int size_in, double *xin, int *size_out, double **xout);

void remove_2nans(int size_in, double *xin, double *yin, int *size_out, double **xout, double **yout);


#endif /* KMCLIPM_PRIV_SPLINES_H */


