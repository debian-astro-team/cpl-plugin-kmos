
/*********************************************************************
 * E.S.O. - VLT project
 *
 * "@(#) $Id: kmclipm_rtd_image.h,v 1.4 2012-06-26 08:44:15 aagudo Exp $"
 *
 * Main functions for the RTD.
 *
 * who       when        what
 * --------  ----------  ----------------------------------------------
 * aagudo    2007-10-19  created
 * aagudo    2007-12-07  added kmclipm_rtd_image_from_memory()
 */

#ifndef KMCLIPM_RTD_IMAGE_H
#define KMCLIPM_RTD_IMAGE_H

/*------------------------------------------------------------------------------
 *              Includes
 *----------------------------------------------------------------------------*/

#include <cpl.h>

#include "kmclipm_functions.h"


/*------------------------------------------------------------------------------
 *              Declaration Block
 *----------------------------------------------------------------------------*/

#ifdef __cplusplus
extern "C" {
#endif

//int          kmclipm_print_cal_files;

/*------------------------------------------------------------------------------
 *              Prototypes
 *----------------------------------------------------------------------------*/

cpl_error_code  kmclipm_rtd_image(
                        const cpl_image *actual_img,
                        const char      *actual_img_type,
                        const char      *additional_img_path,
                        const char      *additional_img_type,
                        const int       *ifu_id,
                        const double    *nominal_pos,
                        const char      *grating,
                        const char      *filter,
                        const double    rotator_offset,
                        kmclipm_fitpar *fitpar,
                        cpl_image       **rtd_img,
                        cpl_image       **patrol_img);

cpl_error_code  kmclipm_rtd_image_from_files(
                        const char      *actual_img,
                        const char      *actual_img_type,
                        const char      *additional_img_path,
                        const char      *additional_img_type,
                        const int       *ifu_id,
                        const double    *nominal_pos,
                        kmclipm_fitpar  *fitpar,
                        cpl_image       **rtd_img,
                        cpl_image       **patrol_img);

cpl_error_code  kmclipm_rtd_image_from_memory(
                        const cpl_image *actual_img,
                        const char      *actual_img_type,
                        const cpl_image *additional_img,
                        const char      *additional_img_type,
                        const int       *ifu_id,
                        const double    *nominal_pos,
                        const char      *grating,
                        const char      *filter,
                        const double    rotator_offset,
                        kmclipm_fitpar  *fitpar,
                        cpl_image       **rtd_img,
                        cpl_image       **patrol_img);

/*----------------------------------------------------------------------------*/

#ifdef __cplusplus
}   /* extern "C" */
#endif

#endif
