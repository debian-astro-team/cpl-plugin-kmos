
/*********************************************************************
 * E.S.O. - VLT project
 *
 * "@(#) $Id: kmclipm_compatibility_cpl.h,v 1.1.1.1 2012-01-18 09:32:30 yjung Exp $"
 *
 * Define the compatible CPL version
 *
 * who       when        what
 * --------  ----------  ----------------------------------------------
 * hlorch    2007-08-16  created
 */

/**
 * @defgroup kmclipm_compatibility_cpl    Defines: Compatible CPL Version
 * 
 * Below, the compatible CPL version is defined. It influences compilation.
 * 
 * @par Synopsis:
 * @code
 *   #include "kmclipm_compatibility_cpl.h"
 * @endcode
 */
/** @{ */

#ifndef KMCLIPM_COMPATIBILITY_CPL_H
#define KMCLIPM_COMPATIBILITY_CPL_H

/*-----------------------------------------------------------------------------
    Includes
 -----------------------------------------------------------------------------*/

#include <cpl.h>

/*-----------------------------------------------------------------------------
    Defines
 -----------------------------------------------------------------------------*/

#ifndef CPL_VERSION_CODE
    #error "CPL_VERSION_CODE not defined in cpl_version.h"
#endif

/**
 * @brief   Determine the major CPL version.
 * @hideinitializer
 */
#define KMCLIPM_GET_INSTALLED_CPL_VERSION   CPL_VERSION_CODE/65536

#if KMCLIPM_GET_INSTALLED_CPL_VERSION < 3
    #error "Incompatible CPL version found (too old)."
#endif

/*#if (CPL_VERSION_CODE/65536) >= 4
    #define KMCLIPM_GET_INSTALLED_CPL_VERSION    4
    *#warning "Compiling for CPL version 4."*
#elif (CPL_VERSION_CODE/65536) >= 3
    #define KMCLIPM_GET_INSTALLED_CPL_VERSION    3
    *#warning "Compiling for CPL version 3."*
#else
    #error "Incompatible CPL version found (too old)."
#endif
*/

/*----------------------------------------------------------------------------*/
/** @} */
#endif /* KMCLIPM_COMPATIBILITY_CPL_H */
