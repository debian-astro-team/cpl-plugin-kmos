
/*********************************************************************
 * E.S.O. - VLT project
 *
 * "@(#) $Id: kmclipm_priv_constants.h,v 1.1.1.1 2012-01-18 09:32:30 yjung Exp $"
 *
 * Simple accessor functions to global variables.
 *
 * who       when        what
 * --------  ----------  ----------------------------------------------
 * aaagudo    2007-10-19  created
 * aaagudo    2007-12-07  added RTD_GAP, RTD_OFFSET and redefined RTD_IFU_POS_1
 *                        RTD_IFU_POS_5 accordingly
 * aagudo     2007-12-14  added new define: PATROL_FIELD_BORDER
 *                        (Makes boundary around IFU-images in patrol-view)
 * aagudo     2008-06-24  added kmclipm_priv_get_rtd_width() and
 *                        kmclipm_priv_get_rtd_height()
 * aaagudo    2008-06-25  splitted into kmclipm_constants.h
 */

#ifndef KMCLIPM_PRIV_CONSTANTS_H
#define KMCLIPM_PRIV_CONSTANTS_H

/**
    @internal
    @defgroup kmclipm_priv_constants  PRIVATE: KMCLIPM specific defines & variables

    This module provides functions to access global variables needed within
    the other (private) modules.

    Take care not to address the global variables directly, but to use
    the corresponding @c kmclipm_priv_get_ - and @c kmclipm_priv_set_ -functions
    instead.

    @par Synopsis:
    @code
    #include <kmclipm_priv_constants.h>
    @endcode
    @{
*/

/*------------------------------------------------------------------------------
 *                  Declaration Block
 *----------------------------------------------------------------------------*/

#ifdef __cplusplus
extern "C" {
#endif

/*------------------------------------------------------------------------------
 *              RTD image specific Defines
 *----------------------------------------------------------------------------*/
/**  hallo */
#define RTD_GAP                      3
#define RTD_OFFSET                   KMOS_SLITLET_X + RTD_GAP
#define RTD_IFU_POS_1                RTD_GAP + 1
#define RTD_IFU_POS_2                RTD_IFU_POS_1 + RTD_OFFSET
#define RTD_IFU_POS_3                RTD_IFU_POS_2 + RTD_OFFSET
#define RTD_IFU_POS_4                RTD_IFU_POS_3 + RTD_OFFSET
#define RTD_IFU_POS_5                RTD_IFU_POS_4 + RTD_OFFSET

/*------------------------------------------------------------------------------
 *              PatrolView image specific Defines
 *----------------------------------------------------------------------------*/
#define PATROL_FIELD_BORDER          2
#define PATROL_FIELD_SIZE            2200
#define PATROL_FIELD_OFFSET          20
#define MM_PER_ARCSEC                0.5874

/* 1 pixel = 0.2 arcsec, nominal_pos indicates center of IFU, 
   for positioning in patrol-image lower-left corner is needed --> 0.76362 */
#define OFFSET_IFU                   KMOS_PIX_RESOLUTION * MM_PER_ARCSEC * 6.5

/* Field of view: 7.2 arcmin, convert to seconds,
   divide by to for radius --> 126.8784 */
#define MM_PER_FIELD_OF_VIEW         KMOS_FOV / 2 * 60 * MM_PER_ARCSEC

/*------------------------------------------------------------------------------
 *              Prototypes
 *----------------------------------------------------------------------------*/

int     kmclipm_priv_ifu_pos_x(int ifu);

int     kmclipm_priv_ifu_pos_y(int ifu);

int     kmclipm_priv_get_rtd_width();

int     kmclipm_priv_get_rtd_height();

void    kmclipm_priv_set_output_path(char* path);
char*   kmclipm_priv_get_output_path();

void    kmclipm_priv_set_output_cubes(int val);
int     kmclipm_priv_get_output_cubes();

void    kmclipm_priv_set_output_extracted_images(int val);
int     kmclipm_priv_get_output_extracted_images();

void    kmclipm_priv_set_output_patrol(int val);
int     kmclipm_priv_get_output_patrol();

void    kmclipm_priv_set_output_rtd(int val);
int     kmclipm_priv_get_output_rtd();

void    kmclipm_priv_set_output_images(int val);
int     kmclipm_priv_get_output_images();

/*----------------------------------------------------------------------------*/

#ifdef __cplusplus
}   /* extern "C" */
#endif

/** @} */

#endif
