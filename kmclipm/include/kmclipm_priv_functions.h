
/*********************************************************************
 * E.S.O. - VLT project
 *
 * "@(#) $Id: kmclipm_priv_functions.h,v 1.4 2012-10-02 14:40:03 aagudo Exp $"
 *
 * Functions that are implemented as recipes. They are also useedd in KMOS
 * data reduction pipeline.
 *
 * who       when        what
 * --------  ----------  ----------------------------------------------
 * aagudo     2007-10-19  created
 * aagudo     2007-12-14  added new function: kmclipm_priv_paint_ifu_rectangle()
 *                        (Makes boundary around IFU-images in patrol-view)
 * aagudo     2008-06-24  added kmclipm_priv_paint_ifu_rectangle_rtd() and
 *                        renamed kmclipm_priv_paint_ifu_rectangle() to
 *                        kmclipm_priv_paint_ifu_rectangle_patrol()
 */

#ifndef KMCLIPM_PRIV_FUNCTIONS_H
#define KMCLIPM_PRIV_FUNCTIONS_H

/*------------------------------------------------------------------------------
 *              Includes
 *----------------------------------------------------------------------------*/

#include <cpl.h>
#include "kmclipm_functions.h"


/*------------------------------------------------------------------------------
 *              Declaration Block
 *----------------------------------------------------------------------------*/

#ifdef __cplusplus
extern "C" {
#endif

/*-----------------------------------------------------------------------------
 *              Types
 *-----------------------------------------------------------------------------*/
extern int          kmclipm_cal_test_mode;
extern char         kmclipm_cal_file_path[];
extern int          kmclipm_file_path_was_set;

/*-----------------------------------------------------------------------------
 *              Prototypes
 *----------------------------------------------------------------------------*/

float           kmclipm_priv_paste_ifu_images(
                        const cpl_image *ifu_img,
                        cpl_image **out_img,
                        const int x_pos,
                        const int y_pos);

cpl_image*      kmclipm_priv_create_patrol_view(
                        const cpl_imagelist *ifu_image_list,
                        const double *nominal_pos,
                        const int *ifu_id);

void            kmclipm_priv_paint_ifu_rectangle_patrol(
                        cpl_image **out_img,
                        const int x_pos,
                        const int y_pos,
                        float max_val);

void            kmclipm_priv_paint_ifu_rectangle_rtd(
                        cpl_image **rtd_img,
                        const int *ifu_id,
                        float val);

int             kmclipm_priv_gauss2d_fnc(
                        const double x[],
                        const double a[],
                        double *result);

int             kmclipm_priv_gauss2d_fncd(
                        const double x[],
                        const double a[],
                        double result[]);

cpl_error_code  kmclipm_priv_debug_vector(
                        const cpl_vector *vec);

cpl_error_code  kmclipm_priv_debug_image(
                        const cpl_image *img);

cpl_error_code  kmclipm_priv_debug_cube(
                        const cpl_imagelist *imglist);

cpl_error_code  kmclipm_priv_find_angle(
                        double find_angle,
                        char **filt,
                        char **grat,
                        char **fn_xcal,
                        char **fn_ycal,
                        char **fn_lcal,
                        char **fn_lut);

const char*     kmclipm_get_cal_path();
/*
cpl_image*      kmclipm_imagelist_get_stderr(
                        const cpl_imagelist *cube);
*/
/*----------------------------------------------------------------------------*/

#ifdef __cplusplus
}   /* extern "C" */
#endif

#endif
