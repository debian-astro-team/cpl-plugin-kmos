------------------------------------------------------------------
  In this file:
------------------------------------------------------------------
* About KMCLIPM
* Prerequisites
* Installation
* Reporting Bugs

------------------------------------------------------------------
  About KMCLIPM
------------------------------------------------------------------
This module is an interface to the KMOS data reduction pipeline (DRS)
and the KMOS instrument control software (ICS).
DRS provides means to reconstruct exposures which ICS in return needs
for displaying in the real time display (RTD).

------------------------------------------------------------------
  Prerequisites
------------------------------------------------------------------
- CPL 5.3.1 or higher	(see http://www.eso.org/observing/cpl/)
- ANSI-C compiler
- UNIX Sh-Shell

------------------------------------------------------------------
  Installation
------------------------------------------------------------------
To install this library, THE FIRST thing is to decide where it shall be
installed, on a VLT machine or under a standard UNIX environment.

1.) VLT Installation
On a VLT machine, run the commands below (for detailed information please refer
to the user manual):

 $ cd src/
 $ make
 $ make install

2.) UNIX Installation
On a UNIX machine or under a compatible environment, run the commands below (for
detailed information please refer to the user manual):

 $ ./configure --prefix=<install-directory>
 $ make
 $ make install

Optionally, if the HTML reference manual should be created, execute also

 $ cd doxygen
 $ make html

Finally, if the KMCLIPM was not installed in one of the standard system
directories do not forget to add the location of the KMCLIPM library to
your LD_LIBRARY_PATH variable or the equivalent for your system.

------------------------------------------------------------------
  Reporting Bugs
------------------------------------------------------------------
Please report any bugs to the ESO SPR system.

