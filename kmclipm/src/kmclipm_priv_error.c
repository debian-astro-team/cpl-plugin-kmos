/*********************************************************************
 * E.S.O. - VLT project
 *
 * "@(#) $Id: kmclipm_priv_error.c,v 1.1.1.1 2012-01-18 09:32:30 yjung Exp $"
 *
 * PRIVATE macros/functions for kmclipm internal error handling
 *
 * who       when        what
 * --------  ----------  ----------------------------------------------
 * hlorch    2008-02-29  created
 */

/*-----------------------------------------------------------------------------
    Includes
 -----------------------------------------------------------------------------*/

#include <string.h>
#include "kmclipm_priv_error.h"

/*-----------------------------------------------------------------------------
    Global Definitions
 -----------------------------------------------------------------------------*/

/* Don't document these values with doxygen. */
const char  _KMCLIPM_MSG_ERR_UNEXPECTED[] =
                "unexpected error, aborting."
                " Please report to the CLIP team.",
            _KMCLIPM_MSG_ERR_HANDLING[] =
                "error handling bug found, aborting."
                " Please report to the CLIP team.";

/*-----------------------------------------------------------------------------
    Implementation
 -----------------------------------------------------------------------------*/
/**@{*/

/*----------------------------------------------------------------------------*/
/**
 * @ingroup  kmclipm_priv_error
 * @internal
 * @brief   Print two messages into one string. If both messages are not empty,
 *          then they are separated by a colon.
 * @param   outstr  The string to be output, is updated here.
 * @param   msg1    First message to append to outstr.
 * @param   msg2    Second message to append to outstr.
 * @param   maxlen  Max number of characters
 *
 * @return  Nothing
 */
/*----------------------------------------------------------------------------*/
void        _kmclipm_priv_error_sprint_messages(
                                            char        *outstr,
                                            const char  *msg1,
                                            const char  *msg2,
                                            int         maxlen)
{
    if (outstr == NULL)
        return;

    outstr[0] = '\0';
    if (msg1 == NULL || msg1[0] == '\0')
    {
        if (msg2 != NULL)
        {
            strncpy(outstr, msg2, maxlen);
            outstr[maxlen] = '\0';
        }
    }
    else
    {
        strncpy(outstr, msg1, maxlen);
        outstr[maxlen] = '\0';

        if (msg2 != NULL && msg2[0] != '\0')
        {
            int l;
            l = strlen(outstr);
            strncat(outstr, ": ", maxlen-l);
            l += 2;
            l = l > maxlen ? maxlen : 1;
            strncat(outstr, msg2, maxlen - l);
        }
    }

    return;
}


/**@}*/



