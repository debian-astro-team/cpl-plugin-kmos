
/*********************************************************************
 * E.S.O. - VLT project
 *
 * "@(#) $Id: kmclipm_priv_constants.c,v 1.6 2012-09-30 22:35:46 aagudo Exp $"
 *
 * Simple accessor functions to global variables.
 *
 * who       when        what
 * --------  ----------  ----------------------------------------------
 * aagudo     2007-10-19  created
 * aagudo     2008-06-24  added kmclipm_priv_get_rtd_width() and
 *                        kmclipm_priv_get_rtd_height()
 * aaagudo    2008-06-25  splitted into kmclipm_constants.c
 */

/*------------------------------------------------------------------------------
 *                  Includes
 *----------------------------------------------------------------------------*/

#include <math.h>

#include <cpl.h>

#include "kmclipm_constants.h"
#include "kmclipm_priv_constants.h"

/*------------------------------------------------------------------------------
 *                  Global Debugging Variables
 * These shouldn't be addressed directly. The corresponding kmclipm_priv_get_-
 * and kmclipm_priv_set_-functions should be used.
 *----------------------------------------------------------------------------*/

int  OUTPUT_CUBES = FALSE;
int  OUTPUT_IMAGE = FALSE;
int  OUTPUT_EXTRACTED_IMAGES = FALSE;
int  OUTPUT_PATROL = TRUE;
int  OUTPUT_RTD = TRUE;
char OUTPATH[1024];

const int x_pos_ifu_rtd[] =
     {RTD_IFU_POS_1, RTD_IFU_POS_2, RTD_IFU_POS_3, RTD_IFU_POS_4, RTD_IFU_POS_5,
      RTD_IFU_POS_1, RTD_IFU_POS_2, RTD_IFU_POS_3, RTD_IFU_POS_4, RTD_IFU_POS_5,
      RTD_IFU_POS_1, RTD_IFU_POS_2, RTD_IFU_POS_3, RTD_IFU_POS_4, RTD_IFU_POS_5,
      RTD_IFU_POS_1, RTD_IFU_POS_2, RTD_IFU_POS_3, RTD_IFU_POS_4, RTD_IFU_POS_5,
      RTD_IFU_POS_1, RTD_IFU_POS_2, RTD_IFU_POS_3, RTD_IFU_POS_4};

const int y_pos_ifu_rtd[] =
     {RTD_IFU_POS_5, RTD_IFU_POS_5, RTD_IFU_POS_5, RTD_IFU_POS_5, RTD_IFU_POS_5,
      RTD_IFU_POS_4, RTD_IFU_POS_4, RTD_IFU_POS_4, RTD_IFU_POS_4, RTD_IFU_POS_4,
      RTD_IFU_POS_3, RTD_IFU_POS_3, RTD_IFU_POS_3, RTD_IFU_POS_3, RTD_IFU_POS_3,
      RTD_IFU_POS_2, RTD_IFU_POS_2, RTD_IFU_POS_2, RTD_IFU_POS_2, RTD_IFU_POS_2,
      RTD_IFU_POS_1, RTD_IFU_POS_1, RTD_IFU_POS_1, RTD_IFU_POS_1};

/*------------------------------------------------------------------------------
 *                  Functions
 *----------------------------------------------------------------------------*/

/**
    @brief  Returns the x-position of the specified IFU in the RTD-image.
    @param  ifu The IFU of which the x-position is needed.

    @return The x-position of the specified IFU. Returns -1 when invalid @c ifu
            is passed (ifu <0 or ifu > KMOS_NR_IFUS).

    The numbering of the IFU starts at @c 0 and ends at <tt> KMOS_NR_IFUS - 1
    </tt>. The first IFU is located in the upper-left corner, the last IFU in
    the lower-right corner.
*/
int kmclipm_priv_ifu_pos_x(int ifu)
{
    if ((ifu >= 0) && (ifu < KMOS_NR_IFUS))
        return x_pos_ifu_rtd[ifu];
    else
        return -1;
}

/**
    @brief  Returns the y-position of the specified IFU in the RTD-image.
    @param  ifu The IFU of which the y-position is needed.

    @return The y-position of the specified IFU. Returns -1 when invalid @c ifu
            is passed (ifu <0 or ifu > KMOS_NR_IFUS).

    The numbering of the IFU starts at @c 0 and ends at <tt> KMOS_NR_IFUS - 1
    </tt>. The first IFU is located in the upper-left corner, the last IFU in
    the lower-right corner
*/
int kmclipm_priv_ifu_pos_y(int ifu)
{
    if ((ifu >= 0) && (ifu < KMOS_NR_IFUS))
        return y_pos_ifu_rtd[ifu];
    else
        return -1;
}

/**
    @brief  Calculates the width of the RTD-image.
*/
int kmclipm_priv_get_rtd_width()
{
    /* following equation equals to:
        5 * KMOS_SLITLET_X + 6 * RTD_GAP */
    int temp = (int)ceil(sqrt(KMOS_NR_IFUS));

    return temp * KMOS_SLITLET_X + ((temp + 1) * RTD_GAP);
}

/**
    @brief  Calculates the height of the RTD-image.
*/
int kmclipm_priv_get_rtd_height()
{
    /* following equation equals to:
        5 * KMOS_SLITLET_Y + 6 * RTD_GAP */
    int temp = (int)ceil(sqrt(KMOS_NR_IFUS));

    return temp * KMOS_SLITLET_Y + ((temp + 1) * RTD_GAP);
}

/**
    @brief  Sets the output-path where the generated fits-files will be saved.
    @param  path The new output-path.
*/
void kmclipm_priv_set_output_path(char* path)
{
    sprintf(OUTPATH, "%s", path);
}


/**
    @brief  Gets the output-path where the generated fits-files will be saved.
    @return The actual output path.
*/
char* kmclipm_priv_get_output_path()
{
    return OUTPATH;
}

/**
    @brief  Defines whether the cubes of each IFU should be saved to disk. 
    @param  val Set <tt>val = TRUE</tt> if they should be saved or @c FALSE
                otherwise.

    The default is @c FALSE. Saving the cubes takes a lot of time.
*/
void kmclipm_priv_set_output_cubes(int val)
{
    OUTPUT_CUBES = val;
}

/**
    @brief  Checks whether the cubes of each IFU should be saved to disk or not.
    @return If the return value is @c TRUE, the cubes are saved to disk.
            When @c FALSE they aren't saved.

    The default is @c FALSE.
*/
int kmclipm_priv_get_output_cubes()
{
    return OUTPUT_CUBES;
}

/**
    @brief  Defines whether the extracted images of each IFU should be saved
            to disk.
    @param  val Set <tt>val = TRUE</tt> if they should be saved or @c FALSE
                otherwise.

    The default is @c FALSE. Saving the images takes a lot of time.
*/
void kmclipm_priv_set_output_extracted_images(int val)
{
    OUTPUT_EXTRACTED_IMAGES = val;
}

/**
    @brief  Checks whether the extracted images of each IFU should be saved
            to disk or not.
    @return If the return value is @c TRUE, the images are saved to disk.
            When @c FALSE they aren't saved.

    The default is @c FALSE.
*/
int kmclipm_priv_get_output_extracted_images()
{
    return OUTPUT_EXTRACTED_IMAGES;
}

/**
    @brief  Defines whether the flattened images of the cubes of each IFU should
            be saved to disk. 
    @param  val  Set <tt>val = TRUE</tt> if they should be saved or @c FALSE
                 otherwise.

    The default is @c FALSE.
*/
void kmclipm_priv_set_output_images(int val)
{
    OUTPUT_IMAGE = val;
}


/**
    @brief  Checks whether the flattened images of the cubes of each IFU should
            be saved to disk or not.
    @return If the return value is @c TRUE, the images are saved to disk.
            When @c FALSE they aren't saved.

    The default is @c FALSE.
*/
int kmclipm_priv_get_output_images()
{
    return OUTPUT_IMAGE;
}

/**
    @brief  Defines whether the fits-file with the Patrol-View of the IFUs
            should be saved to disk.
    @param  val Set <tt>val = FALSE</tt> if it should not be saved or @c TRUE
                otherwise.

    The default is @c TRUE. The file is named patrol.fits.
*/
void kmclipm_priv_set_output_patrol(int val)
{
    OUTPUT_PATROL = val;
}

/**
    @brief  Checks whether fits-file with the Patrol-View of the IFUs
            should be saved to disk or not.
    @return If the return value is @c TRUE, the Patrol-View is saved to disk.
            When @c FALSE it isn't saved.

    The default is @c TRUE. The file is named patrol.fits.
*/
int kmclipm_priv_get_output_patrol()
{
    return OUTPUT_PATROL;
}

/**
    @brief  Defines whether the fits-file with the Real-Time-Display (RTD) of
            the IFUs should be saved to disk.
    @param  val  Set <tt>val = FALSE</tt> if it should not be saved or @c TRUE
                 otherwise.

    The default is @c TRUE. The file is named rtd.fits.
*/
void kmclipm_priv_set_output_rtd(int val)
{
    OUTPUT_RTD = val;
}

/**
    @brief  Checks whether fits-file with the Real-Time-Display (RTD) of the
            IFUs should be saved to disk or not.
    @return If the return value is @c TRUE, RTD-image is saved to disk.
            When @c FALSE it isn't saved.

    The default is @c TRUE. The file is named rtd.fits.
*/
int kmclipm_priv_get_output_rtd()
{
    return OUTPUT_RTD;
}
