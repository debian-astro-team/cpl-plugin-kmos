/*********************************************************************
 * E.S.O. - VLT project
 *
 * "@(#) $Id: kmclipm_version.c,v 1.1.1.1 2012-01-18 09:32:30 yjung Exp $"
 *
 * Functions to access the kmclipm version information
 *
 * who       when        what
 * --------  ----------  ----------------------------------------------
 * hlorch    2006-05-11  created
 */

/**
 * @defgroup kmclipm_version Library Version Information
 *
 * This module provides functions to access the kmclipm library's version
 * information.
 *
 * The library version applies to all components of the kmclipm,
 * and changes if any of the component libraries changes.
 *
 * @par Synopsis:
 * @code
 *   #include <kmclipm_version.h>
 * @endcode
 *
 * @{
 */

/*-----------------------------------------------------------------------------
    Includes
 -----------------------------------------------------------------------------*/

#include <stdlib.h>

#include "kmclipm_version.h"

/*-----------------------------------------------------------------------------
    Private Prototypes
 -----------------------------------------------------------------------------*/

static char *kmclipm_rcskey_extract_value(    char *rcskey);

/*-----------------------------------------------------------------------------
    Globals
 -----------------------------------------------------------------------------*/

static char rcskey_date[] = "@(#) $Date: 2012-01-18 09:32:30 $";
static char rcskey_revision[] = "@(#) $Revision: 1.1.1.1 $";

/*-----------------------------------------------------------------------------
    Implementation
 -----------------------------------------------------------------------------*/

/**
 * @brief   A pointer to the beginning of the value-string inside
 *          the RCS keyword ist returned, and the keyword is terminated
 *          at the end of the value-string.
 * @param   rcskey  The RCS keyword
 * @return  The value-string
 */
static char *kmclipm_rcskey_extract_value(    char *rcskey) {
    int n = 0;
    char c, *result;

    /* search end of keyword name (search for the colon) */
    while ((c = rcskey[n]) != 0 && c != ':')
    	n++;

    /* search end of space = beginning of value */
    while ((c = rcskey[n]) != 0 && (c == ' ' || c == ':'))
	   n++;

    result = &rcskey[n];

    /* terminate the rcs keyword at the end of the value */
    while ((c = rcskey[n]) != 0 && c != ' ' && c != ',' && c != '$')
	   n++;
    rcskey[n] = 0;

    return result;
}

/**
 * @brief   Get the library's revision string.
 * @return  The function returns the library's revision string.
 *
 * The function returns a pointer to the revision string of the library.
 */

const char *kmclipm_version_get_revision(     void) {
    static char *revision = NULL;

    if (revision == NULL)
    	revision = kmclipm_rcskey_extract_value(rcskey_revision);

    return revision;
}

/**
 * @brief   Get the library's revision-date string.
 * @return  The library's revision-date string.
 *
 * The function returns a pointer to the date string of the library's revision.
 */

const char *kmclipm_version_get_date(         void) {
    static char *date = NULL;

    if (date == NULL)
        date = kmclipm_rcskey_extract_value(rcskey_date);

    return date;
}

/**@}*/
