Source: cpl-plugin-kmos
Section: science
Priority: optional
Maintainer: Debian Astronomy Maintainers <debian-astro-maintainers@lists.alioth.debian.org>
Uploaders: Ole Streicher <olebole@debian.org>
Build-Depends: debhelper (>= 11),
               libcpl-dev (>= 6.3.1),
               python3,
               python3-astropy,
               python3-cpl,
               python3-sphinx
Standards-Version: 4.2.1
Homepage: https://www.eso.org/sci/software/pipelines/kmos
Vcs-Git: https://salsa.debian.org/debian-astro-team/cpl-plugin-kmos.git
Vcs-Browser: https://salsa.debian.org/debian-astro-team/cpl-plugin-kmos

Package: cpl-plugin-kmos
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Recommends: esorex|python3-cpl
Suggests: cpl-plugin-kmos-calib, cpl-plugin-kmos-doc
Multi-Arch: same
Description: ESO data reduction pipeline for the KMOS instrument
 This is the data reduction pipeline for the KMOS instrument of the
 Very Large Telescope (VLT) from the European Southern Observatory (ESO).
 .
 The K-band Multi Object Spectrograph (KMOS) is a second-generation instrument
 designed for operation on the VLT. The key feature of KMOS is its ability to
 perform Integral Field Spectroscopy in the near-infrared bands for 24 targets
 simultaneously.

Package: cpl-plugin-kmos-doc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Section: doc
Description: ESO data reduction pipeline documentation for KMOS
 This package contains the HTML documentation and manpages for the data
 reduction pipeline for the KMOS instrument of the Very Large Telescope
 (VLT) from the European Southern Observatory (ESO).

Package: cpl-plugin-kmos-calib
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, cpl-plugin-kmos, wget
Section: contrib/science
Description: ESO data reduction pipeline calibration data downloader for KMOS
 This package downloads calibration and other static data of the
 data reduction pipeline for the KMOS instrument of the
 Very Large Telescope (VLT) from the European Southern Observatory (ESO).
 .
 The K-band Multi Object Spectrograph (KMOS) is a second-generation instrument
 designed for operation on the VLT. The key feature of KMOS is its ability to
 perform Integral Field Spectroscopy in the near-infrared bands for 24 targets
 simultaneously.
